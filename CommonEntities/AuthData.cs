﻿using System;
using System.Collections.Generic;

namespace FactoryTuner.CommonEntities
{
    public class AuthData
    {
        public AuthData()
        {
            NameNumbers = new List<NameNumber>();
        }
        public List<NameNumber> NameNumbers { get; set; }
        public uint Version { get; set; }
    }

    public class NameNumber
    {
        public NameNumber()
        {

        }

        public NameNumber(string name, uint number)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Number = number;
            Role = 0;
        }

        public NameNumber(string name, uint number, string passwordHash, ushort role)
        {
            Name = name;
            Number = number;
            PasswordHash = passwordHash;
            Role = role;
        }
        public string Name { get; set; }
        public string PasswordHash { get; set; }
        public uint Number { get; set; }

        /// <summary>
        /// 1 - админ, 0 - простой
        /// </summary>
        public ushort Role { get; set; }

        public override string ToString()
        {
            return $"{this.Name} ({this.Number})";
        }
    }
}

