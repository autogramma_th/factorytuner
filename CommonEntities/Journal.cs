﻿using System;
using System.Collections.Generic;

namespace FactoryTuner.CommonEntities
{
    public class Journal
    {
        public List<Event> Events { get; set; } = new List<Event>();
    }

    public class Event
    {
        public string Date { get; set; }
        public string ChassisNuber { get; set; }
        public NameNumber NameNumber { get; set; } = new NameNumber();
        public string UserMachineName { get; set; }
        public bool IsOverride { get; set; }
        public float GPM { get; set; }
        public TireModel TireModel { get; set; } = new TireModel();
        public Transmission Transmission { get; set; } = new Transmission();
        public UInt16 K { get; set; }
        public string SN { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Crc { get; set; }

        public UInt32 OdometerTotal { get; set; }
        public UInt32 OdometerDay { get; set; }
        public UInt32 PrevOdometerTotal { get; set; }
        public UInt32 PrevOdometerDay { get; set; }
        public bool IsOdometerSet { get; set; }

        public bool IsUsdDeviceWritten { get; set; }
        public string USDDeviceSN { get; set; }
    }
}

