﻿namespace FactoryTuner.CommonEntities
{
    public class Configuration
    {
        public Configuration()
        {
        }
        public FileSource MainDataLocation { get; set; } = new FileSource();
        public FileSource AuthDataLocation { get; set; } = new FileSource();
        public FileSource JournalLocation { get; set; } = new FileSource();
    }

    public class FileSource
    {
        public FileSource()
        {

        }
        public uint Type { get; set; }
        public string Path { get; set; }
    }
}

