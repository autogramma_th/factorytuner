﻿using System.Collections.Generic;

namespace FactoryTuner.CommonEntities
{
    public class MainData
    {
        public MainData()
        {
        }
        public List<TireModel> TireModels { get; set; } = new List<TireModel>();
        public List<Transmission> Transmissions { get; set; } = new List<Transmission>();
        public List<float> GPM { get; set; } = new List<float>();
        public uint Version { get; set; }
    }

    public class TireModel
    {
        public TireModel()
        {

        }
        public TireModel(string size, string name, float dr)
        {
            Size = size;
            Name = name;
            DR = dr;
        }
        public string Size { get; set; }
        public string Name { get; set; }
        public float DR { get; set; }
    }

    public class Transmission
    {
        public Transmission()
        {

        }
        public Transmission(uint type, string name, float n)
        {
            Type = type;
            Name = name;
            N = n;
        }
        /// <summary>
        /// 1-manual, 2-auto, 3-tk
        /// </summary>
        public uint Type { get; set; }
        public string Name { get; set; }
        public float N { get; set; }
    }
}

