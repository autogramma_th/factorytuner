﻿using FactoryDataConfig;
using FactoryInstall.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace FactoryInstall
{
    public partial class MainForm : Form
    {
        public static MainForm Current;

        public MainForm()
        {
            Current = this;
            InitializeComponent();
            _defaultInstallationFolderParentPath = CodeHelpers.GetUserPublicFolderPath();
            _defaultInstallationFolderPath = Path.Combine(_defaultInstallationFolderParentPath, _defaultInstallFolderName);
        }

        private string _defaultInstallFolderName = "FactoryTuner";
        private string _defaultInstallationFolderPath;
        private string _defaultInstallationFolderParentPath;


#if EXTENDED
        private bool isExtended = true;
#else
        private bool isExtended = false;
#endif

        private void MainForm_Load(object sender, EventArgs e)
        {
            A.I.Init();
            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);
            this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();
            tbHelp.Text = GetDefaultHelpText();
            pictureBox1.Image = Resources.img_none;
            tbHelp.TextAlign = ContentAlignment.MiddleLeft;

            if (!A.I.ExtractDataFolderIfNotExtracted())
                MessageBox.Show(Resources.strExtractError);
            RefreshAppVersionsInUI();


            bnEditData.Visible = isExtended;
            bnEditUsers.Visible = isExtended;
            bnExport.Visible = isExtended;
            bnImport.Visible = isExtended;
            label1.Visible = isExtended;
            label2.Visible = isExtended;
            label3.Visible = isExtended;
        }

        private string GetDefaultHelpText()
        {
            if (isExtended)
                return Resources.strHelpText;
            else
                return Resources.strHelpText2;
        }

        private void bn_MouseLeave(object sender, EventArgs e)
        {
            tbHelp.Text = GetDefaultHelpText();
            tbHelp.TextAlign = ContentAlignment.MiddleLeft;
            pictureBox1.Image = Resources.img_none;
        }

        private void bnInstallFactoryTuner_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strInstallFactoryHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_install;
        }

        private void bnInstallFactoryDataConfig_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strInstallDataHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
        }

        private void bnInstallFactoryUserConfig_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strInstallUserHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
        }

        private void bnEditCfg_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strEditCfgHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
        }

        private void bnEditData_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strEditDataHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_editAutos;
        }

        private void bnEditUsers_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strEditUserHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_editUsers;
        }

        private void bnImport_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strImportHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_import;
        }

        private void bnRestore_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strRestoreInstallHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_restore;
        }

        private void bnExport_MouseEnter(object sender, EventArgs e)
        {
            tbHelp.Text = Resources.strExportHelp;
            tbHelp.TextAlign = ContentAlignment.MiddleCenter;
            pictureBox1.Image = Resources.img_export;
        }

        private void bnRestore_Click(object sender, EventArgs e)
        {
            A.I.RestoreDataFolder();
        }

        private void bnEditUsers_Click(object sender, EventArgs e)
        {
            A.I.OpenEditUserUtil();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            A.I.RemoveExtractedWithoutData();
        }

        private void bnEditCfg_Click(object sender, EventArgs e)
        {
            A.I.OpenEditFactoryTunerUtil();
        }

        private void bnEditData_Click(object sender, EventArgs e)
        {
            A.I.OpenEditDataUtil();
        }

        private string ShowInstallDialog(string guid)
        {
            string defaultDirectoryPath = Settings.Default.savedInstallPath;
            if (string.IsNullOrWhiteSpace(defaultDirectoryPath))
                defaultDirectoryPath = A.I.GetInstalledAppFolderPath(guid);

            if (string.IsNullOrEmpty(defaultDirectoryPath))
                defaultDirectoryPath = _defaultInstallationFolderPath;
            try
            {
                if (!Directory.Exists(defaultDirectoryPath))
                    Directory.CreateDirectory(defaultDirectoryPath);
            }
            catch
            {
                defaultDirectoryPath = _defaultInstallationFolderParentPath;
            }

            var installForm = new InstallForm(InstallForm.Type.Install);

            installForm.DefaultPath = defaultDirectoryPath;
            var result = installForm.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    if (!Directory.Exists(installForm.CurrentPath))
                        Directory.CreateDirectory(installForm.CurrentPath);
                }
                catch
                {
                    MessageBox.Show(Current, "Ошибка создания папки");
                    return null;
                }
                Settings.Default.savedInstallPath = installForm.CurrentPath;
                Settings.Default.Save();
                Settings.Default.Upgrade();
                return installForm.CurrentPath;
            }
            else return null;
        }



        private void bnInstallFactoryDataConfig_Click(object sender, EventArgs e)
        {
            var savePath = ShowInstallDialog(SharedConstants.FactoryDataCfgGuid);
            if (!string.IsNullOrWhiteSpace(savePath))
            {
                string folderPath = savePath;
                var exeName = AppName.FactoryDataConfig.ToString() + ".exe";
                var exePath = Path.Combine(folderPath, exeName);
                if (A.I.ExtractDataConfigFolder(folderPath))
                {
                    AddUninstaller(AppName.FactoryDataConfig, SharedConstants.FactoryDataCfgGuid, folderPath, exeName);
                    A.I.CreateDesktopShortcutLnk(exePath, SharedConstants.FactoryAutoShortkutName);
                    MessageBox.Show(Current, Resources.strDone);
                }
                else
                    MessageBox.Show(Resources.strExtractError);
            }
        }

        private void bnInstallFactoryUserConfig_Click(object sender, EventArgs e)
        {
            var savePath = ShowInstallDialog(SharedConstants.FactoryUserCfgGuid);
            if (!string.IsNullOrWhiteSpace(savePath))
            {
                string folderPath = savePath;
                var exeName = AppName.FactoryUserConfig.ToString() + ".exe";
                var exePath = Path.Combine(folderPath, exeName);
                if (A.I.ExtractUserConfigFolder(folderPath))
                {
                    AddUninstaller(AppName.FactoryUserConfig, SharedConstants.FactoryUserCfgGuid, folderPath, exeName);
                    A.I.CreateDesktopShortcutLnk(exePath, SharedConstants.FactoryUserShortkutName);
                    MessageBox.Show(Current, Resources.strDone);
                }
                else
                    MessageBox.Show(Resources.strExtractError);
            }
        }

        private void AddUninstaller(AppName app, string guid, string folderPath, string exeName)
        {
            if (!A.I.TryCreateUninstaller(app.ToString(), guid, folderPath, exeName))
            {
                MessageBox.Show(Current, Resources.strCantCreateUninstaller);
            }
        }

        private void RefreshAppVersionsInUI()
        {
            var installedVersion = A.I.GetAppVersion(Settings.Default.savedInstallPath, SharedConstants.FactoryTunerGuid);
            if (string.IsNullOrWhiteSpace(installedVersion))
                installedVersion = A.I.GetInstalledAppVersion(SharedConstants.FactoryTunerGuid);
            lbInstallerVersion.Text = "В дистрибутиве\nМИКАС Мастер: v" + CodeHelpers.GetAppVersion();

            if (string.IsNullOrWhiteSpace(installedVersion))
                lbInstalledVersion.Text = "Установленный\nМИКАС Мастер: не установлен";
            else
                lbInstalledVersion.Text = "Установленный\nМИКАС Мастер: v" + installedVersion;
        }

        private void bnInstallFactoryTuner_Click(object sender, EventArgs e)
        {
            var savePath = ShowInstallDialog(SharedConstants.FactoryTunerGuid);
            if (!string.IsNullOrWhiteSpace(savePath))
            {
                string folderPath = savePath;
                var exeName = AppName.MIKAS_Master.ToString() + ".exe";
                var exePath = Path.Combine(folderPath, exeName);
                CodeHelpers.RemoveAllFilesFromDirector(folderPath);
                if (A.I.ExtractFactoryTunerFolder(folderPath))
                {
                    //data после factoryTuner, т.к. могут попасть data скомпилированные с factoryTuner
                    CodeHelpers.CopyDirectory(A.I.GetInstallerDataFolderPath(), Path.Combine(folderPath, "data"));
                    AddUninstaller(AppName.MIKAS_Master, SharedConstants.FactoryTunerGuid, folderPath, exeName);
                    A.I.CreateDesktopShortcutLnk(exePath, SharedConstants.FactoryTunerShortkutName);
                    MessageBox.Show(Current, Resources.strDone);
                    RefreshAppVersionsInUI();
                }
                else
                    MessageBox.Show(Resources.strExtractError);
            }
        }

        private void bnImport_Click(object sender, EventArgs e)
        {

            var importForm = new InstallForm(InstallForm.Type.Import);
            string defaultDirectoryPath = Settings.Default.savedInportPath;
            if (string.IsNullOrWhiteSpace(defaultDirectoryPath))
                defaultDirectoryPath = A.I.GetInstalledAppFolderPath(SharedConstants.FactoryTunerGuid);
            importForm.DefaultPath = defaultDirectoryPath;
            var result = importForm.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                Settings.Default.savedInportPath = importForm.CurrentPath;
                Settings.Default.Save();
                Settings.Default.Upgrade();

                var fromPath = importForm.CurrentPath;

                if (string.IsNullOrEmpty(fromPath) || !Directory.Exists(Path.Combine(fromPath, @"data")))
                {
                    MessageBox.Show(Current, Resources.strDataFolderNotFound);
                }
                else
                {
                    bool isDone = false;
                    try
                    {
                        var installerDataPath = A.I.GetInstallerDataFolderPath();
                        if (!Directory.Exists(installerDataPath))
                            Directory.CreateDirectory(installerDataPath);

                        if (importForm.IsImportUser)
                        {
                            //Copy users.xml
                            File.Copy(Path.Combine(fromPath, SharedConstants.DefaultDataDirName, SharedConstants.DefaultUsersConfigFileName),
                                Path.Combine(installerDataPath, SharedConstants.DefaultUsersConfigFileName), true);
                        }

                        if (importForm.IsImportAuto)
                        {
                            //Copy base.xml
                            File.Copy(Path.Combine(fromPath, SharedConstants.DefaultDataDirName, SharedConstants.DefaultAutoDataFileName),
                                Path.Combine(installerDataPath, SharedConstants.DefaultAutoDataFileName), true);
                        }
                        isDone = true;
                    }
                    catch { }

                    if (isDone)
                        MessageBox.Show(Current, Resources.strDone);
                    else
                        MessageBox.Show(Current, "Ошибка копирования");
                }


            }
        }

        private void bnExport_Click(object sender, EventArgs e)
        {
            var exportForm = new InstallForm(InstallForm.Type.Export);
            string defaultDirectoryPath = Settings.Default.savedExportPath;
            if (string.IsNullOrWhiteSpace(defaultDirectoryPath))
                defaultDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            exportForm.DefaultPath = defaultDirectoryPath;
            var result = exportForm.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                Settings.Default.savedExportPath = exportForm.CurrentPath;
                Settings.Default.Save();
                Settings.Default.Upgrade();
                bool isDone = false;
                try
                {
                    var installerDataPath = A.I.GetInstallerDataFolderPath();
                    var toPath = exportForm.CurrentPath;
                    var toExtractedDataPath = Path.Combine(toPath, A.ExtractedFolderName, SharedConstants.DefaultDataDirName);
                    if (!Directory.Exists(toExtractedDataPath))
                        Directory.CreateDirectory(toExtractedDataPath);

                    //Copy config.xml
                    File.Copy(Path.Combine(installerDataPath, SharedConstants.DefaultConfigFileName),
                        Path.Combine(toExtractedDataPath, SharedConstants.DefaultConfigFileName), true);

                    if (exportForm.IsExportAuto)
                    {
                        //Copy base.xml
                        File.Copy(Path.Combine(installerDataPath, SharedConstants.DefaultAutoDataFileName),
                            Path.Combine(toExtractedDataPath, SharedConstants.DefaultAutoDataFileName), true);

                    }
                    if (exportForm.IsExportUser)
                    {
                        //Copy users.xml
                        File.Copy(Path.Combine(installerDataPath, SharedConstants.DefaultUsersConfigFileName),
                            Path.Combine(toExtractedDataPath, SharedConstants.DefaultUsersConfigFileName), true);

                    }
                    if (exportForm.IsExportDistr)
                    {
                        if (!A.I.ExtractSimplifiedInstallerFolder(toPath))
                            throw new Exception("Extract exception");
                    }

                    //Change output config
                    var dataFolder = Path.Combine(exportForm.CurrentPath, A.ExtractedFolderName, SharedConstants.DefaultDataDirName);
                    A.I.SetPathsInConfig(Path.Combine(dataFolder, SharedConstants.DefaultConfigFileName), dataFolder);
                    isDone = true;
                }
                catch { }
                if (isDone)
                    MessageBox.Show(Current, "Готово");
                else
                    MessageBox.Show(Current, "Ошибка копирования");
            }
        }
    }
}
