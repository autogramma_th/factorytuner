﻿namespace FactoryInstall
{
    partial class InstallForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstallForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbCaption = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInstallPath = new System.Windows.Forms.TextBox();
            this.bnSelectFolder = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.bnInstall = new System.Windows.Forms.Button();
            this.bnCancel = new System.Windows.Forms.Button();
            this.bnDefaultPath = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabInstall = new System.Windows.Forms.TabPage();
            this.tabImport = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.dgImport = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbImportUsers = new System.Windows.Forms.CheckBox();
            this.cbImportAuto = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbImportPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabExport = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.dgExport = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbExportUsers = new System.Windows.Forms.CheckBox();
            this.cbExportAuto = new System.Windows.Forms.CheckBox();
            this.cbExportDistr = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbExportPath = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabInstall.SuspendLayout();
            this.tabImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgImport)).BeginInit();
            this.tabExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgExport)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1042, 108);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lbCaption
            // 
            this.lbCaption.AutoSize = true;
            this.lbCaption.BackColor = System.Drawing.Color.Transparent;
            this.lbCaption.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCaption.Location = new System.Drawing.Point(16, 10);
            this.lbCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(107, 23);
            this.lbCaption.TabIndex = 1;
            this.lbCaption.Text = "Заголовок 2";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox2.Location = new System.Drawing.Point(0, 107);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1042, 1);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(8, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1011, 126);
            this.label1.TabIndex = 3;
            this.label1.Text = "Установщик установит \"МИКАС Мастер\" в следующую папку.\r\n\r\nЧтобы установить в данн" +
    "ую папку, нажмите кнопку \"Ок\". \r\nЧтобы сменить папку, введите нужный путь или вы" +
    "берите её, нажав кнопку \"Обзор\".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 142);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Папка:";
            // 
            // tbInstallPath
            // 
            this.tbInstallPath.Location = new System.Drawing.Point(14, 172);
            this.tbInstallPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbInstallPath.Name = "tbInstallPath";
            this.tbInstallPath.Size = new System.Drawing.Size(606, 30);
            this.tbInstallPath.TabIndex = 5;
            this.tbInstallPath.TextChanged += new System.EventHandler(this.tbInstallPath_TextChanged);
            // 
            // bnSelectFolder
            // 
            this.bnSelectFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSelectFolder.Location = new System.Drawing.Point(628, 171);
            this.bnSelectFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bnSelectFolder.Name = "bnSelectFolder";
            this.bnSelectFolder.Size = new System.Drawing.Size(192, 35);
            this.bnSelectFolder.TabIndex = 6;
            this.bnSelectFolder.Text = "Обзор...";
            this.bnSelectFolder.UseVisualStyleBackColor = true;
            this.bnSelectFolder.Click += new System.EventHandler(this.bnSelectFolder_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox3.Location = new System.Drawing.Point(0, 613);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1042, 1);
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // bnInstall
            // 
            this.bnInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bnInstall.Location = new System.Drawing.Point(835, 624);
            this.bnInstall.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bnInstall.Name = "bnInstall";
            this.bnInstall.Size = new System.Drawing.Size(192, 35);
            this.bnInstall.TabIndex = 8;
            this.bnInstall.Text = "Ок";
            this.bnInstall.UseVisualStyleBackColor = true;
            this.bnInstall.Click += new System.EventHandler(this.bnInstall_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bnCancel.Location = new System.Drawing.Point(634, 624);
            this.bnCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(192, 35);
            this.bnCancel.TabIndex = 9;
            this.bnCancel.Text = "Отмена";
            this.bnCancel.UseVisualStyleBackColor = true;
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // bnDefaultPath
            // 
            this.bnDefaultPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bnDefaultPath.Location = new System.Drawing.Point(826, 171);
            this.bnDefaultPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bnDefaultPath.Name = "bnDefaultPath";
            this.bnDefaultPath.Size = new System.Drawing.Size(192, 35);
            this.bnDefaultPath.TabIndex = 10;
            this.bnDefaultPath.Text = "По-умолчанию";
            this.bnDefaultPath.UseVisualStyleBackColor = true;
            this.bnDefaultPath.Click += new System.EventHandler(this.bnDefaultPath_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabInstall);
            this.tabControl1.Controls.Add(this.tabImport);
            this.tabControl1.Controls.Add(this.tabExport);
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(0, 107);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(6, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1042, 503);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 12;
            this.tabControl1.TabStop = false;
            // 
            // tabInstall
            // 
            this.tabInstall.Controls.Add(this.label1);
            this.tabInstall.Controls.Add(this.bnDefaultPath);
            this.tabInstall.Controls.Add(this.label2);
            this.tabInstall.Controls.Add(this.tbInstallPath);
            this.tabInstall.Controls.Add(this.bnSelectFolder);
            this.tabInstall.Location = new System.Drawing.Point(4, 5);
            this.tabInstall.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabInstall.Name = "tabInstall";
            this.tabInstall.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabInstall.Size = new System.Drawing.Size(1034, 494);
            this.tabInstall.TabIndex = 0;
            this.tabInstall.Text = "tabPage1";
            this.tabInstall.UseVisualStyleBackColor = true;
            // 
            // tabImport
            // 
            this.tabImport.Controls.Add(this.button3);
            this.tabImport.Controls.Add(this.dgImport);
            this.tabImport.Controls.Add(this.cbImportUsers);
            this.tabImport.Controls.Add(this.cbImportAuto);
            this.tabImport.Controls.Add(this.label3);
            this.tabImport.Controls.Add(this.label4);
            this.tabImport.Controls.Add(this.tbImportPath);
            this.tabImport.Controls.Add(this.button1);
            this.tabImport.Location = new System.Drawing.Point(4, 5);
            this.tabImport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabImport.Name = "tabImport";
            this.tabImport.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabImport.Size = new System.Drawing.Size(1034, 494);
            this.tabImport.TabIndex = 1;
            this.tabImport.Text = "tabPage2";
            this.tabImport.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(830, 174);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(192, 35);
            this.button3.TabIndex = 14;
            this.button3.Text = "По-умолчанию";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.bnDefaultPath_Click);
            // 
            // dgImport
            // 
            this.dgImport.AllowUserToAddRows = false;
            this.dgImport.AllowUserToDeleteRows = false;
            this.dgImport.AllowUserToResizeColumns = false;
            this.dgImport.AllowUserToResizeRows = false;
            this.dgImport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgImport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgImport.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgImport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgImport.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgImport.Enabled = false;
            this.dgImport.EnableHeadersVisualStyles = false;
            this.dgImport.Location = new System.Drawing.Point(16, 347);
            this.dgImport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgImport.Name = "dgImport";
            this.dgImport.ReadOnly = true;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgImport.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgImport.RowHeadersVisible = false;
            this.dgImport.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.dgImport.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgImport.RowTemplate.Height = 24;
            this.dgImport.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgImport.Size = new System.Drawing.Size(1005, 132);
            this.dgImport.TabIndex = 13;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Название";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Версия в дистрибутиве";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Версия в расположении";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // cbImportUsers
            // 
            this.cbImportUsers.AutoSize = true;
            this.cbImportUsers.Location = new System.Drawing.Point(16, 253);
            this.cbImportUsers.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbImportUsers.Name = "cbImportUsers";
            this.cbImportUsers.Size = new System.Drawing.Size(401, 29);
            this.cbImportUsers.TabIndex = 12;
            this.cbImportUsers.Text = "Импортировать список пользователей";
            this.cbImportUsers.UseVisualStyleBackColor = true;
            // 
            // cbImportAuto
            // 
            this.cbImportAuto.AutoSize = true;
            this.cbImportAuto.Location = new System.Drawing.Point(16, 215);
            this.cbImportAuto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbImportAuto.Name = "cbImportAuto";
            this.cbImportAuto.Size = new System.Drawing.Size(392, 29);
            this.cbImportAuto.TabIndex = 11;
            this.cbImportAuto.Text = "Импортировать список настроек авто";
            this.cbImportAuto.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(10, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1011, 126);
            this.label3.TabIndex = 7;
            this.label3.Text = "Система импортирует данные из следующей папки.\r\n\r\nЧтобы импортировать из данной п" +
    "апки, нажмите кнопку \"Ок\". \r\nЧтобы сменить папку, введите нужный путь или выбери" +
    "те её, нажав кнопку \"Обзор\".";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Папка:";
            // 
            // tbImportPath
            // 
            this.tbImportPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbImportPath.Location = new System.Drawing.Point(16, 175);
            this.tbImportPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbImportPath.Name = "tbImportPath";
            this.tbImportPath.Size = new System.Drawing.Size(604, 30);
            this.tbImportPath.TabIndex = 9;
            this.tbImportPath.TextChanged += new System.EventHandler(this.tbInstallPath_TextChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(629, 174);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(192, 35);
            this.button1.TabIndex = 10;
            this.button1.Text = "Обзор...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.bnSelectFolder_Click);
            // 
            // tabExport
            // 
            this.tabExport.Controls.Add(this.button4);
            this.tabExport.Controls.Add(this.dgExport);
            this.tabExport.Controls.Add(this.cbExportUsers);
            this.tabExport.Controls.Add(this.cbExportAuto);
            this.tabExport.Controls.Add(this.cbExportDistr);
            this.tabExport.Controls.Add(this.label5);
            this.tabExport.Controls.Add(this.label6);
            this.tabExport.Controls.Add(this.tbExportPath);
            this.tabExport.Controls.Add(this.button2);
            this.tabExport.Location = new System.Drawing.Point(4, 5);
            this.tabExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabExport.Name = "tabExport";
            this.tabExport.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabExport.Size = new System.Drawing.Size(1034, 494);
            this.tabExport.TabIndex = 2;
            this.tabExport.Text = "tabPage1";
            this.tabExport.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(830, 169);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(192, 35);
            this.button4.TabIndex = 17;
            this.button4.Text = "По-умолчанию";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.bnDefaultPath_Click);
            // 
            // dgExport
            // 
            this.dgExport.AllowUserToAddRows = false;
            this.dgExport.AllowUserToDeleteRows = false;
            this.dgExport.AllowUserToResizeColumns = false;
            this.dgExport.AllowUserToResizeRows = false;
            this.dgExport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgExport.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgExport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgExport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgExport.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgExport.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgExport.Enabled = false;
            this.dgExport.EnableHeadersVisualStyles = false;
            this.dgExport.Location = new System.Drawing.Point(16, 324);
            this.dgExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgExport.Name = "dgExport";
            this.dgExport.ReadOnly = true;
            this.dgExport.RowHeadersVisible = false;
            this.dgExport.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.dgExport.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgExport.RowTemplate.Height = 24;
            this.dgExport.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgExport.Size = new System.Drawing.Size(1005, 161);
            this.dgExport.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Название";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Версия в дистрибутиве";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Версия в расположении";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // cbExportUsers
            // 
            this.cbExportUsers.AutoSize = true;
            this.cbExportUsers.Location = new System.Drawing.Point(16, 286);
            this.cbExportUsers.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbExportUsers.Name = "cbExportUsers";
            this.cbExportUsers.Size = new System.Drawing.Size(404, 29);
            this.cbExportUsers.TabIndex = 15;
            this.cbExportUsers.Text = "Экспортировать список пользователей";
            this.cbExportUsers.UseVisualStyleBackColor = true;
            // 
            // cbExportAuto
            // 
            this.cbExportAuto.AutoSize = true;
            this.cbExportAuto.Location = new System.Drawing.Point(16, 249);
            this.cbExportAuto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbExportAuto.Name = "cbExportAuto";
            this.cbExportAuto.Size = new System.Drawing.Size(395, 29);
            this.cbExportAuto.TabIndex = 14;
            this.cbExportAuto.Text = "Экспортировать список настроек авто";
            this.cbExportAuto.UseVisualStyleBackColor = true;
            // 
            // cbExportDistr
            // 
            this.cbExportDistr.AutoSize = true;
            this.cbExportDistr.Location = new System.Drawing.Point(16, 211);
            this.cbExportDistr.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbExportDistr.Name = "cbExportDistr";
            this.cbExportDistr.Size = new System.Drawing.Size(570, 29);
            this.cbExportDistr.TabIndex = 13;
            this.cbExportDistr.Text = "Экспортировать дистрибутив установки МИКАС Мастера";
            this.cbExportDistr.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(10, 19);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(1011, 126);
            this.label5.TabIndex = 7;
            this.label5.Text = "Система скопирует данные в следующую папку.\r\n\r\nЧтобы скопировать в данную папку, " +
    "нажмите кнопку \"Ок\". \r\nЧтобы сменить папку, введите нужный путь или выберите её," +
    " нажав кнопку \"Обзор\".";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 141);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Папка:";
            // 
            // tbExportPath
            // 
            this.tbExportPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbExportPath.Location = new System.Drawing.Point(16, 171);
            this.tbExportPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbExportPath.Name = "tbExportPath";
            this.tbExportPath.Size = new System.Drawing.Size(604, 30);
            this.tbExportPath.TabIndex = 9;
            this.tbExportPath.TextChanged += new System.EventHandler(this.tbInstallPath_TextChanged);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(629, 169);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(192, 35);
            this.button2.TabIndex = 10;
            this.button2.Text = "Обзор...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.bnSelectFolder_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.CheckFileExists = false;
            this.openFileDialog1.CheckPathExists = false;
            this.openFileDialog1.FileName = "_";
            this.openFileDialog1.Filter = "Папка|*.folder";
            this.openFileDialog1.ReadOnlyChecked = true;
            this.openFileDialog1.Title = "Открыть";
            this.openFileDialog1.ValidateNames = false;
            // 
            // InstallForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 671);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bnCancel);
            this.Controls.Add(this.bnInstall);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lbCaption);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InstallForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Заголовок 1";
            this.Load += new System.EventHandler(this.InstallForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabInstall.ResumeLayout(false);
            this.tabInstall.PerformLayout();
            this.tabImport.ResumeLayout(false);
            this.tabImport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgImport)).EndInit();
            this.tabExport.ResumeLayout(false);
            this.tabExport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgExport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbCaption;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbInstallPath;
        private System.Windows.Forms.Button bnSelectFolder;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button bnInstall;
        private System.Windows.Forms.Button bnCancel;
        private System.Windows.Forms.Button bnDefaultPath;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabInstall;
        private System.Windows.Forms.TabPage tabImport;
        private System.Windows.Forms.TabPage tabExport;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbImportPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbExportPath;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbImportUsers;
        private System.Windows.Forms.CheckBox cbImportAuto;
        private System.Windows.Forms.CheckBox cbExportUsers;
        private System.Windows.Forms.CheckBox cbExportAuto;
        private System.Windows.Forms.CheckBox cbExportDistr;
        private System.Windows.Forms.DataGridView dgImport;
        private System.Windows.Forms.DataGridView dgExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}