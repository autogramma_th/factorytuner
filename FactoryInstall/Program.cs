﻿using FactoryDataConfig;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace FactoryInstall
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();
        static Mutex mutex = new Mutex(false, SharedConstants.FactoryInstallerGuid.ToString());

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetProcessDPIAware();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            RunForm(() => Application.Run(new MainForm()));
        }

        static void RunForm(Action createFormAction)
        {
            // if you like to wait a few seconds in case that the instance is just 
            // shutting down
            if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
            {
                CodeHelpers.SwitchToAlreadyOpenedApp();
                return;
            }

            try
            {
                createFormAction();
            }
            finally { mutex.ReleaseMutex(); } // I find this more explicit
        }
    }
}
