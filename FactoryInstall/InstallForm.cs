﻿using FactoryDataConfig;
using FactoryTuner.CommonEntities;
using Ionic.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FactoryInstall
{
    public partial class InstallForm : Form
    {
        public static InstallForm Current;
        public enum Type
        {
            Install = 0,
            Import = 1,
            Export = 2
        }

        public Type CurrentType { get; set; }
        public string DefaultPath
        {
            get => _defaultPath;
            set
            {
                if (value != null)
                    _defaultPath = value;
                else
                    _defaultPath = string.Empty;
            }
        }
        public string CurrentPath
        {
            get => _currentPath;
            set
            {
                if (value != null)
                    _currentPath = value;
                else
                    _currentPath = string.Empty;
            }
        }

        private const string naStr = "N/A";
        private string _defaultPath;
        private string _currentPath;

        public bool IsImportUser
        {
            get { return cbImportUsers.Checked; }
        }

        public bool IsImportAuto
        {
            get { return cbImportAuto.Checked; }
        }

        public bool IsExportUser
        {
            get { return cbExportUsers.Checked; }
        }

        public bool IsExportAuto
        {
            get { return cbExportAuto.Checked; }
        }

        public bool IsExportDistr
        {
            get { return cbExportDistr.Checked; }
        }

        public InstallForm(Type type)
        {
            Current = this;
            CurrentType = type;
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
        }

        private void InstallForm_Load(object sender, EventArgs e)
        {
            lbCaption.Parent = pictureBox1;
            lbCaption.BackColor = Color.Transparent;

            switch (CurrentType)
            {
                case Type.Install:
                    this.Text = "Установка МИКАС Мастера";
                    lbCaption.Text = "Выбор папки для установки";
                    tabControl1.SelectTab(0);
                    bnInstall.Text = "Установить";
                    break;
                case Type.Import:
                    this.Text = "Импорт";
                    lbCaption.Text = "Выбор параметров импорта";
                    tabControl1.SelectTab(1);
                    bnInstall.Text = "Импорт";
                    break;
                case Type.Export:
                    this.Text = "Экспорт дистрибутива";
                    lbCaption.Text = "Выбор параметров экспорта";
                    bnInstall.Text = "Экспорт";
                    tabControl1.SelectTab(2);
                    break;
                default:
                    this.Close();
                    return;
            }

            CurrentPath = DefaultPath;
            UpdateCurrentPathInTextBox();

            UpdateVersions();
        }

        private void UpdateVersions()
        {
            var installerUsersPath = Path.Combine(A.I.GetInstallerDataFolderPath(), SharedConstants.DefaultUsersConfigFileName);
            var installerUsersData = CodeHelpers.TryLoadDataFromXmlFile<AuthData>(installerUsersPath, true);
            var installerUsersVersion = installerUsersData != null ? installerUsersData.Version.ToString() : naStr;

            var installerAutoPath = Path.Combine(A.I.GetInstallerDataFolderPath(), SharedConstants.DefaultAutoDataFileName);
            var installerAutoData = CodeHelpers.TryLoadDataFromXmlFile<MainData>(installerAutoPath);
            var installerAutoVersion = installerAutoData != null ? installerAutoData.Version.ToString() : naStr;

            var installerAppVersion = CodeHelpers.GetAppVersion();

            var destinationUsersPath = Path.Combine(CurrentPath, SharedConstants.DefaultDataDirName, SharedConstants.DefaultUsersConfigFileName);
            var destinationAutoPath = Path.Combine(CurrentPath, SharedConstants.DefaultDataDirName, SharedConstants.DefaultAutoDataFileName);

            if (CurrentType == Type.Export)
            {
                destinationUsersPath = Path.Combine(CurrentPath, A.ExtractedFolderName, SharedConstants.DefaultDataDirName, SharedConstants.DefaultUsersConfigFileName);
                destinationAutoPath = Path.Combine(CurrentPath, A.ExtractedFolderName, SharedConstants.DefaultDataDirName, SharedConstants.DefaultAutoDataFileName);
            }

            var destinationUsersData = CodeHelpers.TryLoadDataFromXmlFile<AuthData>(destinationUsersPath, true);
            var destinationUsersVersion = destinationUsersData != null ? destinationUsersData.Version.ToString() : naStr;

            var destinationAutoData = CodeHelpers.TryLoadDataFromXmlFile<MainData>(destinationAutoPath);
            var destinationAutoVersion = destinationAutoData != null ? destinationAutoData.Version.ToString() : naStr;

            var destinationAppVersion = A.I.GetAppVersion(CurrentPath, SharedConstants.FactoryInstallerGuid);
            if (string.IsNullOrWhiteSpace(destinationAppVersion))
                destinationAppVersion = naStr;


            switch (CurrentType)
            {
                case Type.Import:
                    {
                        dgImport.Rows.Clear();
                        dgImport.Rows.Add("Список настроек авто", installerAutoVersion, destinationAutoVersion);
                        dgImport.Rows.Add("Список пользователей", installerUsersVersion, destinationUsersVersion);
                        dgImport.ClearSelection();
                        AutoSizeRows(dgImport);
                        break;
                    }
                case Type.Export:
                    {
                        dgExport.Rows.Clear();
                        dgExport.Rows.Add("Дистр. МИКАС Мастера", installerAppVersion, destinationAppVersion);
                        dgExport.Rows.Add("Список настроек авто", installerAutoVersion, destinationAutoVersion);
                        dgExport.Rows.Add("Список пользователей", installerUsersVersion, destinationUsersVersion);
                        dgExport.ClearSelection();
                        AutoSizeRows(dgExport);
                        break;
                    }
                case Type.Install:
                default:
                    break;
            }
        }

        private void bnInstall_Click(object sender, EventArgs e)
        {
            if (CurrentType == Type.Export && (!IsExportAuto && !IsExportDistr && !IsExportUser))
                MessageBox.Show(Current, "Выберите объект для экспорта");
            else if (CurrentType == Type.Import && (!IsImportAuto && !IsImportUser))
                MessageBox.Show(Current, "Выберите объект для импорта");
            else
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void AutoSizeRows(DataGridView grid)
        {
            foreach (DataGridViewRow row in grid.Rows)
            {
                row.Height = (grid.ClientRectangle.Height - grid.ColumnHeadersHeight) / grid.Rows.Count;
            }
        }

        private void bnDefaultPath_Click(object sender, EventArgs e)
        {
            CurrentPath = DefaultPath;
            UpdateCurrentPathInTextBox();
        }

        void UpdateCurrentPathInTextBox()
        {
            tbInstallPath.Text = CurrentPath;
            tbImportPath.Text = CurrentPath;
            tbExportPath.Text = CurrentPath;
        }

        private void bnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogEx openFolderDialogEx = new FolderBrowserDialogEx()
            {
                ShowNewFolderButton = true,
                ShowEditBox = true,
            };


            openFolderDialogEx.SelectedPath = CodeHelpers.FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Current, CurrentPath);
            SendKeys.Send("{TAB}{TAB}{RIGHT}"); //развернуть папку
            var diagResult = openFolderDialogEx.ShowDialog();
            if (diagResult == DialogResult.OK || diagResult == DialogResult.Yes)
            {
                CurrentPath = openFolderDialogEx.SelectedPath;
                UpdateCurrentPathInTextBox();
                UpdateVersions();
            }
        }

        private void tbInstallPath_TextChanged(object sender, EventArgs e)
        {
            CurrentPath = ((TextBox)sender).Text;
            UpdateVersions();
        }
    }
}
