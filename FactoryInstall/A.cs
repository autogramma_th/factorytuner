﻿using FactoryDataConfig;
using FactoryInstall.Properties;
using FactoryTuner.CommonEntities;
using Ionic.Zip;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FactoryInstall
{
    /// <summary>
    /// Main entry point
    /// </summary>
    public class A
    {
        #region I
        private static A instance;

        public static A I
        {
            get
            {
                if (instance == null)
                    instance = new A();
                return instance;
            }
        }

        internal void Init()
        {
            ;
        }
        #endregion

        private A()
        {
            Debug.WriteLine(nameof(A));

            InitTrace();
            Trace.TraceInformation(CodeHelpers.GetAppVersionString());
            CodeHelpers.SubscribeToUndandledExceptions();
            _installerExtractFolderPath = Path.Combine(Environment.CurrentDirectory, ExtractedFolderName);
        }

        public const string ExtractedFolderName = "extracted";

        private string _installerExtractFolderPath = ExtractedFolderName;
        private string _dataFolderName = SharedConstants.DefaultDataDirName;

        public bool ExtractDataFolderIfNotExtracted()
        {
            if (!Directory.Exists(GetInstallerDataFolderPath()))
            {
                return ExtractDataFolder();
            }
            else
                Trace.TraceInformation("Data folder exist");
            return true;
        }

        public string GetInstallerDataFolderPath()
        {
            return Path.Combine(_installerExtractFolderPath, _dataFolderName);
        }

        private bool ExtractDataFolder()
        {
            Trace.TraceInformation("Extract data folder");
            return ExtractZip(Resources.data);
        }

        private void RunExe(string path, string args = "", bool isModal = true)
        {
            Debug.WriteLine(nameof(RunExe) + ": " + path);

            var psi = new ProcessStartInfo(path)
            {
                Arguments = args,
                WorkingDirectory = Path.GetDirectoryName(path)
            };
            Debug.WriteLine("Working dir: " + psi.WorkingDirectory);
            var p = Process.Start(psi);
            if (isModal)
                p.WaitForExit();
        }

        public bool ExtractUserConfigFolder(string path = "")
        {
            Trace.TraceInformation("Extract user config folder");
            return ExtractZip(Resources.FactoryUserConfig, path);
        }

        public bool ExtractDataConfigFolder(string path = "")
        {
            Trace.TraceInformation("Extract data config folder");
            return ExtractZip(Resources.FactoryDataConfig, path);
        }

        public bool ExtractFactoryTunerFolder(string path = "")
        {
            Trace.TraceInformation("Extract data config folder");
            return ExtractZip(Resources.FactoryTuner, path);
        }

        public bool ExtractSimplifiedInstallerFolder(string path = "")
        {
            Trace.TraceInformation("Extract data config folder");
            //del old exe
            var oldExeName = "FactoryInstall.exe";
            var oldExePath = Path.Combine(path, oldExeName);
            try
            {
                if (File.Exists(oldExePath))
                    File.Delete(oldExePath);
            }
            catch { }
            return ExtractZip(Resources.FactoryInstall, path);
        }

        private bool ExtractZip(byte[] _zip, string path = "")
        {
            using (var ms = new MemoryStream(_zip))
            {
                using (ZipFile zip = ZipFile.Read(ms))
                {
                    var extractPath = _installerExtractFolderPath;
                    if (!string.IsNullOrEmpty(path))
                        extractPath = path;

                    foreach (ZipEntry e in zip.Entries)
                    {
                        try
                        {
                            e.Extract(extractPath, ExtractExistingFileAction.OverwriteSilently);
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError("Can't extract file: " + e.FileName);
                            Trace.TraceError(ex.ToString());
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public void RestoreDataFolder()
        {
            Trace.TraceInformation("Restore data folder");
            if (Directory.Exists(GetInstallerDataFolderPath()))
                Directory.Delete(GetInstallerDataFolderPath(), true);

            ExtractDataFolder();
            Trace.TraceInformation("Done");
        }

        private void InitTrace()
        {
            Debug.WriteLine(nameof(InitTrace));

            Trace.AutoFlush = true;
            var logFileName = "Log" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt";
            var logFilePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), logFileName);
            var _logFileStream = new FileStream(logFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, 4096, FileOptions.Asynchronous);
            var _streamWriter = new StreamWriter(_logFileStream);
            Trace.Listeners.Add(new TextWriterTraceListener(_streamWriter));
        }

        internal void RemoveExtractedWithoutData()
        {
            Debug.WriteLine(nameof(RemoveExtractedWithoutData));
            Trace.TraceInformation("Clean");
            try
            {
                var di = new DirectoryInfo(_installerExtractFolderPath);
                foreach (FileInfo file in di.GetFiles())
                {
                    try
                    {
                        file.Delete();
                    }
                    catch (Exception ex) { Trace.TraceWarning(ex.ToString()); }
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    if (dir.Name != _dataFolderName)
                    {
                        try
                        {
                            dir.Delete(true);
                        }
                        catch (Exception ex) { Trace.TraceWarning(ex.ToString()); }
                    }
                }
            }
            catch (Exception ex) { Trace.TraceWarning(ex.ToString()); }
        }

        internal void OpenEditUserUtil()
        {
            Trace.TraceInformation("Open edit user util");

            ExtractUserConfigFolder();
            RunExe(Path.Combine(_installerExtractFolderPath, AppName.FactoryUserConfig.ToString() + ".exe"));
        }

        internal void OpenEditDataUtil()
        {
            Trace.TraceInformation("Open edit data util");

            ExtractDataConfigFolder();
            RunExe(Path.Combine(_installerExtractFolderPath, AppName.FactoryDataConfig.ToString() + ".exe"));
        }

        internal void OpenEditFactoryTunerUtil()
        {
            Trace.TraceInformation("Open edit factorytuner util");

            ExtractFactoryTunerFolder();
            RunExe(Path.Combine(_installerExtractFolderPath, AppName.MIKAS_Master.ToString() + ".exe"), "c");
        }

        public string GetInstalledAppFolderPath(string installGuid)
        {
            string result = string.Empty;
            try
            {
                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(SharedConstants.UninstallRegKeyPath, false))
                {
                    string guidText = new Guid(installGuid).ToString("B");
                    RegistryKey key = parent.OpenSubKey(guidText, false);
                    result = (string)key.GetValue("FolderPath", string.Empty);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            return result;
        }

        public string GetAppVersion(string directoryFolder, string appGuid)
        {
            if (!Directory.Exists(directoryFolder))
                return string.Empty;

            foreach (var file in Directory.GetFiles(directoryFolder, "*.exe", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    var assembly = Assembly.Load(System.IO.File.ReadAllBytes(file));
                    var guidAttribute = assembly.GetCustomAttributes(typeof(GuidAttribute), true).FirstOrDefault();
                    if (guidAttribute != null)
                    {
                        var guid = (guidAttribute as GuidAttribute).Value;
                        if (guid == appGuid)
                        {
                            var versInfo = FileVersionInfo.GetVersionInfo(file);
                            return versInfo.FileVersion;
                        }
                    }
                }
                catch { }
            }
            return string.Empty;
        }

        public void SetPathsInConfig(string configFilePath, string dataFolderPath)
        {
            var configPath = configFilePath;
            var cfg = CodeHelpers.TryLoadDataFromXmlFile<Configuration>(configPath);
            if (cfg != null)
            {
                cfg.AuthDataLocation.Path = Path.Combine(dataFolderPath, SharedConstants.DefaultUsersConfigFileName);
                cfg.MainDataLocation.Path = Path.Combine(dataFolderPath, SharedConstants.DefaultAutoDataFileName);
                cfg.JournalLocation.Path = Path.Combine(dataFolderPath, SharedConstants.DefaultJournalFileName);
                CodeHelpers.TrySaveDataToXmlFile(cfg, configPath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="installGuid"></param>
        /// <returns>Version or empty</returns>
        public string GetInstalledAppVersion(string installGuid)
        {
            string result = string.Empty;
            try
            {
                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(SharedConstants.UninstallRegKeyPath, false))
                {
                    string guidText = new Guid(installGuid).ToString("B");
                    RegistryKey key = parent.OpenSubKey(guidText, false);
                    if (key == null)
                        return string.Empty;
                    result = (string)key.GetValue("Version", "1.0.0.0");
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return string.Empty;
            }
            return result;
        }

        public bool TryCreateUninstaller(string appName, string installGuid, string folderPath, String exeName)
        {
            var exePath = Path.Combine(folderPath, exeName);
            try
            {
                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(SharedConstants.UninstallRegKeyPath, true))
                {
                    if (parent == null)
                    {
                        throw new Exception("Uninstall registry key not found.");
                    }
                    try
                    {
                        RegistryKey key = null;

                        try
                        {
                            string guidText = new Guid(installGuid).ToString("B");
                            key = parent.OpenSubKey(guidText, true) ??
                                  parent.CreateSubKey(guidText);

                            if (key == null)
                            {
                                throw new Exception(String.Format("Unable to create uninstaller '{0}\\{1}'", SharedConstants.UninstallRegKeyPath, guidText));
                            }

                            key.SetValue("DisplayName", appName);
                            key.SetValue("ApplicationVersion", "1");
                            key.SetValue("Publisher", "Autogramma");
                            key.SetValue("DisplayIcon", exePath);
                            key.SetValue("DisplayVersion", "1");
                            key.SetValue("URLInfoAbout", "http://autogramma.ru");
                            key.SetValue("Contact", "http://autogramma.ru");
                            key.SetValue("InstallDate", DateTime.Now.ToString("yyyyMMdd"));
                            key.SetValue("UninstallString", exePath + " d");
                            key.SetValue("FolderPath", folderPath);
                            key.SetValue("Version", CodeHelpers.GetAppVersion());
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.ToString());
                            return false;
                        }
                        finally
                        {
                            if (key != null)
                            {
                                key.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.ToString());
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
            return true;
        }

        public void CreateDesktopShortcutLnk(string exePath, string shortcutName)
        {
            string deskDir = CodeHelpers.GetAllUsersDesktopFolderPath();
            var shell = new IWshRuntimeLibrary.WshShell();
            string shortcutAddress = deskDir + "\\" + shortcutName + "." + SharedConstants.ShortcutExtension;
            var shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutAddress);
            shortcut.Description = shortcutName;
            shortcut.IconLocation = exePath;
            shortcut.WorkingDirectory = Path.GetDirectoryName(exePath);
            shortcut.TargetPath = exePath;
            shortcut.Save();
        }
    }
}
