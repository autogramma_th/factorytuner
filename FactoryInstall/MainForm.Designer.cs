﻿namespace FactoryInstall
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bnEditUsers = new System.Windows.Forms.Button();
            this.bnEditData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.bnInstallFactoryTuner = new System.Windows.Forms.Button();
            this.bnImport = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbHelp = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbInstalledVersion = new System.Windows.Forms.Label();
            this.lbInstallerVersion = new System.Windows.Forms.Label();
            this.bnExport = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bnEditUsers
            // 
            this.bnEditUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bnEditUsers.Location = new System.Drawing.Point(17, 415);
            this.bnEditUsers.Margin = new System.Windows.Forms.Padding(4);
            this.bnEditUsers.Name = "bnEditUsers";
            this.bnEditUsers.Size = new System.Drawing.Size(406, 88);
            this.bnEditUsers.TabIndex = 2;
            this.bnEditUsers.TabStop = false;
            this.bnEditUsers.Text = "Подготовить \r\nсписок пользователей";
            this.bnEditUsers.UseVisualStyleBackColor = true;
            this.bnEditUsers.Click += new System.EventHandler(this.bnEditUsers_Click);
            this.bnEditUsers.Enter += new System.EventHandler(this.bnEditUsers_MouseEnter);
            this.bnEditUsers.Leave += new System.EventHandler(this.bn_MouseLeave);
            this.bnEditUsers.MouseEnter += new System.EventHandler(this.bnEditUsers_MouseEnter);
            this.bnEditUsers.MouseLeave += new System.EventHandler(this.bn_MouseLeave);
            // 
            // bnEditData
            // 
            this.bnEditData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bnEditData.Location = new System.Drawing.Point(17, 319);
            this.bnEditData.Margin = new System.Windows.Forms.Padding(4);
            this.bnEditData.Name = "bnEditData";
            this.bnEditData.Size = new System.Drawing.Size(406, 88);
            this.bnEditData.TabIndex = 1;
            this.bnEditData.TabStop = false;
            this.bnEditData.Text = "Состав автомобиля \r\n(шины, датч. скор., коэф. КПП)";
            this.bnEditData.UseVisualStyleBackColor = true;
            this.bnEditData.Click += new System.EventHandler(this.bnEditData_Click);
            this.bnEditData.Enter += new System.EventHandler(this.bnEditData_MouseEnter);
            this.bnEditData.Leave += new System.EventHandler(this.bn_MouseLeave);
            this.bnEditData.MouseEnter += new System.EventHandler(this.bnEditData_MouseEnter);
            this.bnEditData.MouseLeave += new System.EventHandler(this.bn_MouseLeave);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(13, 290);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 16, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(338, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Редактирование конфигураций:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // bnInstallFactoryTuner
            // 
            this.bnInstallFactoryTuner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bnInstallFactoryTuner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(242)))), ((int)(((byte)(234)))));
            this.bnInstallFactoryTuner.Location = new System.Drawing.Point(13, 46);
            this.bnInstallFactoryTuner.Margin = new System.Windows.Forms.Padding(4);
            this.bnInstallFactoryTuner.Name = "bnInstallFactoryTuner";
            this.bnInstallFactoryTuner.Size = new System.Drawing.Size(406, 224);
            this.bnInstallFactoryTuner.TabIndex = 0;
            this.bnInstallFactoryTuner.TabStop = false;
            this.bnInstallFactoryTuner.Text = "Установка МИКАС Мастер";
            this.bnInstallFactoryTuner.UseVisualStyleBackColor = false;
            this.bnInstallFactoryTuner.Click += new System.EventHandler(this.bnInstallFactoryTuner_Click);
            this.bnInstallFactoryTuner.Enter += new System.EventHandler(this.bnInstallFactoryTuner_MouseEnter);
            this.bnInstallFactoryTuner.Leave += new System.EventHandler(this.bn_MouseLeave);
            this.bnInstallFactoryTuner.MouseEnter += new System.EventHandler(this.bnInstallFactoryTuner_MouseEnter);
            this.bnInstallFactoryTuner.MouseLeave += new System.EventHandler(this.bn_MouseLeave);
            // 
            // bnImport
            // 
            this.bnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bnImport.Location = new System.Drawing.Point(18, 642);
            this.bnImport.Margin = new System.Windows.Forms.Padding(4);
            this.bnImport.Name = "bnImport";
            this.bnImport.Size = new System.Drawing.Size(406, 88);
            this.bnImport.TabIndex = 3;
            this.bnImport.TabStop = false;
            this.bnImport.Text = "Импорт актуальных баз данных";
            this.bnImport.UseVisualStyleBackColor = true;
            this.bnImport.TabStopChanged += new System.EventHandler(this.bnImport_MouseEnter);
            this.bnImport.Click += new System.EventHandler(this.bnImport_Click);
            this.bnImport.Enter += new System.EventHandler(this.bnImport_MouseEnter);
            this.bnImport.Leave += new System.EventHandler(this.bn_MouseLeave);
            this.bnImport.MouseEnter += new System.EventHandler(this.bnImport_MouseEnter);
            this.bnImport.MouseLeave += new System.EventHandler(this.bn_MouseLeave);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbHelp);
            this.groupBox1.Location = new System.Drawing.Point(442, 404);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(712, 330);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Помощь";
            // 
            // tbHelp
            // 
            this.tbHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbHelp.Location = new System.Drawing.Point(4, 27);
            this.tbHelp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbHelp.Name = "tbHelp";
            this.tbHelp.Size = new System.Drawing.Size(704, 299);
            this.tbHelp.TabIndex = 0;
            this.tbHelp.Text = resources.GetString("tbHelp.Text");
            this.tbHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Установка:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(12, 517);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Сервис:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::FactoryInstall.Properties.Resources.img_none;
            this.pictureBox1.Location = new System.Drawing.Point(442, 19);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(730, 303);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // lbInstalledVersion
            // 
            this.lbInstalledVersion.Location = new System.Drawing.Point(778, 327);
            this.lbInstalledVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbInstalledVersion.Name = "lbInstalledVersion";
            this.lbInstalledVersion.Size = new System.Drawing.Size(394, 73);
            this.lbInstalledVersion.TabIndex = 10;
            this.lbInstalledVersion.Text = "Установленный \r\nМИКАС Мастер: v1.0.0.0";
            this.lbInstalledVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbInstallerVersion
            // 
            this.lbInstallerVersion.Location = new System.Drawing.Point(453, 327);
            this.lbInstallerVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbInstallerVersion.Name = "lbInstallerVersion";
            this.lbInstallerVersion.Size = new System.Drawing.Size(330, 73);
            this.lbInstallerVersion.TabIndex = 11;
            this.lbInstallerVersion.Text = "В дистрибутиве \r\nМИКАС Мастер: v1.0.0.0";
            this.lbInstallerVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bnExport
            // 
            this.bnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bnExport.Location = new System.Drawing.Point(18, 546);
            this.bnExport.Margin = new System.Windows.Forms.Padding(4);
            this.bnExport.Name = "bnExport";
            this.bnExport.Size = new System.Drawing.Size(406, 88);
            this.bnExport.TabIndex = 12;
            this.bnExport.TabStop = false;
            this.bnExport.Text = "Экспорт на сервер";
            this.bnExport.UseVisualStyleBackColor = true;
            this.bnExport.TabStopChanged += new System.EventHandler(this.bnExport_MouseEnter);
            this.bnExport.Click += new System.EventHandler(this.bnExport_Click);
            this.bnExport.Enter += new System.EventHandler(this.bnExport_MouseEnter);
            this.bnExport.Leave += new System.EventHandler(this.bn_MouseLeave);
            this.bnExport.MouseEnter += new System.EventHandler(this.bnExport_MouseEnter);
            this.bnExport.MouseLeave += new System.EventHandler(this.bn_MouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1173, 753);
            this.Controls.Add(this.bnExport);
            this.Controls.Add(this.lbInstallerVersion);
            this.Controls.Add(this.lbInstalledVersion);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bnImport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bnEditUsers);
            this.Controls.Add(this.bnEditData);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bnInstallFactoryTuner);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1191, 800);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "МИКАС Менеджер";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button bnEditUsers;
        private System.Windows.Forms.Button bnEditData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bnInstallFactoryTuner;
        private System.Windows.Forms.Button bnImport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label tbHelp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbInstalledVersion;
        private System.Windows.Forms.Label lbInstallerVersion;
        private System.Windows.Forms.Button bnExport;
    }
}

