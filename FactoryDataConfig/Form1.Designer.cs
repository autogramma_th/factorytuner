﻿namespace FactoryDataConfig
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label4 = new System.Windows.Forms.Label();
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.bnSave = new System.Windows.Forms.Button();
            this.bnSaveAs = new System.Windows.Forms.Button();
            this.bnOpen = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.treeListView1 = new BrightIdeasSoftware.TreeListView();
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.bnMoveUp = new System.Windows.Forms.Button();
            this.bnMoveDown = new System.Windows.Forms.Button();
            this.bnRemove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // tbVersion
            // 
            resources.ApplyResources(this.tbVersion, "tbVersion");
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.ReadOnly = true;
            this.tbVersion.Validating += new System.ComponentModel.CancelEventHandler(this.tbVersion_Validating);
            // 
            // bnSave
            // 
            resources.ApplyResources(this.bnSave, "bnSave");
            this.bnSave.Name = "bnSave";
            this.bnSave.UseVisualStyleBackColor = true;
            this.bnSave.Click += new System.EventHandler(this.bnSave_Click);
            // 
            // bnSaveAs
            // 
            resources.ApplyResources(this.bnSaveAs, "bnSaveAs");
            this.bnSaveAs.Name = "bnSaveAs";
            this.bnSaveAs.UseVisualStyleBackColor = true;
            this.bnSaveAs.Click += new System.EventHandler(this.bnSaveAs_Click);
            // 
            // bnOpen
            // 
            resources.ApplyResources(this.bnOpen, "bnOpen");
            this.bnOpen.Name = "bnOpen";
            this.bnOpen.UseVisualStyleBackColor = true;
            this.bnOpen.Click += new System.EventHandler(this.bnOpen_Click);
            // 
            // openFileDialog1
            // 
            resources.ApplyResources(this.openFileDialog1, "openFileDialog1");
            // 
            // saveFileDialog1
            // 
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // treeListView1
            // 
            this.treeListView1.AllColumns.Add(this.olvColumn3);
            this.treeListView1.AllColumns.Add(this.olvColumn4);
            resources.ApplyResources(this.treeListView1, "treeListView1");
            this.treeListView1.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.treeListView1.CellEditEnterChangesRows = true;
            this.treeListView1.CellEditTabChangesRows = true;
            this.treeListView1.CellEditUseWholeCell = false;
            this.treeListView1.CellVerticalAlignment = System.Drawing.StringAlignment.Near;
            this.treeListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn3,
            this.olvColumn4});
            this.treeListView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView1.FullRowSelect = true;
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.ShowGroups = false;
            this.treeListView1.UseCompatibleStateImageBehavior = false;
            this.treeListView1.View = System.Windows.Forms.View.Details;
            this.treeListView1.VirtualMode = true;
            this.treeListView1.ModelCanDrop += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.treeListView1_ModelCanDrop);
            this.treeListView1.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.treeListView1_ModelDropped);
            this.treeListView1.SelectionChanged += new System.EventHandler(this.treeListView1_SelectionChanged);
            this.treeListView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.treeListView1_ItemSelectionChanged);
            this.treeListView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeListView1_KeyDown);
            // 
            // olvColumn3
            // 
            this.olvColumn3.FillsFreeSpace = true;
            resources.ApplyResources(this.olvColumn3, "olvColumn3");
            // 
            // olvColumn4
            // 
            this.olvColumn4.FillsFreeSpace = true;
            resources.ApplyResources(this.olvColumn4, "olvColumn4");
            // 
            // bnMoveUp
            // 
            resources.ApplyResources(this.bnMoveUp, "bnMoveUp");
            this.bnMoveUp.Name = "bnMoveUp";
            this.bnMoveUp.UseVisualStyleBackColor = true;
            this.bnMoveUp.Click += new System.EventHandler(this.bnMoveUp_Click);
            // 
            // bnMoveDown
            // 
            resources.ApplyResources(this.bnMoveDown, "bnMoveDown");
            this.bnMoveDown.Name = "bnMoveDown";
            this.bnMoveDown.UseVisualStyleBackColor = true;
            this.bnMoveDown.Click += new System.EventHandler(this.bnMoveDown_Click);
            // 
            // bnRemove
            // 
            resources.ApplyResources(this.bnRemove, "bnRemove");
            this.bnRemove.Name = "bnRemove";
            this.bnRemove.UseVisualStyleBackColor = true;
            this.bnRemove.Click += new System.EventHandler(this.bnRemove_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bnRemove);
            this.Controls.Add(this.bnMoveDown);
            this.Controls.Add(this.bnMoveUp);
            this.Controls.Add(this.treeListView1);
            this.Controls.Add(this.bnOpen);
            this.Controls.Add(this.bnSaveAs);
            this.Controls.Add(this.bnSave);
            this.Controls.Add(this.tbVersion);
            this.Controls.Add(this.label4);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.Button bnSave;
        private System.Windows.Forms.Button bnSaveAs;
        private System.Windows.Forms.Button bnOpen;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private BrightIdeasSoftware.TreeListView treeListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private System.Windows.Forms.Button bnMoveUp;
        private System.Windows.Forms.Button bnMoveDown;
        private System.Windows.Forms.Button bnRemove;
    }
}

