﻿using FactoryDataConfig.Properties;
using FactoryTuner.CommonEntities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FactoryDataConfig
{
    /// <summary>
    /// Main entry point
    /// </summary>
    public class A
    {
        #region I
        private static A instance;

        public static A I
        {
            get
            {
                if (instance == null)
                    instance = new A();
                return instance;
            }
        }

        internal void Init()
        {
            ;
        }
        #endregion

        public MainData CurrentLoadedMainData { get; private set; } = new MainData();
        public event Action OnMainDataLoaded = delegate { };

        public string OpenedDocumentPath { get; private set; }
        public event Action OnOpenDocumentPathChanged = delegate { };

        public string DefaultFilePath { get; private set; }

        private A()
        {
            Debug.WriteLine(nameof(A));

            InitTrace();
            Trace.TraceInformation(CodeHelpers.GetAppVersionString());
            CodeHelpers.SubscribeToUndandledExceptions();
            DefaultFilePath = Path.Combine(Environment.CurrentDirectory, SharedConstants.DefaultDataDirName, SharedConstants.DefaultAutoDataFileName);
        }

        public bool TrySaveMainDataToFile(string filePath)
        {
            Debug.WriteLine(nameof(TrySaveMainDataToFile));

            CurrentLoadedMainData.Version++;
            return CodeHelpers.TrySaveDataToXmlFile(CurrentLoadedMainData, filePath);
        }

        public bool TryLoadMainDataFromFile(string filePath)
        {
            Debug.WriteLine(nameof(TryLoadMainDataFromFile));

            var result = CodeHelpers.TryLoadDataFromXmlFile<MainData>(filePath);

            if (result != null)
            {
                CurrentLoadedMainData = result;
                OnMainDataLoaded.Invoke();
                OpenedDocumentPath = filePath;
                OnOpenDocumentPathChanged();
                return true;
            }
            return false;
        }

        private void InitTrace()
        {
            Debug.WriteLine(nameof(InitTrace));

            Trace.AutoFlush = true;
            var logFileName = "Log" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt";
            var logFilePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), logFileName);
            var _logFileStream = new FileStream(logFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, 4096, FileOptions.Asynchronous);
            var _streamWriter = new StreamWriter(_logFileStream);
            Trace.Listeners.Add(new TextWriterTraceListener(_streamWriter));
        }
    }
}
