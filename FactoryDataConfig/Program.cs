﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace FactoryDataConfig
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        static Mutex mutex = new Mutex(false, SharedConstants.FactoryDataCfgGuid.ToString());
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SetProcessDPIAware();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length > 0 && (args[0] == "d"))
            {
                CodeHelpers.DeleteSelfExeAndShortcut(SharedConstants.FactoryDataCfgGuid, SharedConstants.FactoryAutoShortkutName);
                Application.Exit();
            }
            else
                RunForm(() => Application.Run(new Form1()));
        }

        static void RunForm(Action createFormAction)
        {
            // if you like to wait a few seconds in case that the instance is just 
            // shutting down
            if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
            {
                CodeHelpers.SwitchToAlreadyOpenedApp();
                return;
            }

            try
            {
                createFormAction();
            }
            finally { mutex.ReleaseMutex(); } // I find this more explicit
        }
    }
}
