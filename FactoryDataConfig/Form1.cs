﻿using BrightIdeasSoftware;
using FactoryDataConfig.Properties;
using FactoryTuner.CommonEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FactoryDataConfig
{
    public partial class Form1 : Form
    {
        private List<string> _transmissionsTypes = new List<string>();

        private TreeViewNode tiresRoot;
        private TreeViewNode transRoot;
        private TreeViewNode gpmRoot;

        public static Form1 Current;

        public Form1()
        {
            Current = this;
            InitializeComponent();
            bnSave.Enabled = false;
            this.treeListView1.CanExpandGetter = delegate (object x) { return ((TreeViewNode)x).Children.Count > 0; };
            this.treeListView1.ChildrenGetter = delegate (object x) { return ((TreeViewNode)x).Children; };

            //header color
            var headerStyle = new HeaderFormatStyle();
            headerStyle.SetBackColor(Color.FromArgb(240, 240, 240));
            treeListView1.HeaderFormatStyle = headerStyle;
            //
            treeListView1.CellEditStarting += TreeListView1_CellEditStarting; //fix edit offset
            treeListView1.AllColumns[0].AspectPutter = treeListView1_Col0Put;
            treeListView1.AllColumns[1].AspectPutter = treeListView1_Col1Put;
            treeListView1.CellEditUseWholeCell = true;
            treeListView1.AllColumns.ForEach(x => x.Sortable = false);
            treeListView1.AllColumns.ForEach(x => x.AutoCompleteEditor = false);
            treeListView1.AddDecoration(new EditingCellBorderDecoration { UseLightbox = true, CornerRounding = 0f, BoundsPadding = new Size(0, 0) });
            treeListView1.AllColumns[0].AspectGetter = delegate (object x) { return ((TreeViewNode)x).ObjectName; };
            treeListView1.AllColumns[1].AspectGetter = delegate (object x) { return ((TreeViewNode)x).ParameterValue; };
            //var dropSink = (SimpleDropSink)treeListView1.DropSink;
            //dropSink.CanDropBetween = true;
            //dropSink.CanDropOnItem = false;
            UpdateEditButtons();
        }

        private void TreeListView1_CellEditStarting(object sender, CellEditEventArgs e)
        {
            //fix edit offset: https://stackoverflow.com/questions/41363661/c-sharp-objectlistview-undefined-offset-in-cell-editor
            if (e.Column != treeListView1.AllColumns[0])
            {
                e.Control.Bounds = e.CellBounds;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(Form1_Load));

            A.I.Init();
            LoadTransmissionTypes();
            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);
            this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();
            A.I.OnOpenDocumentPathChanged += OpenDocumentPathChanged;
            A.I.OnMainDataLoaded += MainDataLoaded;

            if (Directory.Exists(Path.GetDirectoryName(A.I.DefaultFilePath)))
            {
                openFileDialog1.InitialDirectory = Path.GetDirectoryName(A.I.DefaultFilePath);
                saveFileDialog1.InitialDirectory = Path.GetDirectoryName(A.I.DefaultFilePath);
                A.I.TryLoadMainDataFromFile(A.I.DefaultFilePath);
            }
        }

        private void MainDataLoaded()
        {
            Debug.WriteLine(nameof(MainDataLoaded));

            FillTableGridFromData();
        }

        private void OpenDocumentPathChanged()
        {
            Debug.WriteLine(nameof(OpenDocumentPathChanged));

            //tbFilePath.Text = A.I.OpenedDocumentPath;
            bnSave.Enabled = !string.IsNullOrEmpty(A.I.OpenedDocumentPath);
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnSave_Click));

            FillDataFromTableGrid();
            if (!string.IsNullOrEmpty(A.I.OpenedDocumentPath))
            {
                if (A.I.TrySaveMainDataToFile(A.I.OpenedDocumentPath))
                {
                    MessageBox.Show(Current, Resources.strSaved);
                    A.I.TryLoadMainDataFromFile(A.I.OpenedDocumentPath);
                }
                else
                    MessageBox.Show(Current, Resources.strNotSaved + "\n" + Resources.strSeeLogs);
            }
        }

        private void bnSaveAs_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnSaveAs_Click));

            FillDataFromTableGrid();
            saveFileDialog1.InitialDirectory = CodeHelpers.FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Current, saveFileDialog1.InitialDirectory);
            var dialogResult = saveFileDialog1.ShowDialog();
            if ((dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes) && !string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                if (A.I.TrySaveMainDataToFile(saveFileDialog1.FileName))
                {
                    MessageBox.Show(Current, Resources.strSaved);
                    A.I.TryLoadMainDataFromFile(saveFileDialog1.FileName);
                    openFileDialog1.InitialDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                    saveFileDialog1.InitialDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                }
                else
                {
                    MessageBox.Show(Current, Resources.strNotSaved + "\n" + Resources.strSeeLogs);
                }
            }
        }

        private void bnOpen_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnOpen_Click));

            openFileDialog1.InitialDirectory = CodeHelpers.FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Current, openFileDialog1.InitialDirectory);
            var dialogResult = openFileDialog1.ShowDialog();
            if ((dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes) && !string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                if (!A.I.TryLoadMainDataFromFile(openFileDialog1.FileName))
                    MessageBox.Show(Current, Resources.strNotOpened + "\n" + Resources.strSeeLogs);
                else
                {
                    openFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                    saveFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                }
            }
        }

        private void FillTableGridFromData()
        {
            Debug.WriteLine(nameof(FillTableGridFromData));

            //Tires
            var tireTree = new List<TreeViewNode>();
            tiresRoot = new TreeViewNode(null, "Шины", string.Empty, tireTree);
            foreach (var tiresBySize in A.I.CurrentLoadedMainData.TireModels.GroupBy(x => x.Size))
            {
                var child = new List<TreeViewNode>();
                var tireGroupRoot = new TreeViewNode(tiresRoot, tiresBySize.Key, string.Empty, child) { IsEditable = true };
                tireTree.Add(tireGroupRoot);
                foreach (var tire in tiresBySize)
                {
                    child.Add(new TreeViewNode(tireGroupRoot, tire.Name, tire.DR.ToString()) { IsEditable = true });
                }
                child.Add(new TreeViewNode(tireGroupRoot, "*", "*") { IsEditable = true, IsNewRow = true });
            }
            tiresRoot.Children.Add(new TreeViewNode(tiresRoot, "*", string.Empty) { IsEditable = true, IsNewRow = true, NewRowSubchildCount = 1 });


            //Trans
            var transTree = new List<TreeViewNode>();
            transRoot = new TreeViewNode(null, "Расположение датчика скорости", string.Empty, transTree);
            foreach (var transByType in A.I.CurrentLoadedMainData.Transmissions.GroupBy(x => x.Type))
            {
                var child = new List<TreeViewNode>();
                var transGroupRoot = new TreeViewNode(transRoot, _transmissionsTypes[(int)transByType.Key], string.Empty, child);
                transTree.Add(transGroupRoot);
                foreach (var trans in transByType)
                {
                    child.Add(new TreeViewNode(transGroupRoot, trans.Name, trans.N.ToString()) { IsEditable = true });
                }
                child.Add(new TreeViewNode(transGroupRoot, "*", "*") { IsEditable = true, IsNewRow = true });
            }


            //GPM
            var gpmTree = new List<TreeViewNode>();
            gpmRoot = new TreeViewNode(null, "Передаточное отношение", string.Empty, gpmTree);
            foreach (var gpm in A.I.CurrentLoadedMainData.GPM)
                gpmTree.Add(new TreeViewNode(gpmRoot, gpm.ToString(), gpm.ToString()) { IsEditable = true });
            gpmTree.Add(new TreeViewNode(gpmRoot, "*", "*") { IsEditable = true, IsNewRow = true });

            treeListView1.Roots = new ArrayList { tiresRoot, transRoot, gpmRoot };

            tbVersion.Text = A.I.CurrentLoadedMainData.Version.ToString();
        }

        private void FillDataFromTableGrid()
        {
            Debug.WriteLine(nameof(FillDataFromTableGrid));

            A.I.CurrentLoadedMainData.GPM.Clear();
            foreach (var gpm in gpmRoot.Children)
                if (!gpm.IsNewRow)
                {
                    if (float.TryParse(gpm.ObjectName, out float gpmValue))
                        A.I.CurrentLoadedMainData.GPM.Add(gpmValue);
                }

            A.I.CurrentLoadedMainData.Transmissions.Clear();
            foreach (var transType in transRoot.Children)
                if (!transType.IsNewRow)
                {
                    foreach (var trans in transType.Children)
                    {
                        if (!trans.IsNewRow)
                        {
                            var transTypeNum = (uint)_transmissionsTypes.IndexOf(transType.ObjectName);
                            if (float.TryParse(trans.ParameterValue, out float transValue))
                                A.I.CurrentLoadedMainData.Transmissions.Add(new Transmission(transTypeNum, trans.ObjectName, transValue));
                        }
                    }
                }

            A.I.CurrentLoadedMainData.TireModels.Clear();
            foreach (var tireSize in tiresRoot.Children)
                if (!tireSize.IsNewRow)
                {
                    foreach (var tire in tireSize.Children)
                    {
                        if (!tire.IsNewRow)
                        {
                            if (float.TryParse(tire.ParameterValue, out float tireValue))
                                A.I.CurrentLoadedMainData.TireModels.Add(new TireModel(tireSize.ObjectName, tire.ObjectName, tireValue));
                        }
                    }
                }

            A.I.CurrentLoadedMainData.Version = UInt32.Parse(tbVersion.Text);
        }

        private void OnGridViewFormatError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Debug.WriteLine(nameof(OnGridViewFormatError));

            MessageBox.Show(Current, Resources.strBedInputFormat);
        }

        private void tbVersion_Validating(object sender, CancelEventArgs e)
        {
            tbVersion.Text = Regex.Replace(tbVersion.Text, "[^0-9]", "");
            uint temp;
            if (!UInt32.TryParse(tbVersion.Text, out temp))
                tbVersion.Text = A.I.CurrentLoadedMainData.Version.ToString();
        }

        private void LoadTransmissionTypes()
        {
            _transmissionsTypes.Add("-");
            _transmissionsTypes.Add(Resources.strTrans0);
            _transmissionsTypes.Add(Resources.strTrans1);
            _transmissionsTypes.Add(Resources.strTrans2);
        }

        private void treeListView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelected();
            }
        }

        private void RemoveSelected()
        {
            foreach (var selected in treeListView1.SelectedObjects)
            {
                var selectedNode = (TreeViewNode)selected;
                if (!selectedNode.IsNewRow && selectedNode.IsEditable)
                {
                    selectedNode.Parent.Children.Remove(selectedNode);
                    treeListView1.RemoveObject(selectedNode);
                    selectedNode.Parent = null;
                }
            }
            treeListView1.Refresh();
        }

        private void treeListView1_ModelCanDrop(object sender, ModelDropEventArgs e)
        {
            e.Handled = true;
            e.Effect = DragDropEffects.None;
            var sourceModels = e.SourceModels.Cast<TreeViewNode>();
            sourceModels = sourceModels.Where(x => !x.IsNewRow);
            TreeViewNode target = e.TargetModel as TreeViewNode;

            if (sourceModels.Any(x => !x.IsEditable))
                e.InfoMessage = "Выбран неперемещаемый";
            else
            {
                if (sourceModels.All(x => x.Parent == target.Parent))
                {
                    e.Effect = DragDropEffects.Move;
                }
                else
                    e.InfoMessage = "Перемещать только в своей ветке";
            }
        }

        private void treeListView1_ModelDropped(object sender, ModelDropEventArgs e)
        {
            Debug.WriteLine(e.DropTargetLocation);
            if (e.DropTargetLocation == DropTargetLocation.AboveItem ||
                e.DropTargetLocation == DropTargetLocation.BelowItem)
            {
                var sourceModels = e.SourceModels.Cast<TreeViewNode>();
                sourceModels = sourceModels.Where(x => !x.IsNewRow);
                TreeViewNode target = e.TargetModel as TreeViewNode;
                target.Parent.Children = target.Parent.Children.Except(sourceModels).ToList();
                var insertIndex = target.Parent.Children.IndexOf(target);
                if (e.DropTargetLocation == DropTargetLocation.BelowItem)
                    insertIndex++;

                if (insertIndex == target.Parent.Children.Count)
                    insertIndex--;

                try //глючит, когда дроп на тоже положение что и элемент
                {
                    target.Parent.Children.InsertRange(insertIndex, sourceModels);
                }
                catch { }

                e.RefreshObjects();
            }
        }

        private void treeListView1_Col0Put(object x, object _newValue)
        {
            var newValue = (string)_newValue;
            if (!string.IsNullOrWhiteSpace(newValue))
            {
                var item = ((TreeViewNode)x);

                if (item.IsNewRow && newValue != "*")
                {
                    item.ObjectName = newValue;
                    item.IsEditable = true;
                    item.IsNewRow = false;
                    var newItem = new TreeViewNode(item.Parent, "*", "*") { IsEditable = true, IsNewRow = true, NewRowSubchildCount = item.NewRowSubchildCount };
                    item.Parent.Children.Insert(item.Parent.Children.Count, newItem);
                    //Добавить поддерево
                    AddSubTreeIfNeed(item);
                    treeListView1.RefreshObjects(item.Parent.Children);
                }
                else if (item.IsEditable)
                {
                    item.ObjectName = newValue;
                }
            }
        }

        private void treeListView1_Col1Put(object x, object _newValue)
        {
            var newValue = (string)_newValue;
            if (!string.IsNullOrWhiteSpace(newValue))
            {
                var item = ((TreeViewNode)x);

                if (item.IsNewRow && newValue != "*")
                {
                    item.ParameterValue = newValue;
                    item.IsEditable = true;
                    item.IsNewRow = false;
                    var newItem = new TreeViewNode(item.Parent, "*", "*") { IsEditable = true, IsNewRow = true, NewRowSubchildCount = item.NewRowSubchildCount };
                    item.Parent.Children.Insert(item.Parent.Children.Count, newItem);
                    //Добавить поддерево
                    AddSubTreeIfNeed(item);
                    treeListView1.RefreshObjects(item.Parent.Children);
                }
                else if (item.IsEditable)
                {
                    item.ParameterValue = newValue;
                }
            }
        }

        private void UpdateEditButtons()
        {
            var selected = treeListView1.SelectedObjects.Cast<TreeViewNode>();

            bool isSelectedAny = (treeListView1.SelectedObjects.Count != 0);

            if (isSelectedAny)
            {
                bool isHaveChildren = selected.All(x => x.Children.Count != 0);
                bool isSelectedOnlyOne = (treeListView1.SelectedObjects.Count == 1);
                bool isHaveNewRowInNode = selected.First().Parent != null && selected.First().Parent.Children.Exists(x => x.IsNewRow);
                bool isHaveNewRowInChildNode = selected.First().Children.Exists(x => x.IsNewRow);
                bool isEditable = selected.All(x => x.IsEditable);
                bool isSelectedOnlyNewRow = selected.All(x => x.IsNewRow);

                //bnAddNear.Enabled = isHaveNewRowInNode;
                bnRemove.Enabled = isEditable && !isSelectedOnlyNewRow;
                bnMoveUp.Enabled = isEditable && !isSelectedOnlyNewRow;
                bnMoveDown.Enabled = isEditable && !isSelectedOnlyNewRow;
                //bnAddChild.Enabled = isSelectedOnlyOne && isHaveNewRowInChildNode && !isSelectedOnlyNewRow;
            }
            else
            {
                //bnAddNear.Enabled = false;
                bnRemove.Enabled = false;
                bnMoveUp.Enabled = false;
                bnMoveDown.Enabled = false;
                //bnAddChild.Enabled = false;
            }
        }

        private void treeListView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            UpdateEditButtons();
        }

        private void bnRemove_Click(object sender, EventArgs e)
        {
            RemoveSelected();
            treeListView1.Focus();
        }

        private void bnAddNear_Click(object sender, EventArgs e)
        {
            var selected = treeListView1.SelectedObjects.Cast<TreeViewNode>();
            if (selected.First().Parent != null && selected.First().Parent.Children.Exists(x => x.IsNewRow))
            {
                //перед первым выделенным вставить
                var parent = selected.First().Parent;
                var newItem = new TreeViewNode(parent, string.Empty, string.Empty) { IsEditable = true };
                parent.Children.Insert(parent.Children.IndexOf(selected.First()), newItem);
                //Добавить поддерево
                AddSubTreeIfNeed(newItem);
                treeListView1.RefreshObjects(parent.Children);
                //выделить новый
                treeListView1.SelectObject(newItem);
                //вкл редактирование
                treeListView1.EnsureModelVisible(newItem);
                treeListView1.EditModel(newItem);
            }
        }

        private void bnMoveUp_Click(object sender, EventArgs e)
        {
            var selected = treeListView1.SelectedObjects.Cast<TreeViewNode>();
            if (selected.Count() > 0)
            {
                var parent = selected.First().Parent;
                if (parent != null)
                {
                    //взять каждый выделенный
                    foreach (var sel in selected)
                    {
                        if (sel.IsEditable && !sel.IsNewRow)
                        {
                            var curIndex = parent.Children.IndexOf(sel);
                            if (curIndex > 0)
                            {
                                //поменять местами с тем, кто выше, если тот кто выше не выделен
                                if (!selected.Contains(parent.Children[curIndex - 1]))
                                {
                                    parent.Children.Remove(sel);
                                    parent.Children.Insert(curIndex - 1, sel);
                                    treeListView1.RefreshObjects(parent.Children);
                                }
                            }
                        }
                    }
                }
                treeListView1.SelectObjects(selected.ToList());
                treeListView1.EnsureModelVisible(selected.First());
                treeListView1.Focus();
            }
        }

        private void bnMoveDown_Click(object sender, EventArgs e)
        {
            var selected = treeListView1.SelectedObjects.Cast<TreeViewNode>();
            if (selected.Count() > 0)
            {
                var parent = selected.First().Parent;
                if (parent != null)
                {
                    //взять каждый выделенный
                    foreach (var sel in selected)
                    {
                        if (sel.IsEditable && !sel.IsNewRow)
                        {
                            var curIndex = parent.Children.IndexOf(sel);
                            if (curIndex < parent.Children.Count() - 1)
                            {
                                //поменять местами с тем, кто ниже, если тот кто ниже не выделен и не isNewRow
                                var next = parent.Children[curIndex + 1];
                                if (!next.IsNewRow && !selected.Contains(next))
                                {
                                    parent.Children.Remove(sel);
                                    parent.Children.Insert(curIndex + 1, sel);
                                    treeListView1.RefreshObjects(parent.Children);
                                }
                            }
                        }
                    }
                }
                treeListView1.SelectObjects(selected.ToList());
                treeListView1.EnsureModelVisible(selected.Last());
                treeListView1.Focus();
            }
        }

        private void bnAddChild_Click(object sender, EventArgs e)
        {
            var selected = treeListView1.SelectedObjects.Cast<TreeViewNode>();
            if (selected.Count() == 1)
            {
                var first = selected.First();
                if (!first.IsNewRow && first.Children.Exists(x => x.IsNewRow))
                {
                    var newItem = new TreeViewNode(first, string.Empty, string.Empty) { IsEditable = true };
                    first.Children.Insert(first.Children.Count() - 1, newItem);

                    //Добавить поддерево
                    AddSubTreeIfNeed(newItem);

                    treeListView1.RefreshObjects(first.Children);
                    //выделить новый
                    treeListView1.SelectObject(newItem);
                    //вкл редактирование
                    treeListView1.EnsureModelVisible(newItem);
                    treeListView1.EditModel(newItem);
                }
            }
        }

        private void AddSubTreeIfNeed(TreeViewNode item)
        {
            var newRow = item.Parent.Children.FirstOrDefault(x => x.IsNewRow);
            if (newRow != null)
            {
                TreeViewNode tempPrev = item;
                for (int i = newRow.NewRowSubchildCount; i > 0; i--)
                {
                    tempPrev = new TreeViewNode(tempPrev, "*", "*") { IsEditable = true, IsNewRow = true, NewRowSubchildCount = i - 1 };
                    item.Children.Add(tempPrev);
                }
            }
        }

        private void treeListView1_SelectionChanged(object sender, EventArgs e)
        {
            //Мультиселект только в одной ноде
            if (treeListView1.SelectedObjects.Count > 1)
            {
                var first = (TreeViewNode)treeListView1.SelectedObjects[0];
                foreach (var sel in treeListView1.SelectedObjects)
                {
                    var selObj = (TreeViewNode)sel;
                    if (selObj.Parent != first.Parent)
                        treeListView1.Items[treeListView1.IndexOf(selObj)].Selected = false;
                }
            }
        }
    }
}
