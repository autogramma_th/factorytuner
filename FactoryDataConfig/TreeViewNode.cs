﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryDataConfig
{
    class TreeViewNode
    {
        public TreeViewNode(TreeViewNode parent, string objectName, string parameterValue, List<TreeViewNode> children = null)
        {
            ObjectName = objectName;
            ParameterValue = parameterValue;
            Children = children;
            Parent = parent;
            if (children == null)
                Children = new List<TreeViewNode>();
        }

        public bool IsEditable { get; set; }
        public string ObjectName { get; set; }
        public string ParameterValue { get; set; }
        public bool IsNewRow { get; set; }
        public int NewRowSubchildCount { get; set; }
        public TreeViewNode Parent { get; set; }
        public List<TreeViewNode> Children { get; set; }
    }
}
