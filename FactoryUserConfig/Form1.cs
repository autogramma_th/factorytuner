﻿using FactoryDataConfig;
using FactoryTuner.CommonEntities;
using FactoryUserConfig.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FactoryUserConfig
{
    public partial class Form1 : Form
    {
        public static Form1 Current;

        public Form1()
        {
            Current = this;
            InitializeComponent();

            dataGridView1.Columns[1].ValueType = typeof(uint);
            bnSave.Enabled = false;
            dataGridView1.DataError += OnGridViewFormatError;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(Form1_Load));

            A.I.Init();
            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);
            this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();
            A.I.OnOpenDocumentPathChanged += OpenDocumentPathChanged;
            A.I.OnAuthDataLoaded += AuthDataLoaded;
            if (Directory.Exists(Path.GetDirectoryName(A.I.DefaultFilePath)))
            {
                openFileDialog1.InitialDirectory = Path.GetDirectoryName(A.I.DefaultFilePath);
                saveFileDialog1.InitialDirectory = Path.GetDirectoryName(A.I.DefaultFilePath);
                A.I.TryLoadAuthDataFromFile(A.I.DefaultFilePath);
            }
        }

        private void AuthDataLoaded()
        {
            Debug.WriteLine(nameof(AuthDataLoaded));

            FillTableGridFromData();
        }

        private void OpenDocumentPathChanged()
        {
            Debug.WriteLine(nameof(OpenDocumentPathChanged));

            //tbFilePath.Text = A.I.OpenedDocumentPath;
            bnSave.Enabled = !string.IsNullOrEmpty(A.I.OpenedDocumentPath);
        }

        private void bnOpen_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnOpen_Click));

            openFileDialog1.InitialDirectory = CodeHelpers.FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Current, openFileDialog1.InitialDirectory);
            var dialogResult = openFileDialog1.ShowDialog();
            if ((dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes) && !string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                if (!A.I.TryLoadAuthDataFromFile(openFileDialog1.FileName))
                    MessageBox.Show(Current, Resources.strNotOpened + "\n" + Resources.strSeeLogs);
                else
                {
                    saveFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                    openFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                }
            }
        }

        private void bnSaveAs_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnSaveAs_Click));

            FillDataFromTableGrid();
            saveFileDialog1.InitialDirectory = CodeHelpers.FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Current, saveFileDialog1.InitialDirectory);
            var dialogResult = saveFileDialog1.ShowDialog();
            if ((dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes) && !string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                if (A.I.TrySaveAuthDataToFile(saveFileDialog1.FileName))
                {
                    MessageBox.Show(Current, Resources.strSaved);
                    A.I.TryLoadAuthDataFromFile(saveFileDialog1.FileName);
                    saveFileDialog1.InitialDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                    openFileDialog1.InitialDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                }
                else
                {
                    MessageBox.Show(Current, Resources.strNotSaved + "\n" + Resources.strSeeLogs);
                }
            }
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnSave_Click));

            FillDataFromTableGrid();
            if (!string.IsNullOrEmpty(A.I.OpenedDocumentPath))
            {
                if (A.I.TrySaveAuthDataToFile(A.I.OpenedDocumentPath))
                {
                    MessageBox.Show(Current, Resources.strSaved);
                    A.I.TryLoadAuthDataFromFile(A.I.OpenedDocumentPath);
                }
                else
                    MessageBox.Show(Current, Resources.strNotSaved + "\n" + Resources.strSeeLogs);
            }
        }

        private void tbVersion_Validating(object sender, CancelEventArgs e)
        {
            tbVersion.Text = Regex.Replace(tbVersion.Text, "[^0-9]", "");
            uint temp;
            if (!UInt32.TryParse(tbVersion.Text, out temp))
                tbVersion.Text = A.I.CurrentLoadedAuthData.Version.ToString();
        }

        private void OnGridViewFormatError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Debug.WriteLine(nameof(OnGridViewFormatError));

            MessageBox.Show(Current, Resources.strBedInputFormat);
        }

        private void FillTableGridFromData()
        {
            Debug.WriteLine(nameof(FillTableGridFromData));

            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            foreach (var val in A.I.CurrentLoadedAuthData.NameNumbers.OrderByDescending(x => x.Role).ThenBy(x => x.Name))
            {
                var rowIndex = dataGridView1.Rows.Add(val.Name, val.Number, val.PasswordHash, val.Role == 0 ? false : true);
            }

            tbVersion.Text = A.I.CurrentLoadedAuthData.Version.ToString();
        }

        private void FillDataFromTableGrid()
        {
            Debug.WriteLine(nameof(FillDataFromTableGrid));

            A.I.CurrentLoadedAuthData.NameNumbers.Clear();
            foreach (DataGridViewRow val in dataGridView1.Rows)
                if (!string.IsNullOrWhiteSpace((string)val.Cells[0].Value) && val.Cells[1].Value != null)
                    A.I.CurrentLoadedAuthData.NameNumbers.Add(new NameNumber((string)(val.Cells[0].Value ?? "???"), (uint)(val.Cells[1].Value ?? 0), (string)(val.Cells[2].Value ?? ""), Convert.ToUInt16((bool)(val.Cells[3].Value ?? false))));

            A.I.CurrentLoadedAuthData.Version = UInt32.Parse(tbVersion.Text);
        }

        private void bnSetPass_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 0 ||
                dataGridView1.SelectedCells[0].OwningRow.IsNewRow)
            {
                MessageBox.Show(Current, "Выберите пользователя");
            }
            else
            {
                var setPassDiag = new SetPasswordForm();
                var result = setPassDiag.ShowDialog();
                if (result == DialogResult.OK)
                {
                    var md5 = setPassDiag.PasswordMD5;
                    dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[2].Value = md5;
                }
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //e.SuppressKeyPress = true;
            }
        }

        private void bnDeleteUser_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
            {
                if (oneCell.Selected)
                    dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex > -1)
            {
                var setPassDiag = new SetPasswordForm();
                var result = setPassDiag.ShowDialog();
                if (result == DialogResult.OK)
                {
                    var md5 = setPassDiag.PasswordMD5;
                    dataGridView1.Rows[e.RowIndex].Cells[2].Value = md5;
                }
            }
        }
    }
}
