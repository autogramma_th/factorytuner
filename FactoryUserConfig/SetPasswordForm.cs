﻿using FactoryDataConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace FactoryUserConfig
{
    public partial class SetPasswordForm : Form
    {
        public string PasswordMD5 { get; set; }

        public static SetPasswordForm Current;

        public SetPasswordForm()
        {
            Current = this;
            InitializeComponent();
            bnSave.Enabled = false;
            this.DialogResult = DialogResult.None;
            PasswordMD5 = string.Empty;
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            if (IsInputDone())
            {
                var md5 = CodeHelpers.GetMd5Hash(tbPass1.Text);
                PasswordMD5 = md5;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void tbPass1_TextChanged(object sender, EventArgs e)
        {
            bnSave.Enabled = IsInputDone();
        }

        private void tbPass2_TextChanged(object sender, EventArgs e)
        {
            bnSave.Enabled = IsInputDone();
        }

        bool IsInputDone()
        {
            if (tbPass1.Text.Length < 4)
                return false;
            if (tbPass1.Text != tbPass2.Text)
                return false;
            return true;
        }
    }
}
