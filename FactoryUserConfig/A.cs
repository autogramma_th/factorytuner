﻿using FactoryDataConfig;
using FactoryTuner.CommonEntities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FactoryUserConfig
{
    /// <summary>
    /// Main entry point
    /// </summary>
    public class A
    {
        #region I
        private static A instance;

        public static A I
        {
            get
            {
                if (instance == null)
                    instance = new A();
                return instance;
            }
        }

        internal void Init()
        {
            ;
        }
        #endregion

        public AuthData CurrentLoadedAuthData { get; private set; } = new AuthData();
        public event Action OnAuthDataLoaded = delegate { };

        public string OpenedDocumentPath { get; private set; }
        public event Action OnOpenDocumentPathChanged = delegate { };

        public string DefaultFilePath { get; private set; }

        private A()
        {
            Debug.WriteLine(nameof(A));

            InitTrace();
            Trace.TraceInformation(CodeHelpers.GetAppVersionString());
            CodeHelpers.SubscribeToUndandledExceptions();
            DefaultFilePath = Path.Combine(Environment.CurrentDirectory, SharedConstants.DefaultDataDirName, SharedConstants.DefaultUsersConfigFileName);
        }

        public bool TrySaveAuthDataToFile(string filePath)
        {
            Debug.WriteLine(nameof(TrySaveAuthDataToFile));
            CurrentLoadedAuthData.Version++;
            return CodeHelpers.TrySaveDataToXmlFile(CurrentLoadedAuthData, filePath, true);
        }

        public bool TryLoadAuthDataFromFile(string filePath)
        {
            Debug.WriteLine(nameof(TryLoadAuthDataFromFile));

            var result = CodeHelpers.TryLoadDataFromXmlFile<AuthData>(filePath, true);

            if (result != null)
            {
                CurrentLoadedAuthData = result;
                OnAuthDataLoaded.Invoke();
                OpenedDocumentPath = filePath;
                OnOpenDocumentPathChanged();
                return true;
            }
            return false;
        }

        private void InitTrace()
        {
            Debug.WriteLine(nameof(InitTrace));

            Trace.AutoFlush = true;
            var logFileName = "Log" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt";
            var logFilePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), logFileName);
            var _logFileStream = new FileStream(logFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, 4096, FileOptions.Asynchronous);
            var _streamWriter = new StreamWriter(_logFileStream);
            Trace.Listeners.Add(new TextWriterTraceListener(_streamWriter));
        }
    }
}
