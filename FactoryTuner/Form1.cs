﻿using FactoryDataConfig;
using FactoryTuner.Can.Internal;
using FactoryTuner.CommonEntities;
using FactoryTuner.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FactoryTuner
{
    public partial class Form1 : Form
    {
        private float _startFormWidth;
        private float _startFormHeight;
        private float _startFontSize;

        private bool? _isAutosFileExist = false;
        private bool? _isUsersFileExist = false;
        private bool? _isJournalFileExist = false;
        private bool _isCfgsSyncDone = false;
        private FileSystemWatcher watcher = new FileSystemWatcher();

        public static Form1 Current;

        public Form1()
        {
            Current = this;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            A.I.Init(); //Init main entry
            Debug.WriteLine(nameof(Form1_Load));

            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);
            this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();
            _startFormWidth = this.Width;
            _startFormHeight = this.Height;
            _startFontSize = this.Font.Size;

            //Save font size
            //if (Settings.Default.WinFontSizeProp != 0)
            //    this.Font = new Font(this.Font.FontFamily, Settings.Default.WinFontSizeProp);

            lbFontSize.Text = "x" + this.Font.Size;

            if (Settings.Default.WinSizeProp != null && Settings.Default.WinSizeProp != Size.Empty)
            {
                this.Size = Settings.Default.WinSizeProp;
                this.CenterToScreen();
            }

            Init();
        }

        private void Init()
        {
            if (A.I.TryLoadAllDataFromFiles())
            {
                Trace.TraceInformation("Data loaded from configs");
                ResetForm();
                FillForm();
            }
            else
            {
                MessageBox.Show(Form1.Current, Resources.strCantLoadData);
            }
            InitCheckerDataFolder();
            UpdatePathLablesState();
        }

        private void InitCheckerDataFolder()
        {
            watcher = new FileSystemWatcher
            {
                Path = SharedConstants.DefaultDataDirName,
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                   | NotifyFilters.FileName | NotifyFilters.DirectoryName,
                Filter = "*." + SharedConstants.DataFileFormat
            };
            watcher.Changed += new FileSystemEventHandler((sender, e) =>
            {
                watcher.EnableRaisingEvents = false;
                BeginInvoke(new Action(() =>
                {
                    MessageBox.Show(Form1.Current, "Наборы настроек авто и пользователей обновлены.", "Обновление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Init();
                }));
            });
            watcher.EnableRaisingEvents = true;
        }

        private void FillForm()
        {
            Debug.WriteLine(nameof(FillForm));

            cbMechanic.Items.Clear();
            cbMechanic.DisplayMember = nameof(NameNumber.ToString);
            cbMechanic.Items.Add(new NameNumber("Не указан", 0));
            foreach (var user in A.I.AuthData.NameNumbers.OrderBy(x => x.Name))
                cbMechanic.Items.Add(user);

            cbTireMarking.Items.Clear();
            var tireSizesLoaded = new List<string>();
            cbTireMarking.DisplayMember = nameof(TireModel.Size);
            foreach (var tireModel in A.I.MainData.TireModels)
                if (!tireSizesLoaded.Contains(tireModel.Size))
                {
                    cbTireMarking.Items.Add(tireModel);
                    tireSizesLoaded.Add(tireModel.Size);
                }

            cbManuals.Items.Clear();
            cbManuals.DisplayMember = nameof(Transmission.Name);
            foreach (var manualItem in A.I.MainData.Transmissions)
                if (manualItem.Type == 1)
                    cbManuals.Items.Add(manualItem);

            cbAutos.Items.Clear();
            cbAutos.DisplayMember = nameof(Transmission.Name);
            foreach (var manualItem in A.I.MainData.Transmissions)
                if (manualItem.Type == 2)
                    cbAutos.Items.Add(manualItem);

            cbTKs.Items.Clear();
            cbTKs.DisplayMember = nameof(Transmission.Name);
            foreach (var manualItem in A.I.MainData.Transmissions)
                if (manualItem.Type == 3)
                    cbTKs.Items.Add(manualItem);

            cbMTB.Items.Clear();
            foreach (var gpm in A.I.MainData.GPM)
                cbMTB.Items.Add(gpm);
        }

        private void ResetForm()
        {
            Debug.WriteLine(nameof(ResetForm));

            tbDynRadius.Text = string.Empty;
            tbPulses.Text = string.Empty;
            tbK.Text = string.Empty;
            cbManualInput.Checked = false;
            tbDynRadius.ReadOnly = true;
            tbPulses.ReadOnly = true;
            tbK.ReadOnly = true;
            //lbStatus.Text = string.Empty;
            //lbSerial.Text = "ˍ ˍ ˍ ˍ ˍ ˍ ˍ ˍ ˍ";
            //lbCRC.Text = string.Empty;
            //lbKValueInDevice.Text = string.Empty;
            tbChassis.Text = string.Empty;
            cbMechanic.SelectedIndex = -1;
            cbMTB.SelectedIndex = -1;
            cbTireModel.SelectedIndex = -1;
            cbTireModel.Items.Clear();
            cbTireMarking.SelectedIndex = -1;
            ResetTransmissionCheckBoxes();
            cbManuals.Enabled = true;
            bnWrite.Enabled = false;
            rbManual.Checked = true;
            bnReadParamsFromTacho.Enabled = cbDemo.Checked;
            lbJournalCaption.Text = GetJournalCaption();
            FillDeviceStatusGrid();
            DoCalculation();
        }

        private void FillDeviceStatusGrid()
        {
            dgDeviceStatus.DataSource = null;
            dgDeviceStatus.Rows.Clear();
            var index = dgDeviceStatus.Rows.Add();
            dgDeviceStatus.Rows[index].Cells[0].Value = "Статус";
            dgDeviceStatus.Rows[index].Cells[1].Value = "Отключен";
            dgDeviceStatus.Rows[index].Cells[2].Value = "Отключен";

            index = dgDeviceStatus.Rows.Add();
            dgDeviceStatus.Rows[index].Cells[0].Value = "Серийный номер";
            dgDeviceStatus.Rows[index].Cells[1].Value = "N/A";
            dgDeviceStatus.Rows[index].Cells[2].Value = "N/A";

            index = dgDeviceStatus.Rows.Add();
            dgDeviceStatus.Rows[index].Cells[0].Value = "Контр. сумма ПО";
            dgDeviceStatus.Rows[index].Cells[1].Value = "N/A";
            dgDeviceStatus.Rows[index].Cells[2].Value = "N/A";

            index = dgDeviceStatus.Rows.Add();
            dgDeviceStatus.Rows[index].Cells[0].Value = "Модель";
            dgDeviceStatus.Rows[index].Cells[1].Value = "N/A";
            dgDeviceStatus.Rows[index].Cells[2].Value = "N/A";

            index = dgDeviceStatus.Rows.Add();
            dgDeviceStatus.Rows[index].Cells[0].Value = "Производитель";
            dgDeviceStatus.Rows[index].Cells[1].Value = "N/A";
            dgDeviceStatus.Rows[index].Cells[2].Value = "N/A";

            foreach (DataGridViewRow row in dgDeviceStatus.Rows)
            {
                row.Height = (dgDeviceStatus.ClientRectangle.Height - dgDeviceStatus.ColumnHeadersHeight) / dgDeviceStatus.Rows.Count;
            }
            dgDeviceStatus.ClearSelection();
        }

        private void bnClear_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnClear_Click));

            ResetForm();
        }

        private void bnWrite_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnWrite_Click));

            if (string.IsNullOrEmpty(tbChassis.Text) || cbMechanic.SelectedIndex == -1)
            {
                var dialogResult = MessageBox.Show(Form1.Current, Resources.strPlzInputName, Resources.strAttension, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var selectedUser = cbMechanic.SelectedItem == null ? null : (NameNumber)cbMechanic.SelectedItem;

            var checkPassDialog = new CheckPasswordForm
            {
                SelectedMechanic = selectedUser
            };
            if (checkPassDialog.ShowDialog() == DialogResult.OK)
            {
                var isUSDDeviceWritten = false;
                var isDeviceWritten = false;

                if (!cbDemo.Checked) {
                    isUSDDeviceWritten = A.I.TryWriteCalculationToUSDDevice();
                    isDeviceWritten = A.I.TryWriteCalculationToDevice();
                }

                //запись на устройство
                if (cbDemo.Checked || isDeviceWritten || isUSDDeviceWritten)
                {
                    var selectedTransmission = cbAutos.SelectedItem;
                    if (rbManual.Checked)
                        selectedTransmission = cbManuals.SelectedItem;
                    else if (rbTC.Checked)
                        selectedTransmission = cbTKs.SelectedItem;


                    var selectedTrans = selectedTransmission == null ? null : (Transmission)selectedTransmission;
                    var selectedTire = cbTireModel.SelectedItem == null ? null : (TireModel)cbTireModel.SelectedItem;
                    if (String.IsNullOrEmpty(A.I.CalculatedData.СhassisNuber))
                        A.I.CalculatedData.СhassisNuber = "Не указан";
                    if (selectedUser == null || string.IsNullOrEmpty(selectedUser.Name))
                    {
                        uint num = 0;
                        if (selectedUser != null)
                            num = selectedUser.Number;
                        selectedUser = new NameNumber
                        {
                            Name = "Не указан",
                            Number = num
                        };
                    }
                    if (!A.I.TryWriteJournalEntry(selectedUser, selectedTrans, selectedTire, A.I.LastReadedSerial, A.I.LastReadedManufacturer, A.I.LastReadedModel, A.I.LastReadedCrc, isUSDDeviceWritten, A.I.LastReadedUSDSerial))
                    {
                        MessageBox.Show(Form1.Current, "Ошибка записи в журнал");
                        Trace.TraceWarning("Save to journal error");
                    }
                    else
                    {
                        Trace.TraceInformation(Resources.strWriteDone);
                        MessageBox.Show(Form1.Current, Resources.strWriteDone);
                    }
                }
                else
                {
                    Trace.TraceInformation(Resources.strCantWriteData);
                    MessageBox.Show(Form1.Current, Resources.strCantWriteData);
                }
            }
        }

        private void bnExit_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnExit_Click));
            Application.Exit();
        }

        private void bnJournal_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnJournal_Click));

            new JournalForm(this).ShowDialog();
        }

        private void bnOpenJournalBySN_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnOpenJournalBySN_Click));

            new JournalForm(this).ShowDialog(true);
        }

        private void checkTimer_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(checkTimer_Tick));
            UpdateDeviceStatusLables();
            CheckRemoteDataFileExists();
            UpdateStateWriteButton();
            UpdatePathLablesState();
            A.I.CopyDataFilesFromRemoteToLocalStoreAsync();
        }

        private bool _isCheckRemoteDataFileExistsNow = false;
        public void CheckRemoteDataFileExists()
        {
            if (!_isCheckRemoteDataFileExistsNow)
            {
                _isCheckRemoteDataFileExistsNow = true;
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        _isAutosFileExist = File.Exists(A.I.Configuration.MainDataLocation.Path);
                    }
                    catch
                    {
                        _isAutosFileExist = null;
                    }

                    try
                    {
                        _isUsersFileExist = File.Exists(A.I.Configuration.AuthDataLocation.Path);
                    }
                    catch
                    {
                        _isUsersFileExist = null;
                    }

                    try
                    {
                        _isJournalFileExist = File.Exists(A.I.Configuration.JournalLocation.Path);
                    }
                    catch
                    {
                        _isJournalFileExist = null;
                    }


                }).ContinueWith((task) =>
                {
                    _isCheckRemoteDataFileExistsNow = false;
                });
            }
        }

        public void UpdateDeviceStatusLables()
        {
            if (dgDeviceStatus.Rows.Count > 0)
            {
                //GET STATUS
                var devStatus = A.I.GetDeviceStatus();

                if (!A.I.IsDeviceConnectedAndValid)
                {
                    dgDeviceStatus.Rows[1].Cells[1].Value = "N/A";
                    dgDeviceStatus.Rows[2].Cells[1].Value = "N/A";
                    dgDeviceStatus.Rows[3].Cells[1].Value = "N/A";
                    dgDeviceStatus.Rows[4].Cells[1].Value = "N/A";
                }

                if (!A.I.IsUSDDeviceConnectedAndValid) {
                    dgDeviceStatus.Rows[1].Cells[2].Value = "N/A";
                    dgDeviceStatus.Rows[2].Cells[2].Value = "N/A";
                    dgDeviceStatus.Rows[3].Cells[2].Value = "N/A";
                    dgDeviceStatus.Rows[4].Cells[2].Value = "N/A";
                }

                bnReadParamsFromTacho.Enabled = A.I.IsDeviceConnectedAndValid;

                var usdDeviceConnected = A.I.IsUSDDeviceConnectedAndValid;

                //Tacho
                //GET SERIAL
                A.I.GetDeviceParameter<string>(TachoParameters.SerialNumberStr, (serial) =>
                {
                    A.I.LastReadedSerial = serial;
                    BeginInvoke(new Action(() =>
                        {
                            dgDeviceStatus.Rows[1].Cells[1].Value = A.I.LastReadedSerial;
                            //GET CRC
                            A.I.GetDeviceParameter<string>(TachoParameters.SoftwareChecksumStr, (crc) =>
                            {
                                A.I.LastReadedCrc = crc;
                                BeginInvoke(new Action(() =>
                                {
                                    dgDeviceStatus.Rows[2].Cells[1].Value = A.I.LastReadedCrc;
                                    //GET modelname
                                    A.I.GetDeviceParameter<string>(TachoParameters.ModelName, (model) =>
                                    {
                                        A.I.LastReadedModel = model;
                                        BeginInvoke(new Action(() =>
                                        {
                                            dgDeviceStatus.Rows[3].Cells[1].Value = A.I.LastReadedModel;
                                            //Get производитель
                                            A.I.GetDeviceParameter<string>(TachoParameters.ManufacturerStr, (dev) =>
                                            {
                                                A.I.LastReadedManufacturer = dev;
                                                BeginInvoke(new Action(() =>
                                                {
                                                    dgDeviceStatus.Rows[4].Cells[1].Value = A.I.LastReadedManufacturer;
                                                }));
                                            });
                                        }));
                                    });
                                }));
                            });
                        }));
                });

                //USD device
                A.I.GetUSDDeviceParameter<byte[]>(USDParameters.SerialNumber, (sn) => {
                    A.I.LastReadedUSDSerial = CodeHelpers.ByteArrayToString(sn);
                    BeginInvoke(new Action(() => {
                        dgDeviceStatus.Rows[1].Cells[2].Value = A.I.LastReadedUSDSerial;
                        A.I.GetUSDDeviceParameter<byte[]>(USDParameters.ModelType, (usdModel) => {
                            A.I.LastReadedUSDModel = CodeHelpers.ByteArrayToString(usdModel);
                            BeginInvoke(new Action(() => {
                                dgDeviceStatus.Rows[3].Cells[2].Value = A.I.LastReadedUSDModel;
                            }));
                        });
                    }));
                });

                dgDeviceStatus.Rows[0].Cells[1].Value = devStatus;

                dgDeviceStatus.Rows[0].Cells[2].Value = usdDeviceConnected ? "Подключен" : "Отключен";
                
                dgDeviceStatus.Rows[2].Cells[2].Value = "N/A";
                dgDeviceStatus.Rows[4].Cells[2].Value = "N/A";
            }
        }

        public void UpdatePathLablesState()
        {
            var bedColor = Color.Red;
            var goodColor = Color.Green;
            bool isCfgsSyncDone = true;

            if (A.I.Configuration == null)
            {
                lbPathAuth.Text = "No config!";
                lbPathData.Text = "No config!";
                lbPathJournal.Text = "No config!";
                lbPathAuth.ForeColor = bedColor;
                lbPathData.ForeColor = bedColor;
                lbPathJournal.ForeColor = bedColor;
                isCfgsSyncDone = false;
            }
            else
            {
                bool isSyncNow = A.I.IsCopyDataFilesFromRemoteToLocalStoreNow;
                if (isSyncNow)
                    isCfgsSyncDone = this._isCfgsSyncDone;

                //base.xml
                if (isSyncNow)
                {
                    lbPathData.ForeColor = Color.Gray;
                    lbPathData.Text = "Синхронизация...";
                }
                else if (!_isAutosFileExist.HasValue || _isAutosFileExist.Value == false)
                {
                    lbPathData.ForeColor = bedColor;
                    lbPathData.Text = "Недоступен";
                    isCfgsSyncDone = false;
                }
                else
                {
                    lbPathData.ForeColor = goodColor;
                    lbPathData.Text = "Доступен";
                }
                lbPathData.Text += $" ({Settings.Default.AutoSyncLastDate.ToString("dd.MM.yy HH:mm")} v{A.I.MainData.Version})";

                //users.xml
                if (isSyncNow)
                {
                    lbPathAuth.ForeColor = Color.Gray;
                    lbPathAuth.Text = "Синхронизация...";
                }
                else if (!_isUsersFileExist.HasValue || _isUsersFileExist.Value == false)
                {
                    lbPathAuth.ForeColor = bedColor;
                    lbPathAuth.Text = "Недоступен";
                    isCfgsSyncDone = false;
                }
                else
                {
                    lbPathAuth.ForeColor = goodColor;
                    lbPathAuth.Text = "Доступен";
                }
                lbPathAuth.Text += $" ({Settings.Default.AutoSyncLastDate.ToString("dd.MM.yy HH:mm")} v{A.I.AuthData.Version})";

                //jounal
                if (!_isJournalFileExist.HasValue)
                {
                    lbPathJournal.ForeColor = bedColor;
                    lbPathJournal.Text = "Недоступен";
                    isCfgsSyncDone = false;
                }
                else if (_isJournalFileExist.Value == false)
                {
                    lbPathJournal.ForeColor = Color.Gray;
                    lbPathJournal.Text = "Пуст";
                }
                else
                {
                    lbPathJournal.ForeColor = goodColor;
                    lbPathJournal.Text = "Доступен";
                }

                lbPathJournal.Text += $" ({Settings.Default.JournalSyncLastDate.ToString("dd.MM.yy HH:mm")})";
            }
            this._isCfgsSyncDone = isCfgsSyncDone;
        }

        private void UpdateStateWriteButton()
        {
            bool isDone = true;
            if (!cbDemo.Checked && !A.I.IsDeviceConnectedAndValid && !A.I.IsUSDDeviceConnectedAndValid)
                isDone = false;
            else if (A.I.CalculatedData.K == 0 || A.I.CalculatedData.K == 65535)
                isDone = false;
            else if (!_isCfgsSyncDone)
                isDone = false;
            bnWrite.Enabled = isDone;
        }

        private void cbTireMarking_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbTireModel.Items.Clear();
            cbTireModel.DisplayMember = nameof(TireModel.Name);
            cbTireModel.SelectedIndex = -1;
            var selectedTireSize = cbTireMarking.Text;
            foreach (var tireModel in A.I.MainData.TireModels)
            {
                if (tireModel.Size == selectedTireSize)
                    cbTireModel.Items.Add(tireModel);
            }
        }

        private void rbManual_CheckedChanged(object sender, EventArgs e)
        {
            ResetTransmissionCheckBoxes();
            cbManuals.Enabled = true;
        }

        private void rbAuto_CheckedChanged(object sender, EventArgs e)
        {
            ResetTransmissionCheckBoxes();
            cbAutos.Enabled = true;
        }

        private void rbTC_CheckedChanged(object sender, EventArgs e)
        {
            ResetTransmissionCheckBoxes();
            cbTKs.Enabled = true;
        }
        public void ResetTransmissionCheckBoxes()
        {
            cbManuals.Enabled = false;
            cbAutos.Enabled = false;
            cbTKs.Enabled = false;
            cbTKs.SelectedIndex = -1;
            cbManuals.SelectedIndex = -1;
            cbAutos.SelectedIndex = -1;
        }

        private void cbManualInput_CheckedChanged(object sender, EventArgs e)
        {
            tbDynRadius.ReadOnly = !cbManualInput.Checked;
            tbPulses.ReadOnly = !cbManualInput.Checked;
            tbK.ReadOnly = !cbManualInput.Checked;

            rbManual.Enabled = !cbManualInput.Checked;
            rbAuto.Enabled = !cbManualInput.Checked;
            rbTC.Enabled = !cbManualInput.Checked;
            cbMTB.Enabled = !cbManualInput.Checked;
            cbTireMarking.Enabled = !cbManualInput.Checked;
            cbTireModel.Enabled = !cbManualInput.Checked;

            cbManuals.Enabled = (!cbManualInput.Checked && rbManual.Checked);
            cbAutos.Enabled = (!cbManualInput.Checked && rbAuto.Checked);
            cbTKs.Enabled = (!cbManualInput.Checked && rbTC.Checked);
        }

        private void cbTireModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbDynRadius.Text = string.Empty;
            foreach (var tireModel in A.I.MainData.TireModels)
                if (tireModel.Name == cbTireModel.Text)
                {
                    tbDynRadius.Text = (tireModel.DR * 1000f).ToString();
                    break;
                }
        }

        private void Transmission_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbPulses.Text = string.Empty;
            var transCombo = (ComboBox)sender;
            foreach (var trans in A.I.MainData.Transmissions)
                if (trans.Name == transCombo.Text)
                {
                    tbPulses.Text = trans.N.ToString();
                    break;
                }
        }

        private void DoCalculation()
        {
            float.TryParse(tbPulses.Text, out A.I.CalculatedData.N);
            float.TryParse(tbDynRadius.Text, out A.I.CalculatedData.DR);
            A.I.CalculatedData.СhassisNuber = tbChassis.Text;
            float.TryParse(cbMTB.Text, out float GPN);
            A.I.CalculatedData.GPM = GPN;
            float kFloat = 0f;
            if (A.I.CalculatedData.DR == 0 || GPN == 0 || A.I.CalculatedData.N == 0)
                kFloat = 0;
            else
                kFloat = 1000f / ((A.I.CalculatedData.DR / 1000f * 2f * (float)Math.PI) / GPN / A.I.CalculatedData.N);
            kFloat = (float)Math.Round(kFloat);

            if (float.IsNaN(kFloat))
                kFloat = UInt16.MaxValue;

            if (kFloat < 0f)
                kFloat = 0f;
            if (kFloat > 65535)
                kFloat = 65535;

            A.I.CalculatedData.K = (UInt16)kFloat;

            A.I.CalculatedData.IsOverride = cbManualInput.Checked;
            tbK.Text = A.I.CalculatedData.K.ToString();
            UpdateStateWriteButton();
        }

        private void tbDynRadius_TextChanged(object sender, EventArgs e)
        {
            DoCalculation();
        }

        private void tbPulses_TextChanged(object sender, EventArgs e)
        {
            DoCalculation();
        }

        private void cbMTB_SelectedIndexChanged(object sender, EventArgs e)
        {
            DoCalculation();
        }

        private void tbChassis_TextChanged(object sender, EventArgs e)
        {
            DoCalculation();
        }

        private void timerSaveToRemoteJournal_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(timerSaveToRemoteJournal_Tick));

            if (A.I.RemoteJournalQueue != null)
                A.I.RemoteJournalQueue.TrySendAllToRemoteAsync();
        }

        private void bnSettings_Click(object sender, EventArgs e)
        {
            var selectedUser = cbMechanic.SelectedItem == null ? null : (NameNumber)cbMechanic.SelectedItem;

            if (selectedUser != null && selectedUser.Role != 1)
            {
                MessageBox.Show(Current, "Выбран НЕ администратор.");
            }
            else
            {
                var checkPassDialog = new CheckPasswordForm
                {
                    SelectedMechanic = selectedUser
                };
                if (checkPassDialog.ShowDialog() == DialogResult.OK)
                {
                    new Config().ShowDialog();
                    Init();
                    A.I.UpdateJournalPath();
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            A.I.Dispose();

            Settings.Default.WinFontSizeProp = this.Font.Size;
            Settings.Default.WinSizeProp = this.Size;
            Settings.Default.Save();
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
        }

        private void bnFontMinus_Click(object sender, EventArgs e)
        {
            if (this.Font.Size > 9)
            {
                this.Font = new Font(this.Font.FontFamily, this.Font.Size - 1);
                lbFontSize.Text = "x" + this.Font.Size;
            }
        }

        private void bnFontPlus_Click(object sender, EventArgs e)
        {
            if (this.Font.Size < 30)
            {
                this.Font = new Font(this.Font.FontFamily, this.Font.Size + 1);
                lbFontSize.Text = "x" + this.Font.Size;
            }
        }

        private void bnReadParamsFromTacho_Click(object sender, EventArgs e)
        {
            new Admin(tbChassis).ShowDialog(Form1.Current);
        }

        private void cbDemo_CheckedChanged(object sender, EventArgs e)
        {
            bnReadParamsFromTacho.Enabled = cbDemo.Checked;
        }

        private string GetJournalCaption()
        {
            if (A.I.GetRemoteJournalPathOrNull() == null)
                return "Общий журнал (локальный):";
            else
                return "Общий журнал (серверный):";
        }
    }
}
