﻿using FactoryDataConfig;
using FactoryTuner.Can.Internal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UsbCan_Interface;

namespace FactoryTuner.Can
{

    class CanWorker : IDisposable
    {
        private struct CanMessageData
        {
            public byte[] LastCanResponceData;
            public uint LastCanResponceID;

            public override string ToString()
            {
                return $"ID:{LastCanResponceID.ToString("X8")} = DATA:{CodeHelpers.ByteArrayToString(LastCanResponceData)}";
            }
        }

        private AbstractUsbInterface _canInterface;
        private CanMessageFabrik _canMessageFabrik;
        private CanMessageData _lastCanMessage;

        //BAM
        private UInt16 _waitForNBAMPacketsBytes;
        private Action<TachoParameters, byte[]> _onBAMPacketsDone;
        private List<byte> _receivedBAMData = new List<byte>();
        private TachoParameters _waitBAMparam;

        public CanWorker()
        {
            ConnectToCanInterface();
        }

        public void ConnectToCanInterface()
        {
            ConnectToCanInterface(CanUsbInterfaceType.Can);
        }

        void ConnectToCanInterface(CanUsbInterfaceType canInterfaceType)
        {
            if (_canInterface != null)
            { //если usb интерфейс уже проинициализирован
                //то деинициализируем интерфейс
                Debug.WriteLine("Деинициализация Can интерфейса.");

                _canInterface.DeInit();
                _canInterface = null;
                _canMessageFabrik = null;
            }

            Debug.WriteLine("Создаётся объект для Can интерфейса. Тип: " + canInterfaceType);

            //здесь инициализируем выбранный тип интерфейса
            switch (canInterfaceType)
            {
                case CanUsbInterfaceType.Can:
                    _canInterface = new UsbCanInterface(BitRate.b_250);
                    _canInterface.OnReceiveCanMessage = OnReceiveCanMessage;
                    _canMessageFabrik = new CanMessageFabrik();
                    break;
                default:
                    _canInterface = null;
                    return;
            }

            Debug.WriteLine("Инициализация Can интерфейса.");

            _canInterface.Init();
            _canInterface.SetFilterConfig(FilterConfigType.All);
        }

        public bool IsReady()
        {
            return _canInterface != null && _canInterface.IsReady();
        }

        public string GetErrorStatus()
        {
            if (_canInterface == null)
                return "CanInf = null";
            else if (!_canInterface.IsReady())
                return "Нет CAN адаптера";
            return "Ок";
        }

        /// <summary>
        /// Установить фильтрацию сообщений с заданными id
        /// </summary>
        /// <param name="ext"></param>
        /// <param name="idFrom"></param>
        /// <param name="idTo"></param>
        /// <returns></returns>
        public bool SetFilterMessages(bool ext, uint idFrom, uint idTo)
        {
            return _canInterface.SetFilterMessages(ext, idFrom, idTo);
        }

        public TachoStatusResult GetTachoStatus()
        {
            Debug.WriteLine(nameof(GetTachoStatus));

            var result = ReadParameterUInt16(TachoParameters.KSpeedU16, null);
            if (!result.HasValue)
                return TachoStatusResult.NoCan;
            else
            {
                if (result.Value == false)
                    return TachoStatusResult.NoTacho;
                else
                    return TachoStatusResult.Tacho;
            }
        }

        public bool IsUSDConnected() {
            var result = PingUSDSession(null);
            return (result.HasValue && result.Value == true);
        }


        public bool? ReadParameterUInt32(TachoParameters param, Action<TachoParameters, UInt32> resultCallback)
        {
            return ReadParameter(param, (parameter, data) =>
            {
                resultCallback?.Invoke(parameter, BitConverter.ToUInt32(data, 0));
            });
        }

        public bool? ReadParameterUInt16(TachoParameters param, Action<TachoParameters, UInt16> resultCallback)
        {
            return ReadParameter(param, (parameter, data) =>
            {
                resultCallback?.Invoke(parameter, BitConverter.ToUInt16(data, 0));
            });
        }

        public bool? ReadParameterString(TachoParameters param, Action<TachoParameters, string> resultCallback)
        {
            return ReadParameter(param, (parameter, data) =>
            {
                Encoding iso = UsbCanConstants.ReadStringDefaultEncoding;
                resultCallback?.Invoke(parameter, Encoding.UTF8.GetString(Encoding.Convert(iso, Encoding.UTF8, data)));
            });
        }

        public bool? ReadParameter(TachoParameters param, Action<TachoParameters, byte[]> resultCallback)
        {
            if (resultCallback == null)
                resultCallback = new Action<TachoParameters, byte[]>((parameter, data) => { });

            object receiveCode;

            var isDone = _canInterface.SendMessage(_canMessageFabrik.ReadParameter(param), true, new uint[] { UsbCanConstants.TachoReadParamBAMId, UsbCanConstants.TachoReadParamResultId }, out receiveCode, 100);

            if (!isDone)
                return null;

            var lastCanMessage = _lastCanMessage;

            if (lastCanMessage.LastCanResponceID == UsbCanConstants.TachoReadParamResultId)
            {
                resultCallback(param, lastCanMessage.LastCanResponceData.Skip(2).ToArray());
                _canInterface.SendMessage(_canMessageFabrik.ReadParameterAck(param), false, 0, out receiveCode);
                return true;
            }
            else if (lastCanMessage.LastCanResponceID == UsbCanConstants.TachoReadParamBAMId)
            {
                _waitForNBAMPacketsBytes = BitConverter.ToUInt16(new byte[] { lastCanMessage.LastCanResponceData[1], lastCanMessage.LastCanResponceData[2] }, 0);
                _waitForNBAMPacketsBytes -= 2; //пропускаем заголовок
                _waitBAMparam = param;
                _receivedBAMData.Clear();
                _onBAMPacketsDone = resultCallback;
                return true;
            }
            else
                return false;
        }

        public bool? SetUSDSessionType(USDSessionType type, Action<USDSessionType, byte[]> resultCallback) {
            if (resultCallback == null)
                resultCallback = new Action<USDSessionType, byte[]>((sessionType, data) => { });

            var result = _canInterface.SendMessage(_canMessageFabrik.USDSetSessionTypeRequest(type), true, UsbCanConstants.USDResponseId, out object receiveCode, 500);
            if (!result)
                return null;

            var lastCanMessage = _lastCanMessage;

            if (lastCanMessage.LastCanResponceID == UsbCanConstants.USDResponseId) {
                resultCallback(type, lastCanMessage.LastCanResponceData.Skip(1).ToArray());
                return true;
            } else
                return false;
        }

        public bool? PingUSDSession(Action<byte[]> resultCallback) {
            if (resultCallback == null)
                resultCallback = new Action<byte[]>((data) => { });

            var result = _canInterface.SendMessage(_canMessageFabrik.USDPing(), true, UsbCanConstants.USDResponseId, out object receiveCode, 500);
            if (!result)
                return null;

            var lastCanMessage = _lastCanMessage;

            if (lastCanMessage.LastCanResponceID == UsbCanConstants.USDResponseId) {
                resultCallback(lastCanMessage.LastCanResponceData.Skip(1).ToArray());
                return true;
            } else
                return false;
        }

        public bool? ReadUSDParameter(USDParameters param, Action<USDParameters, byte[]> resultCallback) {
            if (resultCallback == null)
                resultCallback = new Action<USDParameters, byte[]>((parameter, data) => { });

            var result = _canInterface.SendMessage(_canMessageFabrik.USDReadRequest(param), true, UsbCanConstants.USDResponseId, out object receiveCode, 500);
            if (!result)
                return null;

            var lastCanMessage = _lastCanMessage;

            if (lastCanMessage.LastCanResponceID == UsbCanConstants.USDResponseId) {
                resultCallback(param, lastCanMessage.LastCanResponceData.Skip(1).ToArray()); 
                return true;
            } else
                return false;
        }

        public bool? ReadUSDParameterUInt32(USDParameters param, Action<USDParameters, UInt32> resultCallback) {
            return ReadUSDParameter(param, (parameter, data) => {
                resultCallback?.Invoke(parameter, BitConverter.ToUInt32(data, 0));
            });
        }

        public bool? ReadUSDParameterUInt16(USDParameters param, Action<USDParameters, UInt16> resultCallback) {
            return ReadUSDParameter(param, (parameter, data) => {
                resultCallback?.Invoke(parameter, BitConverter.ToUInt16(data, 0));
            });
        }

        public bool? ReadUSDParameterString(USDParameters param, Action<USDParameters, string> resultCallback) {
            return ReadUSDParameter(param, (parameter, data) => {
                Encoding iso = UsbCanConstants.USDDeviceStringEncoding;
                resultCallback?.Invoke(parameter, Encoding.UTF8.GetString(Encoding.Convert(iso, Encoding.UTF8, data)));
            });
        }

        public bool? WriteUSDParameter(USDParameters param, byte[] value) {
            var result = _canInterface.SendMessage(_canMessageFabrik.USDWriteRequest(param, value), true, UsbCanConstants.USDResponseId, out object receiveCode, 500);
            if (!result)
                return null;
            else {
                if (result)
                    return (_lastCanMessage.LastCanResponceData[1] != 0x7F);
                else
                    return result;
            }
        }

        public bool? WriteUSDParameter16(USDParameters param, UInt16 value) {
            var result = _canInterface.SendMessage(_canMessageFabrik.USDWriteRequest16(param, value), true, UsbCanConstants.USDResponseId, out object receiveCode, 500);
            if (!result)
                return null;
            else {
                if (result)
                    return (_lastCanMessage.LastCanResponceData[1] != 0x7F);
                else
                    return result;
            }
        }

        public bool? WriteParemeter16(TachoParameters param, UInt16 value)
        {
            var waitData = new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xEF, 0x00 };
            var result = _canInterface.SendMessage(_canMessageFabrik.SetParameter16(param, value), true, UsbCanConstants.TachoWriteParamAckId, out object receiveCode, 500);
            if (!result)
                return null;
            else
            {
                return CodeHelpers.ByteArrayCompare(waitData, _lastCanMessage.LastCanResponceData);
            }
        }

        public bool? WriteParemeter32(TachoParameters param, UInt32 value)
        {
            var waitData = new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xEF, 0x00 };
            var result = _canInterface.SendMessage(_canMessageFabrik.SetParameter32(param, value), true, UsbCanConstants.TachoWriteParamAckId, out object receiveCode, 500);
            if (!result)
                return null;
            else
            {
                return CodeHelpers.ByteArrayCompare(waitData, _lastCanMessage.LastCanResponceData);
            }
        }

        void OnReceiveCanMessage(uint canID, byte[] msg)
        {
            _lastCanMessage = new CanMessageData()
            {
                LastCanResponceID = canID,
                LastCanResponceData = msg
            };
            //TODO [Debug] input bytes
#if DEBUG
            if (canID != 0x18FEC1EE &&
                canID != 0x0CFE6CEE)
                Debug.WriteLine("<<<<<< " + _lastCanMessage.ToString());
#endif


            if (canID == UsbCanConstants.TachoReadParamBAMResultId && _waitForNBAMPacketsBytes > 0)
            {
                var fromIndex = 1;
                if (msg[0] == 1)
                    fromIndex = 3;

                for (int i = fromIndex; (i < msg.Length && _waitForNBAMPacketsBytes > 0); i++)
                {
                    _waitForNBAMPacketsBytes--;
                    _receivedBAMData.Add(msg[i]);
                }

                if (_waitForNBAMPacketsBytes == 0)
                {
                    _onBAMPacketsDone?.Invoke(_waitBAMparam, _receivedBAMData.ToArray());
                    _canInterface.SendMessage(_canMessageFabrik.ReadParameterBAMAck(_waitBAMparam), false, 0, out object receiveCode);
                }
            }
        }

        public void Dispose()
        {
            if (_canInterface != null)
                _canInterface.DeInit();
        }
    }

    enum TachoStatusResult
    {
        NoCan,
        NoTacho,
        Tacho
    }
}
