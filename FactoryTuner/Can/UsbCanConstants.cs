﻿
using System.Text;

namespace FactoryTuner.Can.Internal
{
    class UsbCanConstants
    {

        public static UsbCanConstants Instance
        {
            get
            {
                if (_instance == null) _instance = new UsbCanConstants();
                return _instance;
            }
        }

        private static UsbCanConstants _instance;

        public const uint TachoReadParamId = 0x18EFEE7A;

        public const uint TachoReadParamResultId = 0x18EF7AEE;
        public const uint TachoReadParamBAMId = 0x18ECFFEE;
        public const uint TachoReadParamBAMResultId = 0x18EBFFEE;

        public const uint TachoReadParamAckId = 0x06E8FF7A;
        public const uint TachoReadParamBAMAckId = 0x18E8FF7A;

        public const uint TachoWriteParamId = 0x18EFEE7A;
        public const uint TachoWriteParamAckId = 0x18E8FFEE;

        public const uint USDRequestId = 0x18DA17F9;
        public const uint USDResponseId = 0x18DAF917;

        public static readonly Encoding USDDeviceStringEncoding = Encoding.ASCII;
        public static readonly Encoding ReadStringDefaultEncoding = Encoding.GetEncoding("iso-8859-5");
    }
}
