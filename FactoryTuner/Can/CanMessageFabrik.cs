﻿using Asap1B.UsbCan;
using FactoryDataConfig;
using System;
using System.Diagnostics;

namespace FactoryTuner.Can.Internal
{
    internal class CanMessageFabrik
    {
        protected CanMessage GetMessage(uint canId, byte b0 = 0x00, byte b1 = 0x00, byte b2 = 0x00,
            byte b3 = 0x00, byte b4 = 0x00, byte b5 = 0x00, byte b6 = 0x00, byte b7 = 0x00)
        {
            return GetMessage(canId, new[] { b0, b1, b2, b3, b4, b5, b6, b7 });
        }

        protected CanMessage GetMessage(uint canId, byte[] bytes)
        {
            var canMessage = new CanMessage
            {
                //canMessage.Data всегда должен содержать 8 элементов. Иначе ошибка. Но не DataLengthCode
                Data = new byte[8]
            };
            for (int i = 0; i < 8; i++)
            {
                if (i >= bytes.Length)
                    canMessage.Data[i] = 0x00;
                else
                    canMessage.Data[i] = bytes[i];
            }

            canMessage.CanID = canId;
            canMessage.DataLengthCode = (byte)bytes.Length;
            canMessage.FrameFormat = UsbCanMsgFrameFormat.Ext;
            canMessage.TimeStamp = 10;

            //TODO [Debug] output bytes
#if DEBUG
            Debug.WriteLine($">>>>>> ID:{canId.ToString("X8")} = DATA:{CodeHelpers.ByteArrayToString(canMessage.Data)}");
#endif

            return canMessage;
        }

        public CanMessage SetParameter32(TachoParameters param, UInt32 value)
        {
            return GetMessage(UsbCanConstants.TachoWriteParamId, new byte[] { (byte)param, 0x00 }.Concat(BitConverter.GetBytes(value)));
        }

        public CanMessage SetParameter16(TachoParameters param, UInt16 value)
        {
            return GetMessage(UsbCanConstants.TachoWriteParamId, new byte[] { (byte)param, 0x00 }.Concat(BitConverter.GetBytes(value)));
        }

        public CanMessage ReadParameter(TachoParameters param)
        {
            return GetMessage(UsbCanConstants.TachoReadParamId, new byte[] { (byte)param, 0x80 });
        }

        public CanMessage ReadParameterAck(TachoParameters param)
        {
            return GetMessage(UsbCanConstants.TachoReadParamAckId, new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xEF, 0x00 });
        }

        public CanMessage ReadParameterBAMAck(TachoParameters param)
        {
            return GetMessage(UsbCanConstants.TachoReadParamBAMAckId, new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xEB, 0x00 });
        }

        public CanMessage USDPing() {
            return GetMessage(UsbCanConstants.USDRequestId, new byte[] { 0x02, 0x3E, 0x00 });
        }

        public CanMessage USDSetSessionTypeRequest(USDSessionType sessionType) {
            return GetMessage(UsbCanConstants.USDRequestId, new byte[] { 0x02, 0x10, (byte)sessionType });
        }

        public CanMessage USDReadRequest(USDParameters param) {
            //pid, msg length, read/write, 0xf1, paramId, value
            return GetMessage(UsbCanConstants.USDRequestId, new byte[] { 0x03, 0x22, 0xF1, (byte)param });
        }

        public CanMessage USDWriteRequest(USDParameters param, byte[] value) {
            //pid, msg length, read/write, 0xf1, paramId, value
            return GetMessage(UsbCanConstants.USDRequestId, new byte[] { (byte)(value.Length + 3), 0x2E, 0xF1, (byte)param }.Concat(value));
        }

        public CanMessage USDWriteRequest16(USDParameters param, UInt16 value) {
            //pid, msg length, read/write, 0xf1, paramId, value
            return GetMessage(UsbCanConstants.USDRequestId, new byte[] { (byte)(2 + 3), 0x2E, 0xF1, (byte)param, }.Concat(BitConverter.GetBytes(value)));
        }
    }

    public enum TachoParameters : byte
    {
        //Коэффициент скорости
        KSpeedU16 = 0x1,

        //Коэффициент трансмиссии
        KTransmissionU16 = 0x2,

        //Общий одометр
        OdometerTotalU32 = 0x3,

        //Суточный одометр
        OdometerDayU32 = 0x4,

        //Изготовитель (RFU)
        ManufacturerStr = 0x5,

        //Серийный номер(RFU)
        SerialNumberStr = 0x6,

        //Версия ПО (RFU)
        SoftwareVersionStr = 0x7,

        //Контрольная сумма ПО(RFU)
        SoftwareChecksumStr = 0x8,

        //Названием модели устройства
        ModelName = 0x9,
    }

    public enum USDParameters : byte {
        KSpeedU16 = 0xF2,
        KSpeedU16V2 = 0x02,
        KTransmissionU16 = 0xF3,
        KTransmissionU16V2 = 0x03,
        ModelType = 0x92,
        SerialNumber = 0x17,
        EngineType = 0x04,
        KPPType = 0x0E,
    }

    public enum USDSessionType : byte {
        Default = 0x01,
        Extended = 0x03,
    }
}