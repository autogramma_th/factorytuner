﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using FactoryDataConfig;
using FactoryTuner.CommonEntities;
using FactoryTuner.Properties;

namespace FactoryTuner
{
    public class RemoteJournalQueue
    {
        public string RemotePath { get; set; }

        private string _tempPath;

        private LinkedList<Event> _queue = new LinkedList<Event>();

        public RemoteJournalQueue(string tempPath, string remotePath)
        {
            Debug.WriteLine(nameof(RemoteJournalQueue));

            _tempPath = tempPath;
            RemotePath = remotePath;
            LoadFromTemp();
        }

        public int Count()
        {
            return _queue.Count;
        }

        private bool _isTrySendAllToRemoteAsyncNow = false;
        public void TrySendAllToRemoteAsync()
        {
            if (!_isTrySendAllToRemoteAsyncNow)
            {
                _isTrySendAllToRemoteAsyncNow = true;
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        lock (_queue)
                        {
                            Debug.WriteLine(nameof(TrySendAllToRemoteAsync));

                            if (IsMustWriteToRemote() && _queue.Count > 0)
                            {
                                var csvWorker = new CSVWorker();
                                var dataToSend = new List<JournalEventCSV>();

                                foreach (var ev in _queue)
                                    dataToSend.Add(A.I.EventToCSVEvent(ev));

                                if (csvWorker.TrySaveDataToCsvFile(dataToSend, RemotePath, true))
                                {
                                    _queue.Clear();
                                    SaveToTemp();

                                }
                            }
                        }
                    }
                    finally
                    {
                        _isTrySendAllToRemoteAsyncNow = false;
                    }
                });
            }
        }

        public void AddNewIfNeeded(Event newEvent)
        {
            lock (_queue)
            {
                Debug.WriteLine(nameof(AddNewIfNeeded));

                if (newEvent == null)
                {
                    Trace.TraceWarning(new ArgumentNullException(nameof(newEvent)).ToString());
                    return;
                }

                if (IsMustWriteToRemote())
                {
                    _queue.AddLast(newEvent);
                    TrySendAllToRemoteAsync();

                    SaveToTemp();
                }
            }
        }

        private bool IsMustWriteToRemote()
        {
            if (string.IsNullOrEmpty(RemotePath))
                return false;
            return true;
        }

        private void AddToBeginning(List<Event> events)
        {
            Debug.WriteLine(nameof(AddToBeginning));

            if (events == null)
            {
                Trace.TraceWarning(new ArgumentNullException(nameof(events)).ToString());
                return;
            }

            events.Sort(delegate (Event p1, Event p2)
            {
                return -1 * p1.Date.CompareTo(p2.Date);
            });

            foreach (var ev in events)
                if (IsMustWriteToRemote())
                    _queue.AddFirst(ev);

            TrySendAllToRemoteAsync();

            SaveToTemp();
        }

        private void SaveToTemp()
        {
            Debug.WriteLine(nameof(SaveToTemp));

            lock (_queue)
            {
                var journal = new Journal();
                journal.Events = new List<Event>();
                foreach (var q in _queue)
                    journal.Events.Add(q);
                CodeHelpers.TrySaveDataToXmlFile(journal, _tempPath);
            }
        }

        private void LoadFromTemp()
        {
            Debug.WriteLine(nameof(LoadFromTemp));
            lock (_queue)
            {
                _queue.Clear();

                var journal = CodeHelpers.TryLoadDataFromXmlFile<Journal>(_tempPath);
                if (journal != null)
                {
                    AddToBeginning(journal.Events);
                }

                SaveToTemp();
            }
        }
    }
}
