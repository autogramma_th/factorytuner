﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace FactoryTuner
{
    class CSVWorker
    {
        public CSVWorker(char separator = ';')
        {
            Separator = separator;
        }
        public char Separator { get; set; } = ';';

        public List<T> TryLoadDataFromCsvFile<T>(string filePath) where T : class
        {
            Debug.WriteLine(nameof(TryLoadDataFromCsvFile));

            Trace.TraceInformation("Try to load from: " + filePath);
            List<T> result = null;
            if (string.IsNullOrEmpty(filePath))
            {
                Trace.TraceWarning("Not valid filepath to load");
            }
            else if (!File.Exists(filePath))
            {
                Trace.TraceWarning("File not exist");
            }
            else
            {
                try
                {
                    var fileContent = File.ReadAllLines(filePath);

                    result = new List<T>();
                    if (fileContent.Length > 1)
                    {
                        for (int i = 1; i < fileContent.Length; i++)
                        {
                            var journalItem = FromCsv<T>(fileContent[i], Separator);
                            result.Add(journalItem);
                        }
                    }

                    Trace.TraceInformation("Loaded data from: " + filePath);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                }
            }
            return result;
        }
        public bool TrySaveDataToCsvFile<T>(IEnumerable<T> Data, string filePath, bool append)
        {
            Debug.WriteLine(nameof(TrySaveDataToCsvFile));

            Trace.TraceInformation("Try to save to: " + filePath);
            var result = false;
            if (string.IsNullOrEmpty(filePath))
            {
                Trace.TraceWarning("Not valid filepath to save");
            }
            else
            {
                try
                {
                    if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                    FileStream fileStream;
                    bool isWriteHeader = true;
                    var header = GetCsvHeader<T>(Separator);
                    if (append)
                    {
                        if (File.Exists(filePath) && (new FileInfo(filePath).Length) > 2)
                            isWriteHeader = false;
                        fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                    }
                    else
                        fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                    using (fileStream)
                    {
                        using (StreamWriter s = new StreamWriter(fileStream, new UTF8Encoding(true)))
                        {
                            s.AutoFlush = true;
                            if (isWriteHeader)
                                s.WriteLine(header);
                            s.Write(ToCsv(Separator, Data));
                        }
                    }
                    Trace.TraceInformation("Saved data to: " + filePath);
                    result = true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                }
            }
            return result;
        }

        string GetCsvHeader<T>(char separator)
        {
            var names = GetMemberNames<T>();
            return String.Join(separator.ToString(), names.ToArray());

        }

        string ToCsv<T>(char separator, IEnumerable<T> objectlist)
        {
            Type t = typeof(T);
            FieldInfo[] fields = t.GetFields();
            PropertyInfo[] properties = t.GetProperties();

            StringBuilder csvdata = new StringBuilder();

            if (objectlist != null)
                foreach (var o in objectlist)
                    csvdata.AppendLine(ToCsvFields(separator, fields, properties, o));

            return csvdata.ToString();
        }

        T FromCsv<T>(string line, char separator)
        {
            T newInstance = Activator.CreateInstance<T>();
            try
            {
                var lineItem = line.Split(separator);
                var names = GetMemberNames<T>();
                Type t = typeof(T);
                FieldInfo[] fields = t.GetFields();
                PropertyInfo[] properties = t.GetProperties();
                int index = 0;
                foreach (var f in fields)
                {
                    try
                    {
                        var value = lineItem[index];
                        if (f.FieldType == typeof(string))
                            value = value.Trim('\"');
                        f.SetValue(newInstance, Convert.ChangeType(value, f.FieldType));
                        index++;
                    }
                    catch { }
                }
                foreach (var p in properties)
                {
                    try
                    {
                        var value = lineItem[index];
                        if (p.PropertyType == typeof(string))
                            value = value.Trim('\"');
                        p.SetValue(newInstance, Convert.ChangeType(value, p.PropertyType), null);
                        index++;
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Bad csv format\n" + ex.Message);
                newInstance = default(T);
            }
            return newInstance;
        }

        List<string> GetMemberNames<T>()
        {
            Type t = typeof(T);
            FieldInfo[] fields = t.GetFields();
            PropertyInfo[] properties = t.GetProperties();
            List<string> names = new List<string>();
            foreach (var f in fields)
                names.Add(f.Name);
            foreach (var p in properties)
                names.Add(p.Name);
            return names;
        }

        string ToCsvFields(char separator, FieldInfo[] fields, PropertyInfo[] props, object o)
        {
            StringBuilder linie = new StringBuilder();

            if (o == null)
            {
                Trace.TraceWarning(new ArgumentNullException(nameof(o)).ToString());
                return string.Empty;
            }

            if (fields != null)
            {
                foreach (var f in fields)
                {
                    if (linie.Length > 0)
                        linie.Append(separator);

                    var x = f.GetValue(o);

                    if (x != null)
                    {
                        if (f.FieldType == typeof(string))
                            linie.Append("\"" + x.ToString() + "\"");
                        else
                            linie.Append(x.ToString());
                    }
                }
            }

            if (props != null)
            {
                foreach (var f in props)
                {
                    if (linie.Length > 0)
                        linie.Append(separator);

                    var x = f.GetValue(o, null);

                    if (x != null)
                    {
                        if (f.PropertyType == typeof(string))
                            linie.Append("\"" + x.ToString() + "\"");
                        else
                            linie.Append(x.ToString());
                    }
                }
            }

            return linie.ToString();
        }
    }
}
