﻿namespace FactoryTuner
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bnReadParams = new System.Windows.Forms.Button();
            this.ddAdmins = new System.Windows.Forms.ComboBox();
            this.bnSetParams = new System.Windows.Forms.Button();
            this.tbOdomCommon = new System.Windows.Forms.TextBox();
            this.tbOdomDay = new System.Windows.Forms.TextBox();
            this.lbOdomCommon = new System.Windows.Forms.Label();
            this.lbOdomDay = new System.Windows.Forms.Label();
            this.lbTransK = new System.Windows.Forms.Label();
            this.lbSpeedVal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbShassiNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Коэфицент трансмиссии:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Одометр (общее):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Одометр (суточное):";
            // 
            // bnReadParams
            // 
            this.bnReadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bnReadParams.Location = new System.Drawing.Point(12, 257);
            this.bnReadParams.Name = "bnReadParams";
            this.bnReadParams.Size = new System.Drawing.Size(428, 60);
            this.bnReadParams.TabIndex = 3;
            this.bnReadParams.Text = "Считать параметры";
            this.bnReadParams.UseVisualStyleBackColor = true;
            this.bnReadParams.Click += new System.EventHandler(this.bnReadParams_Click);
            // 
            // ddAdmins
            // 
            this.ddAdmins.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddAdmins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddAdmins.FormattingEnabled = true;
            this.ddAdmins.Location = new System.Drawing.Point(459, 206);
            this.ddAdmins.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ddAdmins.Name = "ddAdmins";
            this.ddAdmins.Size = new System.Drawing.Size(379, 33);
            this.ddAdmins.TabIndex = 4;
            // 
            // bnSetParams
            // 
            this.bnSetParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSetParams.Location = new System.Drawing.Point(459, 257);
            this.bnSetParams.Name = "bnSetParams";
            this.bnSetParams.Size = new System.Drawing.Size(379, 60);
            this.bnSetParams.TabIndex = 5;
            this.bnSetParams.Text = "Установить параметры";
            this.bnSetParams.UseVisualStyleBackColor = true;
            this.bnSetParams.Click += new System.EventHandler(this.bnSetParams_Click);
            // 
            // tbOdomCommon
            // 
            this.tbOdomCommon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOdomCommon.Location = new System.Drawing.Point(459, 111);
            this.tbOdomCommon.Name = "tbOdomCommon";
            this.tbOdomCommon.Size = new System.Drawing.Size(379, 30);
            this.tbOdomCommon.TabIndex = 6;
            this.tbOdomCommon.TextChanged += new System.EventHandler(this.odom_TextChanged);
            // 
            // tbOdomDay
            // 
            this.tbOdomDay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOdomDay.Location = new System.Drawing.Point(459, 160);
            this.tbOdomDay.Name = "tbOdomDay";
            this.tbOdomDay.Size = new System.Drawing.Size(379, 30);
            this.tbOdomDay.TabIndex = 7;
            this.tbOdomDay.TextChanged += new System.EventHandler(this.odom_TextChanged);
            // 
            // lbOdomCommon
            // 
            this.lbOdomCommon.Location = new System.Drawing.Point(205, 114);
            this.lbOdomCommon.Name = "lbOdomCommon";
            this.lbOdomCommon.Size = new System.Drawing.Size(178, 25);
            this.lbOdomCommon.TabIndex = 8;
            this.lbOdomCommon.Text = "Х ХХХ ХХХ";
            this.lbOdomCommon.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbOdomDay
            // 
            this.lbOdomDay.Location = new System.Drawing.Point(226, 163);
            this.lbOdomDay.Name = "lbOdomDay";
            this.lbOdomDay.Size = new System.Drawing.Size(157, 25);
            this.lbOdomDay.TabIndex = 9;
            this.lbOdomDay.Text = "Х ХХХ ХХХ";
            this.lbOdomDay.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTransK
            // 
            this.lbTransK.Location = new System.Drawing.Point(268, 65);
            this.lbTransK.Name = "lbTransK";
            this.lbTransK.Size = new System.Drawing.Size(115, 25);
            this.lbTransK.TabIndex = 10;
            this.lbTransK.Text = "XX.XXХ";
            this.lbTransK.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbSpeedVal
            // 
            this.lbSpeedVal.Location = new System.Drawing.Point(231, 16);
            this.lbSpeedVal.Name = "lbSpeedVal";
            this.lbSpeedVal.Size = new System.Drawing.Size(152, 25);
            this.lbSpeedVal.TabIndex = 12;
            this.lbSpeedVal.Text = "XXXXX";
            this.lbSpeedVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Коэфицент скорости:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(414, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "км";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(414, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 25);
            this.label6.TabIndex = 14;
            this.label6.Text = "км";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 25);
            this.label7.TabIndex = 15;
            this.label7.Text = "Администратор:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(414, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 25);
            this.label8.TabIndex = 16;
            this.label8.Text = "№ шасси:";
            // 
            // tbShassiNum
            // 
            this.tbShassiNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbShassiNum.Location = new System.Drawing.Point(510, 13);
            this.tbShassiNum.Name = "tbShassiNum";
            this.tbShassiNum.Size = new System.Drawing.Size(328, 30);
            this.tbShassiNum.TabIndex = 17;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 329);
            this.Controls.Add(this.tbShassiNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbSpeedVal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbTransK);
            this.Controls.Add(this.lbOdomDay);
            this.Controls.Add(this.lbOdomCommon);
            this.Controls.Add(this.tbOdomDay);
            this.Controls.Add(this.tbOdomCommon);
            this.Controls.Add(this.bnSetParams);
            this.Controls.Add(this.ddAdmins);
            this.Controls.Add(this.bnReadParams);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры тахографа";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bnReadParams;
        private System.Windows.Forms.ComboBox ddAdmins;
        private System.Windows.Forms.Button bnSetParams;
        private System.Windows.Forms.TextBox tbOdomCommon;
        private System.Windows.Forms.TextBox tbOdomDay;
        private System.Windows.Forms.Label lbOdomCommon;
        private System.Windows.Forms.Label lbOdomDay;
        private System.Windows.Forms.Label lbTransK;
        private System.Windows.Forms.Label lbSpeedVal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbShassiNum;
    }
}