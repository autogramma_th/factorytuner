﻿using FactoryDataConfig;
using FactoryTuner.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace FactoryTuner
{
    public partial class CheckPasswordForm : Form
    {
        public NameNumber SelectedMechanic { set; private get; }

        public static CheckPasswordForm Current;

        public CheckPasswordForm()
        {
            Current = this;
            InitializeComponent();
            this.DialogResult = DialogResult.None;
        }

        private void SetPasswordForm_Shown(object sender, EventArgs e)
        {
            if (SelectedMechanic == null)
            {
                MessageBox.Show(Current, "Механик не выбран");
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            if (IsInputDone())
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(Current, "Пароль неверный");
            }
        }

        bool IsInputDone()
        {
            var md5Inputed = CodeHelpers.GetMd5Hash(tbPass1.Text);
            var md5OfMechanic = SelectedMechanic.PasswordHash;
            if (String.IsNullOrWhiteSpace(md5OfMechanic))
                return false;
            return string.Equals(md5Inputed, md5OfMechanic, StringComparison.OrdinalIgnoreCase);
        }
    }
}
