﻿namespace FactoryTuner
{
    partial class JournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JournalForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dpFrom = new System.Windows.Forms.DateTimePicker();
            this.dpTo = new System.Windows.Forms.DateTimePicker();
            this.bnBack = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tbJournalPath = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.bnSearch = new System.Windows.Forms.Button();
            this.cbSearchProperty = new System.Windows.Forms.ComboBox();
            this.bnClearSearch = new System.Windows.Forms.Button();
            this.bnExport = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbManualInput = new System.Windows.Forms.CheckBox();
            this.cbTireSize = new System.Windows.Forms.CheckBox();
            this.cbTireName = new System.Windows.Forms.CheckBox();
            this.cbKPName = new System.Windows.Forms.CheckBox();
            this.cbKPType = new System.Windows.Forms.CheckBox();
            this.cbNomerShassi = new System.Windows.Forms.CheckBox();
            this.cbGPM = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // dpFrom
            // 
            resources.ApplyResources(this.dpFrom, "dpFrom");
            this.dpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpFrom.Name = "dpFrom";
            this.dpFrom.ValueChanged += new System.EventHandler(this.dp_ValueChanged);
            // 
            // dpTo
            // 
            resources.ApplyResources(this.dpTo, "dpTo");
            this.dpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpTo.Name = "dpTo";
            this.dpTo.ValueChanged += new System.EventHandler(this.dp_ValueChanged);
            // 
            // bnBack
            // 
            resources.ApplyResources(this.bnBack, "bnBack");
            this.bnBack.Name = "bnBack";
            this.bnBack.UseVisualStyleBackColor = true;
            this.bnBack.Click += new System.EventHandler(this.bnBack_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            // 
            // tbJournalPath
            // 
            resources.ApplyResources(this.tbJournalPath, "tbJournalPath");
            this.tbJournalPath.Name = "tbJournalPath";
            this.tbJournalPath.ReadOnly = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            resources.ApplyResources(this.dataGridView2, "dataGridView2");
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.ColumnHeadersVisible = false;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            // 
            // Column1
            // 
            resources.ApplyResources(this.Column1, "Column1");
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            resources.ApplyResources(this.Column2, "Column2");
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // tbSearch
            // 
            resources.ApplyResources(this.tbSearch, "tbSearch");
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            this.tbSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSearch_KeyPress);
            // 
            // bnSearch
            // 
            this.bnSearch.BackgroundImage = global::FactoryTuner.Properties.Resources.ic_search_api_holo_light;
            resources.ApplyResources(this.bnSearch, "bnSearch");
            this.bnSearch.Name = "bnSearch";
            this.bnSearch.UseVisualStyleBackColor = true;
            this.bnSearch.Click += new System.EventHandler(this.bnSearch_Click);
            // 
            // cbSearchProperty
            // 
            this.cbSearchProperty.DropDownWidth = 300;
            this.cbSearchProperty.FormattingEnabled = true;
            resources.ApplyResources(this.cbSearchProperty, "cbSearchProperty");
            this.cbSearchProperty.Name = "cbSearchProperty";
            this.cbSearchProperty.SelectedIndexChanged += new System.EventHandler(this.cbSearchProperty_SelectedIndexChanged);
            // 
            // bnClearSearch
            // 
            this.bnClearSearch.BackColor = System.Drawing.Color.White;
            this.bnClearSearch.BackgroundImage = global::FactoryTuner.Properties.Resources.presence_offline;
            resources.ApplyResources(this.bnClearSearch, "bnClearSearch");
            this.bnClearSearch.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bnClearSearch.FlatAppearance.BorderSize = 0;
            this.bnClearSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bnClearSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bnClearSearch.Name = "bnClearSearch";
            this.bnClearSearch.UseVisualStyleBackColor = false;
            this.bnClearSearch.Click += new System.EventHandler(this.bnClearSearch_Click);
            // 
            // bnExport
            // 
            resources.ApplyResources(this.bnExport, "bnExport");
            this.bnExport.Name = "bnExport";
            this.bnExport.UseVisualStyleBackColor = true;
            this.bnExport.Click += new System.EventHandler(this.bnExport_Click);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.cbManualInput);
            this.groupBox1.Controls.Add(this.cbTireSize);
            this.groupBox1.Controls.Add(this.cbTireName);
            this.groupBox1.Controls.Add(this.cbKPName);
            this.groupBox1.Controls.Add(this.cbKPType);
            this.groupBox1.Controls.Add(this.cbNomerShassi);
            this.groupBox1.Controls.Add(this.cbGPM);
            this.groupBox1.Controls.Add(this.bnExport);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // cbManualInput
            // 
            resources.ApplyResources(this.cbManualInput, "cbManualInput");
            this.cbManualInput.Name = "cbManualInput";
            this.cbManualInput.UseVisualStyleBackColor = true;
            // 
            // cbTireSize
            // 
            resources.ApplyResources(this.cbTireSize, "cbTireSize");
            this.cbTireSize.Name = "cbTireSize";
            this.cbTireSize.UseVisualStyleBackColor = true;
            // 
            // cbTireName
            // 
            resources.ApplyResources(this.cbTireName, "cbTireName");
            this.cbTireName.Name = "cbTireName";
            this.cbTireName.UseVisualStyleBackColor = true;
            // 
            // cbKPName
            // 
            resources.ApplyResources(this.cbKPName, "cbKPName");
            this.cbKPName.Name = "cbKPName";
            this.cbKPName.UseVisualStyleBackColor = true;
            // 
            // cbKPType
            // 
            resources.ApplyResources(this.cbKPType, "cbKPType");
            this.cbKPType.Name = "cbKPType";
            this.cbKPType.UseVisualStyleBackColor = true;
            // 
            // cbNomerShassi
            // 
            resources.ApplyResources(this.cbNomerShassi, "cbNomerShassi");
            this.cbNomerShassi.Name = "cbNomerShassi";
            this.cbNomerShassi.UseVisualStyleBackColor = true;
            this.cbNomerShassi.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // cbGPM
            // 
            resources.ApplyResources(this.cbGPM, "cbGPM");
            this.cbGPM.Name = "cbGPM";
            this.cbGPM.UseVisualStyleBackColor = true;
            // 
            // JournalForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bnClearSearch);
            this.Controls.Add(this.cbSearchProperty);
            this.Controls.Add(this.bnSearch);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.tbJournalPath);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.bnBack);
            this.Controls.Add(this.dpTo);
            this.Controls.Add(this.dpFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "JournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.JournalForm_FormClosing);
            this.Load += new System.EventHandler(this.JournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dpFrom;
        private System.Windows.Forms.DateTimePicker dpTo;
        private System.Windows.Forms.Button bnBack;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tbJournalPath;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button bnSearch;
        private System.Windows.Forms.ComboBox cbSearchProperty;
        private System.Windows.Forms.Button bnClearSearch;
        private System.Windows.Forms.Button bnExport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbTireSize;
        private System.Windows.Forms.CheckBox cbTireName;
        private System.Windows.Forms.CheckBox cbKPName;
        private System.Windows.Forms.CheckBox cbKPType;
        private System.Windows.Forms.CheckBox cbNomerShassi;
        private System.Windows.Forms.CheckBox cbGPM;
        private System.Windows.Forms.CheckBox cbManualInput;
    }
}