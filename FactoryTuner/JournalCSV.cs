﻿using System;

namespace FactoryTuner
{
    public class JournalEventCSV
    {
        //Добавлять в конец для обратной совместимости !!!!!!!!!!
        public string Date { get; set; } = string.Empty;
        public string ChassisNuber { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public uint UserNumber { get; set; }
        public string UserMachineName { get; set; } = string.Empty;
        public bool IsOverride { get; set; }
        public string TireSize { get; set; } = string.Empty;
        public string TireName { get; set; } = string.Empty;
        public float TireDR { get; set; }
        public uint TransmissionType { get; set; }
        public string TransmissionName { get; set; } = string.Empty;
        public float TransmissionN { get; set; }
        //Добавлять в конец для обратной совместимости !!!!!!!!!!
        public float GPM { get; set; }
        public UInt16 K { get; set; }
        public string SN { get; set; } = string.Empty;
        public double OdometerTotal { get; set; }
        public double OdometerDay { get; set; }
        public double PrevOdometerTotal { get; set; }
        public double PrevOdometerDay { get; set; }
        public bool IsOdometerSet { get; set; }
        public string Crc { get; set; } = string.Empty;
        public string Manufacturer { get; set; } = string.Empty;
        public string Model { get; set; } = string.Empty;
        public bool IsUsdDeviceWritten { get; set; } = false;
        public string USDDeviceSN { get; set; } = string.Empty;
        //Добавлять в конец для обратной совместимости !!!!!!!!!!
    }
}
