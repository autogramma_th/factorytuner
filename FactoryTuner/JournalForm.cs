﻿using FactoryDataConfig;
using FactoryTuner.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FactoryTuner
{
    public partial class JournalForm : Form
    {
        public static JournalForm Current;

        private bool _isSerialFilter;
        private JournalEventCSV _selectedElement;
        private Form1 _mainForm;

        public JournalForm(Form1 mainForm)
        {
            Current = this;
            this._mainForm = mainForm;
            InitializeComponent();
            tbSearch.LostFocus += TbSearch_LostFocus;
        }

        private void TbSearch_LostFocus(object sender, EventArgs e)
        {
            bnSearch_Click(null, null);
        }

        public DialogResult ShowDialog(bool isSerialFilter)
        {
            _isSerialFilter = isSerialFilter;
            return base.ShowDialog();
        }

        private void bnBack_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(bnBack_Click));
            this.Close();
        }

        private void JournalForm_Load(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(JournalForm_Load));

            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);

            if (_isSerialFilter)
            {
                this.Text = "Журнал. Фильтр по SN: " + A.I.LastReadedSerial;
            }
            else
            {
                this.Text = "Журнал";
            }

            //this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();

            var journalSource = A.I.ReLoadLocalJournalFromSource();
            tbJournalPath.Text = journalSource;

            SetDatepickers();
            RestoreExportCheckBoxValues();
        }

        private void RestoreExportCheckBoxValues()
        {
            cbGPM.Checked = Settings.Default.JournalCb1;
            cbNomerShassi.Checked = Settings.Default.JournalCb2;
            cbKPType.Checked = Settings.Default.JournalCb3;
            cbKPName.Checked = Settings.Default.JournalCb4;
            cbTireSize.Checked = Settings.Default.JournalCb5;
            cbTireName.Checked = Settings.Default.JournalCb6;
            cbManualInput.Checked = Settings.Default.JournalCb7;
        }

        private void SetDatepickers()
        {
            Debug.WriteLine(nameof(SetDatepickers));

            DateTime minDate = DateTime.Now;
            DateTime maxDate = DateTime.Now;

            foreach (var jEvent in A.I.Journal.Events)
            {
                if (jEvent != null)
                {
                    if (A.I.TryParseCSVDate(jEvent.Date, out DateTime dtTemp))
                    {
                        if (dtTemp > maxDate)
                            maxDate = dtTemp;
                        if (dtTemp < minDate)
                            minDate = dtTemp;
                    }
                }
            }

            dpFrom.MinDate = minDate.AddDays(-1);
            dpFrom.MaxDate = maxDate;
            dpTo.MinDate = minDate;
            dpTo.MaxDate = maxDate;
            dpTo.Value = maxDate;

            var fromValue = DateTime.Now.AddDays(-1);

            //Если фильтр по SN, то вначале выборку за месяц
            if (!_isSerialFilter)
                fromValue = DateTime.Now.AddDays(-1);
            else
                fromValue = DateTime.Now.AddDays(-31);

            if (fromValue < minDate)
                fromValue = minDate.AddDays(-1);

            dpFrom.Value = fromValue;
        }

        private List<JournalEventCSV> GetEventsBySelectedPeriod()
        {
            var from = dpFrom.Value;
            from = new DateTime(from.Year, from.Month, from.Day);

            var to = dpTo.Value;
            to = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);

            List<JournalEventCSV> eventsToShow = new List<JournalEventCSV>();
            for (int i = A.I.Journal.Events.Count - 1; i >= 0; i--)
            {
                var jEvent = A.I.Journal.Events[i];
                if (jEvent != null)
                {
                    if (A.I.TryParseCSVDate(jEvent.Date, out DateTime dtTemp))
                    {
                        if (dtTemp >= from && dtTemp <= to)
                        {
                            eventsToShow.Add(A.I.EventToCSVEvent(jEvent));
                        }
                    }
                    else
                    {
                        var csvEvent = A.I.EventToCSVEvent(jEvent);
                        csvEvent.Date = "Data error in csv";
                        Trace.TraceError("Data error in csv");
                        eventsToShow.Add(csvEvent);
                    }
                }
                else
                {
                    Trace.TraceError("Parse error");
                    eventsToShow.Add(null);
                }
            }

            if (_isSerialFilter)
            {
                var searchSerial = A.I.LastReadedSerial ?? "";
                eventsToShow = eventsToShow.FindAll(x => x.SN == searchSerial);
            }


            return eventsToShow;
        }

        private void SetDataSource(object source)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;

            dataGridView1.DataSource = source;

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                dataGridView1.Columns[i].Visible = false;
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            }
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            //Header localization
            dataGridView1.Columns[nameof(JournalEventCSV.Date)].HeaderText = "Время";
            dataGridView1.Columns[nameof(JournalEventCSV.Date)].Visible = true;
            dataGridView1.Columns[nameof(JournalEventCSV.ChassisNuber)].HeaderText = "№ шасси";
            dataGridView1.Columns[nameof(JournalEventCSV.ChassisNuber)].Visible = true;
            dataGridView1.Columns[nameof(JournalEventCSV.UserName)].HeaderText = Resources.strJECSVUserName;
            dataGridView1.Columns[nameof(JournalEventCSV.UserName)].Visible = true;
            dataGridView1.Columns[nameof(JournalEventCSV.UserName)].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[nameof(JournalEventCSV.UserNumber)].HeaderText = "№ мех.";
            dataGridView1.Columns[nameof(JournalEventCSV.UserNumber)].Visible = true;

            dataGridView1.Refresh();
        }


        private void FillGrid()
        {
            Debug.WriteLine(nameof(FillGrid));

            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.AllowUserToResizeColumns = true;

            var eventsToShow = GetEventsBySelectedPeriod();

            _selectedElement = null;
            SetDataSource(eventsToShow);

            ClearPropertyGrid();

            if (eventsToShow.Count > 0)
            {
                _selectedElement = eventsToShow.First();
                FillSelectionInformation(eventsToShow.First());
            }
        }

        private void dp_ValueChanged(object sender, EventArgs e)
        {
            Debug.WriteLine(nameof(dp_ValueChanged));

            FillGrid();
        }

        private string GetTransmissionTypeString(uint transType)
        {
            if (transType == 1)
                return Resources.strTrans0;
            else if (transType == 2)
                return Resources.strTrans1;
            else if (transType == 3)
                return Resources.strTrans2;
            else
                return "-";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                ClearPropertyGrid();
                _selectedElement = ((IEnumerable<JournalEventCSV>)dataGridView1.DataSource).ElementAt(e.RowIndex);
                FillSelectionInformation(_selectedElement);
            }
        }

        private void FillSelectionInformation(JournalEventCSV selectedElement)
        {
            AddPropertyRow("Время", selectedElement.Date, nameof(selectedElement.Date));
            AddPropertyRow(Resources.strJECSVChassisNuber, selectedElement.ChassisNuber, nameof(selectedElement.ChassisNuber));
            AddPropertyRow(Resources.strJECSVUserName, selectedElement.UserName, nameof(selectedElement.UserName));
            AddPropertyRow(Resources.strJECSVUserNumber, selectedElement.UserNumber.ToString(), nameof(selectedElement.UserNumber));

            if (selectedElement.IsOverride)
                AddPropertyRow(Resources.strJECSVIsOverride, "Да");
            else
                AddPropertyRow(Resources.strJECSVIsOverride, "Нет");

            AddPropertyRow("Имя станции", selectedElement.UserMachineName, nameof(selectedElement.UserMachineName));

            if (selectedElement.IsOdometerSet)
            {
                AddPropertyRow("Вид процедуры", "Настройка одометра");
            }
            else
            {
                AddPropertyRow("Вид процедуры", "Настройка тахографа");
            }

            AddPropertyRow("Модель", selectedElement.Model.ToString(), nameof(selectedElement.Model));
            AddPropertyRow("Производитель", selectedElement.Manufacturer.ToString(), nameof(selectedElement.Manufacturer));
            AddPropertyRow("SN", selectedElement.SN.ToString(), nameof(selectedElement.SN));
            AddPropertyRow("CRC", selectedElement.Crc, nameof(selectedElement.Crc));
            AddPropertyRow("К Скорости", selectedElement.K.ToString(), nameof(selectedElement.K));
            AddPropertyRow("К Трансмиссии", selectedElement.TransmissionN.ToString(), nameof(selectedElement.TransmissionN));

            if (selectedElement.IsOdometerSet)
            {
                AddPropertyRow("Одометр (сутки)", selectedElement.OdometerDay.ToString());
                AddPropertyRow("Одометр (общее)", selectedElement.OdometerTotal.ToString());
                AddPropertyRow("Одометр пред. (сутки)", selectedElement.PrevOdometerDay.ToString());
                AddPropertyRow("Одометр пред. (общее)", selectedElement.PrevOdometerTotal.ToString());
            }
            else
            {
                AddPropertyRow(Resources.strJECSVTireSize, selectedElement.TireSize.ToString());
                AddPropertyRow(Resources.strJECSVTireName, selectedElement.TireName.ToString());
                AddPropertyRow(Resources.strJECSVTireDR, selectedElement.TireDR.ToString());
                AddPropertyRow(Resources.strJECSVTransmissionName, selectedElement.TransmissionName.ToString());
                AddPropertyRow("Расположение датчика скорости", GetTransmissionTypeString(selectedElement.TransmissionType));
                AddPropertyRow(Resources.strJECSVGPM, selectedElement.GPM.ToString());
            }
            AddPropertyRow("Комб. устр: SN", selectedElement.USDDeviceSN);
            AddPropertyRow("Комб. устр: пароль", "N/A");
            AddPropertyRow("Комб. устр: настроена", selectedElement.IsUsdDeviceWritten ? "Да" : "Нет");

            cbGPM.Enabled = !_selectedElement.IsOdometerSet;
            cbNomerShassi.Enabled = !_selectedElement.IsOdometerSet;
            cbKPType.Enabled = !_selectedElement.IsOdometerSet;
            cbKPName.Enabled = !_selectedElement.IsOdometerSet;
            cbTireSize.Enabled = !_selectedElement.IsOdometerSet;
            cbTireName.Enabled = !_selectedElement.IsOdometerSet;
            cbManualInput.Enabled = !_selectedElement.IsOdometerSet && _selectedElement.IsOverride;
        }

        /// <summary>
        /// Если входящая строка пустая, то отдать "не указано"
        /// </summary>
        private string NAorS(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "не указано";
            }
            return value;
        }

        private void AddPropertyRow(string paramName, string value, string propname = null)
        {
            var index = dataGridView2.Rows.Add();
            dataGridView2.Rows[index].Cells[0].Value = paramName;
            dataGridView2.Rows[index].Cells[1].Value = NAorS(value);

            cbSearchProperty.DisplayMember = "Text";
            cbSearchProperty.ValueMember = "Value";

            if (!string.IsNullOrWhiteSpace(propname))
                cbSearchProperty.Items.Add(new TextValueItem<string>(paramName, propname));
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ClearPropertyGrid()
        {
            dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            cbSearchProperty.Items.Clear();
        }

        private void bnClearSearch_Click(object sender, EventArgs e)
        {
            cbSearchProperty.SelectedIndex = -1;
            tbSearch.Clear();
            SetDataSource(GetEventsBySelectedPeriod());
        }

        private void bnSearch_Click(object sender, EventArgs e)
        {
            var eventsToShow = GetEventsBySelectedPeriod();
            if (cbSearchProperty.SelectedItem != null)
            {
                var selectedItem = (TextValueItem<string>)cbSearchProperty.SelectedItem;
                var searchPropName = selectedItem.Value;
                if (searchPropName != null && !string.IsNullOrWhiteSpace(tbSearch.Text))
                {
                    var props = typeof(JournalEventCSV).GetProperty(searchPropName);
                    eventsToShow = eventsToShow.FindAll(x => props.GetValue(x, null).ToString().ToLower().Contains(tbSearch.Text.ToLower()));
                }
            }

            SetDataSource(eventsToShow);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void bnExport_Click(object sender, EventArgs e)
        {
            if (this._mainForm == null)
            {
                Trace.TraceError("_mainForm in journal == null");
                MessageBox.Show(Current, "Программная ошибка");
            }
            else
            {
                if (_selectedElement == null)
                {
                    MessageBox.Show(Current, "Выберите запись");
                    return;
                }

                if (cbGPM.Checked)
                {
                    var items = _mainForm.cbMTB.Items.ToEnumerable<float>();
                    if (items.Any(x => x == _selectedElement.GPM))
                        _mainForm.cbMTB.SelectedItem = items.First(x => x == _selectedElement.GPM);
                }

                if (cbNomerShassi.Checked)
                {
                    _mainForm.tbChassis.Text = _selectedElement.ChassisNuber;
                }

                ComboBox cbTransmission = null;
                if (cbKPType.Checked || cbKPName.Checked)
                {
                    cbKPType.Checked = true;
                    if (_selectedElement.TransmissionType == 1)
                    {
                        _mainForm.rbManual.Checked = true;
                        cbTransmission = _mainForm.cbManuals;
                    }
                    else if (_selectedElement.TransmissionType == 2)
                    {
                        _mainForm.rbAuto.Checked = true;
                        cbTransmission = _mainForm.cbAutos;
                    }
                    else if (_selectedElement.TransmissionType == 3)
                    {
                        _mainForm.rbTC.Checked = true;
                        cbTransmission = _mainForm.cbTKs;
                    }
                }

                if (cbKPName.Checked && cbTransmission != null)
                {
                    var trans = A.I.MainData.Transmissions.FirstOrDefault(x => x.Type == _selectedElement.TransmissionType);
                    if (trans != null)
                        _mainForm.cbManuals.SelectedItem = trans;
                }

                if (cbTireSize.Checked || cbTireName.Checked)
                {
                    cbTireSize.Checked = true;

                    var tireWithSize = A.I.MainData.TireModels.FirstOrDefault(x => x.Size == _selectedElement.TireSize);
                    if (tireWithSize != null)
                    {
                        _mainForm.cbTireMarking.SelectedItem = tireWithSize;
                    }
                }
                if (cbTireName.Checked)
                {
                    var tire = A.I.MainData.TireModels.FirstOrDefault(x => x.Size == _selectedElement.TireSize && x.Name == _selectedElement.TireName);
                    if (tire != null)
                    {
                        _mainForm.cbTireModel.SelectedItem = tire;
                    }
                }

                if (cbManualInput.Checked)
                {
                    _mainForm.cbManualInput.Checked = _selectedElement.IsOverride;
                    _mainForm.tbDynRadius.Text = _selectedElement.TireDR.ToString();
                    _mainForm.tbK.Text = _selectedElement.K.ToString();
                    _mainForm.tbPulses.Text = _selectedElement.TransmissionN.ToString();
                }

                this.Close();
            }
        }

        private void cbSearchProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.JournalSearchPropertyIndex = cbSearchProperty.SelectedIndex;
        }

        private void JournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.JournalCb1 = cbGPM.Checked;
            Settings.Default.JournalCb2 = cbNomerShassi.Checked;
            Settings.Default.JournalCb3 = cbKPType.Checked;
            Settings.Default.JournalCb4 = cbKPName.Checked;
            Settings.Default.JournalCb5 = cbTireSize.Checked;
            Settings.Default.JournalCb6 = cbTireName.Checked;
            Settings.Default.JournalCb7 = cbManualInput.Checked;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                bnSearch_Click(null, null);
        }
    }
}
