﻿using FactoryDataConfig;
using FactoryTuner.Can.Internal;
using FactoryTuner.CommonEntities;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FactoryTuner
{
    public partial class Admin : Form
    {
        public static Admin Current;
        private TextBox mainFormShassiNumberTextBox;
        public Admin(TextBox tbShassiNumber)
        {
            InitializeComponent();
            if (tbShassiNumber != null)
            {
                mainFormShassiNumberTextBox = tbShassiNumber;
                this.tbShassiNum.Text = tbShassiNumber.Text;
            }
            Current = this;
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            ddAdmins.Items.Clear();
            ddAdmins.DisplayMember = nameof(NameNumber.ToString);
            ddAdmins.Items.Add(new NameNumber("Не указан", 0));
            foreach (var user in A.I.AuthData.NameNumbers.FindAll(x => x.Role == 1).OrderBy(x => x.Name))
                ddAdmins.Items.Add(user);

            lbOdomCommon.Text = "?";
            lbOdomDay.Text = "?";
            lbSpeedVal.Text = "?";
            lbTransK.Text = "?";
            bnSetParams.Enabled = false;
        }

        private void bnReadParams_Click(object sender, EventArgs e)
        {
            FillFromDevice();
        }

        private void FillFromDevice()
        {
            bnSetParams.Enabled = true;
            tbOdomDay.Enabled = A.I.IsDeviceConnectedAndValid;
            tbOdomCommon.Enabled = A.I.IsDeviceConnectedAndValid;


            A.I.GetDeviceParameter<UInt32>(TachoParameters.OdometerTotalU32, (v) =>
            {
                BeginInvoke(new Action(() =>
                {

                    A.I.LastReadedOdomenterTotalM = v;
                    lbOdomCommon.Text = CodeHelpers.DeviceMetersToKm(v).ToString();
                    tbOdomCommon.Text = lbOdomCommon.Text;
                }));
            });

            A.I.GetDeviceParameter<UInt32>(TachoParameters.OdometerDayU32, (v) =>
            {
                BeginInvoke(new Action(() =>
                {
                    A.I.LastReadedOdomenterDayM = v;
                    lbOdomDay.Text = CodeHelpers.DeviceMetersToKm(v).ToString("0.#");
                    tbOdomDay.Text = lbOdomDay.Text;
                }));
            });

            A.I.GetDeviceParameter<UInt16>(TachoParameters.KSpeedU16, (v) =>
            {
                A.I.LastReadedKSpeed = v;
                BeginInvoke(new Action(() =>
                {
                    lbSpeedVal.Text = v.ToString();
                }));
            });

            A.I.GetDeviceParameter<UInt16>(TachoParameters.KTransmissionU16, (v) =>
            {
                A.I.LastReadedTransmissionN = v;
                BeginInvoke(new Action(() =>
                {
                    lbTransK.Text = (v * 0.001).ToString();
                }));
            });
        }

        private void bnSetParams_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbShassiNum.Text))
            {
                this.mainFormShassiNumberTextBox.Text = tbShassiNum.Text; //Записываем номер шасси в главную форму. Чтобы не копипастить
                if (double.TryParse(tbOdomDay.Text, out double odomDay))
                {
                    if (double.TryParse(tbOdomCommon.Text, out double odomTotal))
                    {
                        var selectedUser = ddAdmins.SelectedItem == null ? null : (NameNumber)ddAdmins.SelectedItem;

                        var checkPassDialog = new CheckPasswordForm
                        {
                            SelectedMechanic = selectedUser
                        };
                        if (checkPassDialog.ShowDialog() == DialogResult.OK)
                        {
                            bool isDone = false;

                            isDone = A.I.SetDeviceParameter<UInt32>(TachoParameters.OdometerTotalU32, CodeHelpers.KmToDeviceMeters(odomTotal));
                            if (isDone)
                                isDone = A.I.SetDeviceParameter<UInt32>(TachoParameters.OdometerDayU32, CodeHelpers.KmToDeviceMeters(odomDay));

                            if (!isDone)
                                MessageBox.Show(Current, "Ошибка записи в устройство");
                            else
                            {
                                if (!A.I.TryWriteJournalEntry(selectedUser, A.I.LastReadedOdomenterTotalM, A.I.LastReadedOdomenterDayM, odomTotal, odomDay, A.I.LastReadedSerial, A.I.LastReadedKSpeed, A.I.LastReadedTransmissionN, A.I.LastReadedManufacturer, A.I.LastReadedModel, A.I.LastReadedCrc, tbShassiNum.Text))
                                {
                                    MessageBox.Show(Current, "Ошибка записи в журнал");
                                }
                                else
                                {
                                    MessageBox.Show(Current, "Готово");
                                }
                            }
                            FillFromDevice();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(Current, "Введите номер шасси.");
            }
        }

        private void odom_TextChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("odom_TextChanged");
            var textBox = (TextBox)sender;
            if (double.TryParse(textBox.Text, out double result))
            {
                textBox.ForeColor = Color.Black;
                if (result < 0d)
                {
                    result = 0d;
                    textBox.Text = result.ToString("0.#");
                }
                else if (result > 21055406d)
                {
                    result = 21055406d;
                    textBox.Text = result.ToString("0.#");
                }
                else if ((result - Math.Round(result, 1)) != 0)
                {
                    textBox.Text = result.ToString("0.#");
                }
            }
            else
            {
                textBox.ForeColor = Color.Red;
            }
            textBox.SelectionStart = textBox.Text.Length;
            textBox.SelectionLength = 0;
        }
    }
}
