﻿using FactoryDataConfig;
using FactoryTuner.Can;
using FactoryTuner.CommonEntities;
using FactoryTuner.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FactoryTuner
{
    /// <summary>
    /// Main entry point
    /// </summary>
    public class A : IDisposable
    {
        #region I
        private static A instance;
        private static readonly object syncRoot = new object();

        public static A I
        {
            get
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new A();
                }
                return instance;
            }
        }

        internal void Init()
        {
            _canWorker = new CanWorker();
            GetDeviceStatus();
            RemoteJournalQueue = new RemoteJournalQueue(_journalQueueTempFile, GetRemoteJournalPathOrNull());
        }
        #endregion

        public Configuration Configuration { get; private set; } = new Configuration();
        public AuthData AuthData { get; private set; } = new AuthData();
        public MainData MainData { get; private set; } = new MainData();
        public CalculatedData CalculatedData { get; private set; } = new CalculatedData();
        public Journal Journal { get; private set; } = new Journal();
        public RemoteJournalQueue RemoteJournalQueue { get; private set; }

        private readonly string _cfgFilePath = Path.Combine(SharedConstants.DefaultDataDirName, SharedConstants.DefaultConfigFileName);
        private readonly string _journalDateFormat = "dd.MM.yyyy HH:mm:ss";
        private const string _journalQueueTempFile = @"temp/journalQueueToRemote.xml";
        private CSVWorker csvWorker = new CSVWorker();
        public readonly string JournalDefaultPath = Path.Combine(SharedConstants.DefaultDataDirName, SharedConstants.DefaultJournalFileName);
        public readonly string AuthDataDefaultPath = Path.Combine(SharedConstants.DefaultDataDirName, SharedConstants.DefaultUsersConfigFileName);
        public readonly string MainDataDefaultPath = Path.Combine(SharedConstants.DefaultDataDirName, SharedConstants.DefaultAutoDataFileName);


        public string LastReadedSerial { get; set; }
        public string LastReadedUSDSerial { get; set; }
        public string LastReadedUSDModel { get; set; }
        public string LastReadedManufacturer { get; set; }
        public string LastReadedModel { get; set; }
        public string LastReadedCrc { get; set; }
        public UInt16 LastReadedKSpeed { get; set; }
        public float LastReadedTransmissionN { get; set; }
        public UInt32 LastReadedOdomenterDayM { get; set; }
        public UInt32 LastReadedOdomenterTotalM { get; set; }

        private CanWorker _canWorker;
        private FileStream _logFileStream;
        private StreamWriter _streamWriter;
        private TextWriterTraceListener _traceListener;

        public bool IsDeviceConnectedAndValid { get; private set; } = false;
        public bool IsUSDDeviceConnectedAndValid { get; private set; } = false;

        private A()
        {
            Debug.WriteLine(nameof(A));

            InitTrace();
            Trace.TraceInformation(CodeHelpers.GetAppVersionString());
            CodeHelpers.SubscribeToUndandledExceptions();
        }

        public void UpdateJournalPath()
        {
            RemoteJournalQueue.RemotePath = GetRemoteJournalPathOrNull();
        }

        public string GetRemoteJournalPathOrNull()
        {
            TryLoadConfig();
            if (Configuration == null)
                return null;
            if (Configuration.JournalLocation.Path == JournalDefaultPath)
                return null;
            else
                return Configuration.JournalLocation.Path;
        }

        public bool TryLoadConfig()
        {
            Configuration = CodeHelpers.TryLoadDataFromXmlFile<Configuration>(_cfgFilePath);

            if (Configuration != null)
            {
                CopyDataFilesFromRemoteToLocalStoreAsync();
                return true;
            }
            return false;
        }

        public bool IsCopyDataFilesFromRemoteToLocalStoreNow => _isCopyDataFilesFromRemoteToLocalStoreNow;
        private bool _isCopyDataFilesFromRemoteToLocalStoreNow = false;
        public void CopyDataFilesFromRemoteToLocalStoreAsync()
        {
            if (!_isCopyDataFilesFromRemoteToLocalStoreNow)
            {
                _isCopyDataFilesFromRemoteToLocalStoreNow = true;
                Task.Factory.StartNew<bool>(() =>
                {
                    //return bool - если получил и обновил новую конфигурацию (кроме журнала). Используется для reload интерфейса
                    var isNewDataLoaded = false;
                    if (Configuration != null)
                    {
                        //Autos
                        try
                        {
                            if (Configuration.MainDataLocation.Path != MainDataDefaultPath &&
                            !IsFilesEquals(Configuration.MainDataLocation.Path, MainDataDefaultPath, true))
                            {
                                File.Copy(Configuration.MainDataLocation.Path, MainDataDefaultPath, true);
                                isNewDataLoaded = true;
                            }

                            Settings.Default.AutoSyncLastDate = DateTime.Now;
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceWarning(ex.ToString());
                        }
                        //Users
                        try
                        {
                            if (Configuration.AuthDataLocation.Path != AuthDataDefaultPath &&
                            !IsFilesEquals(Configuration.AuthDataLocation.Path, AuthDataDefaultPath, true))
                            {
                                File.Copy(Configuration.AuthDataLocation.Path, AuthDataDefaultPath, true);
                                isNewDataLoaded = true;
                            }

                            Settings.Default.UserSyncLastDate = DateTime.Now;
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceWarning(ex.ToString());
                        }

                        //Journal
                        try
                        {
                            //!Если нет очереди записи, то берем с сервера.
                            if (RemoteJournalQueue.Count() == 0)
                            {
                                if (Configuration.JournalLocation.Path != JournalDefaultPath &&
                                !IsFilesEquals(Configuration.JournalLocation.Path, JournalDefaultPath, false))
                                {
                                    File.Copy(Configuration.JournalLocation.Path, JournalDefaultPath, true);
                                }

                                Settings.Default.JournalSyncLastDate = DateTime.Now;
                            }
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceWarning(ex.ToString());
                        }
                    }
                    return isNewDataLoaded;
                }).ContinueWith((taskresult) =>
                {
                    Settings.Default.Save();
                    _isCopyDataFilesFromRemoteToLocalStoreNow = false;
                });
            }
        }

        /// <summary>
        /// Сравнить содержимое файлов
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <exception cref="System.IO.IOException">File not found or other</exception>
        /// <returns></returns>
        public bool IsFilesEquals(string path1, string path2, bool isCheckContent)
        {
            var isLenEquals = new FileInfo(path1).Length == new FileInfo(path2).Length;
            if (isCheckContent)
            {
                return isLenEquals && File.ReadAllBytes(path1).SequenceEqual(File.ReadAllBytes(path2));
            }
            else
                return isLenEquals;
        }

        public bool TrySaveConfig()
        {
            return CodeHelpers.TrySaveDataToXmlFile(Configuration, _cfgFilePath);
        }

        public bool TryLoadAllDataFromFiles()
        {
            Debug.WriteLine(nameof(TryLoadAllDataFromFiles));

            var result = false;

            TryLoadConfig();
            var authDataPath = AuthDataDefaultPath;
            var mainDataPath = MainDataDefaultPath;
            AuthData = CodeHelpers.TryLoadDataFromXmlFile<AuthData>(authDataPath, true);
            MainData = CodeHelpers.TryLoadDataFromXmlFile<MainData>(mainDataPath);

            if (AuthData != null && MainData != null)
                result = true;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>source path</returns>
        public string ReLoadLocalJournalFromSource()
        {
            Debug.WriteLine(nameof(ReLoadLocalJournalFromSource));

            var journalPath = GetRemoteJournalPathOrNull();
            if (string.IsNullOrEmpty(journalPath))
                journalPath = JournalDefaultPath;

            var journalEvents = csvWorker.TryLoadDataFromCsvFile<JournalEventCSV>(journalPath);
            if (journalEvents != null)
            {
                Journal = new Journal();
                foreach (var journalEvent in journalEvents)
                    Journal.Events.Add(JournalEventCSVToEvent(journalEvent));
            }
            return journalPath;
        }

        public bool TryWriteCalculationToDevice()
        {
            Debug.WriteLine(nameof(TryWriteCalculationToDevice));

            var result = false;

            if (IsDeviceConnectedAndValid)
            {
                Trace.TraceInformation("Try write K to usd device: " + CalculatedData.K);
                var writeResult = _canWorker.WriteParemeter16(Can.Internal.TachoParameters.KSpeedU16, CalculatedData.K);

                if (writeResult.HasValue && writeResult == true)
                {
                    writeResult = _canWorker.WriteParemeter16(Can.Internal.TachoParameters.KTransmissionU16, (UInt16)(CalculatedData.N / 0.001));
                }

                if (!writeResult.HasValue || writeResult.Value == false)
                    result = false;
                else
                    result = true;
            }

            return result;
        }

        public bool TryWriteCalculationToUSDDevice() {
            Debug.WriteLine(nameof(TryWriteCalculationToUSDDevice));
            var result = false;
            if (IsUSDDeviceConnectedAndValid) {
                
                var NValueToWrite = (UInt16)(CalculatedData.N / 0.001);

                Trace.TraceInformation("Try write K to device: " + CalculatedData.K);
                Trace.TraceInformation("Try write N to device: " + NValueToWrite);

                //TODO del
                _canWorker.ReadUSDParameterUInt16(Can.Internal.USDParameters.KSpeedU16, (param, resultk) => { Debug.WriteLine(resultk); });
                _canWorker.ReadUSDParameterUInt16(Can.Internal.USDParameters.KTransmissionU16, (param, resultk2) => { Debug.WriteLine(resultk2); });


                var setWriteSession = _canWorker.SetUSDSessionType(Can.Internal.USDSessionType.Extended, null);

                if (!setWriteSession.GetValueOrDefault()) {
                    return false;
                }

                var writeResult = _canWorker.WriteUSDParameter16(Can.Internal.USDParameters.KSpeedU16, CalculatedData.K);

                if (writeResult.HasValue && writeResult == true) {
                    writeResult = _canWorker.WriteUSDParameter16(Can.Internal.USDParameters.KTransmissionU16, NValueToWrite);
                }

                //TODO del
                _canWorker.ReadUSDParameterUInt16(Can.Internal.USDParameters.KSpeedU16, (param, resultk) => { Debug.WriteLine(resultk); });
                _canWorker.ReadUSDParameterUInt16(Can.Internal.USDParameters.KTransmissionU16, (param, resultk2) => { Debug.WriteLine(resultk2); });

                return writeResult.GetValueOrDefault();
            }
            return result;
        }

        public string GetDeviceStatus()
        {
            Debug.WriteLine(nameof(GetDeviceStatus));

            bool isDeviceConnected = false;
            bool isUsdConnected = false;
            var result = "";
            if (_canWorker != null)
            {
                if (_canWorker.IsReady())
                {
                    var tachoStatus = _canWorker.GetTachoStatus();
                    if (tachoStatus == TachoStatusResult.NoCan)
                        result = Resources.strTachoStatusResult0;
                    else if (tachoStatus == TachoStatusResult.NoTacho)
                        result = Resources.strTachoStatusResult1;
                    else
                    {
                        result = Resources.strTachoStatusResult2;
                        isDeviceConnected = true;
                    }

                    isUsdConnected = _canWorker.IsUSDConnected();
                }
                else
                {
                    _canWorker.ConnectToCanInterface();
                    result = _canWorker.GetErrorStatus();
                }
            }

            IsDeviceConnectedAndValid = isDeviceConnected;
            IsUSDDeviceConnectedAndValid = isUsdConnected;
            return result;
        }

        /// <summary>
        /// string / uint16 / uint32
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        internal bool GetDeviceParameter<T>(Can.Internal.TachoParameters param, Action<T> result)
        {
            if (!IsDeviceConnectedAndValid)
                return false;
            bool? isDone = false;
            if (typeof(T) == typeof(string))
                isDone = _canWorker.ReadParameterString(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            else if (typeof(T) == typeof(Int16) ||
                typeof(T) == typeof(UInt16))
                isDone = _canWorker.ReadParameterUInt16(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            else if (typeof(T) == typeof(Int32) ||
                typeof(T) == typeof(UInt32))
                isDone = _canWorker.ReadParameterUInt32(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            return isDone.HasValue && isDone == true;
        }

        internal bool GetUSDDeviceParameter<T>(Can.Internal.USDParameters param, Action<T> result) {
            if(!IsUSDDeviceConnectedAndValid)
                return false;
            bool? isDone = false;
            if (typeof(T) == typeof(string))
                isDone = _canWorker.ReadUSDParameterString(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            else if (typeof(T) == typeof(Int16) ||
                typeof(T) == typeof(UInt16))
                isDone = _canWorker.ReadUSDParameterUInt16(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            else if (typeof(T) == typeof(Int32) ||
                typeof(T) == typeof(UInt32))
                isDone = _canWorker.ReadUSDParameterUInt32(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            else if(typeof(T) == typeof(byte[]))
                isDone = _canWorker.ReadUSDParameter(param, (parameter, value) => result((T)Convert.ChangeType(value, typeof(T))));
            return isDone.HasValue && isDone == true;
        }

        /// <summary>
        /// uint16 / uint32
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        internal bool SetDeviceParameter<T>(Can.Internal.TachoParameters param, T value)
        {
            if (!IsDeviceConnectedAndValid)
                return false;
            bool? isDone = false;
            if (typeof(T) == typeof(Int16) ||
                typeof(T) == typeof(UInt16))
                isDone = _canWorker.WriteParemeter16(param, (UInt16)Convert.ChangeType(value, typeof(UInt16)));
            else if (typeof(T) == typeof(Int32) ||
                typeof(T) == typeof(UInt32))
                isDone = _canWorker.WriteParemeter32(param, (UInt32)Convert.ChangeType(value, typeof(UInt32)));
            return isDone.HasValue && isDone == true;
        }

        private void InitTrace()
        {
            Debug.WriteLine(nameof(InitTrace));

            Trace.AutoFlush = true;
            var logFileName = "Log" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt";
            var logFilePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), logFileName);
            _logFileStream = new FileStream(logFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, 4096, FileOptions.Asynchronous);
            _streamWriter = new StreamWriter(_logFileStream);
            _traceListener = new TextWriterTraceListener(_streamWriter);
            Trace.Listeners.Add(_traceListener);
        }

        public bool TryWriteJournalEntry(NameNumber user, Transmission trans, TireModel tire, string serial, string manuf, string model, string crc, bool isUsdDeviceWritten, string usdDeviceSN)
        {
            Debug.WriteLine(nameof(TryWriteJournalEntry));

            var result = false;

            result = true;

            Event newEvent = new Event
            {
                Date = DateTime.Now.ToString(_journalDateFormat, CultureInfo.InvariantCulture),
                K = CalculatedData.K,
                IsOverride = CalculatedData.IsOverride,
                ChassisNuber = CalculatedData.СhassisNuber,
                UserMachineName = System.Net.Dns.GetHostEntry("").HostName,
                NameNumber = user,
                TireModel = tire,
                Transmission = trans,
                GPM = CalculatedData.GPM,
                SN = serial,
                Manufacturer = manuf,
                Model = model,
                Crc = crc,
                IsOdometerSet = false,
                IsUsdDeviceWritten = isUsdDeviceWritten,
                USDDeviceSN = usdDeviceSN,
            };

            if (CalculatedData.IsOverride)
            {
                newEvent.TireModel = new TireModel(string.Empty, string.Empty, CalculatedData.DR);
                newEvent.Transmission = new Transmission(0, string.Empty, CalculatedData.N);
            }

            Journal.Events.Add(newEvent);

            AddEventToLocalCsv(newEvent);

            RemoteJournalQueue.AddNewIfNeeded(newEvent);
            return result;
        }

        public bool TryWriteJournalEntry(NameNumber user, UInt32 prevOdomenterTotalM, UInt32 prevOdomenterDayM, double odomenterTotalKm, double odomenterDayKm, string serial, UInt16 k, float transmissionN, string manuf, string model, string crc, string shassiNum)
        {
            Debug.WriteLine(nameof(TryWriteJournalEntry));

            var result = false;

            result = true;

            Event newEvent = new Event
            {
                Date = DateTime.Now.ToString(_journalDateFormat, CultureInfo.InvariantCulture),
                UserMachineName = System.Environment.MachineName, //System.Net.Dns.GetHostEntry("").HostName;
                NameNumber = user,
                SN = serial,
                OdometerDay = CodeHelpers.KmToDeviceMeters(odomenterDayKm),
                OdometerTotal = CodeHelpers.KmToDeviceMeters(odomenterTotalKm),
                PrevOdometerDay = prevOdomenterDayM,
                ChassisNuber = shassiNum,
                PrevOdometerTotal = prevOdomenterTotalM,
                IsOdometerSet = true,
                Transmission = new Transmission(0, string.Empty, transmissionN), //TODO ??
                K = k,
                Crc = crc,
                Manufacturer = manuf,
                Model = model,
            };

            Journal.Events.Add(newEvent);

            AddEventToLocalCsv(newEvent);

            RemoteJournalQueue.AddNewIfNeeded(newEvent);
            return result;
        }

        private void AddEventToLocalCsv(Event newEvent)
        {
            Debug.WriteLine(nameof(AddEventToLocalCsv));

            if (newEvent == null)
                Trace.TraceWarning(new ArgumentNullException(nameof(newEvent)).ToString());
            else
                csvWorker.TrySaveDataToCsvFile(new List<JournalEventCSV>() { EventToCSVEvent(newEvent) }, JournalDefaultPath, true);
        }

        internal JournalEventCSV EventToCSVEvent(Event newEvent)
        {
            var journalEventCsv = new JournalEventCSV();
            if (newEvent == null)
            {
                Trace.TraceWarning(new ArgumentNullException(nameof(newEvent)).ToString());
                return journalEventCsv;
            }

            journalEventCsv.Date = newEvent.Date;
            journalEventCsv.UserMachineName = newEvent.UserMachineName;
            journalEventCsv.OdometerTotal = CodeHelpers.DeviceMetersToKm(newEvent.OdometerTotal);
            journalEventCsv.OdometerDay = CodeHelpers.DeviceMetersToKm(newEvent.OdometerDay);
            journalEventCsv.PrevOdometerTotal = newEvent.PrevOdometerTotal;
            journalEventCsv.PrevOdometerDay = newEvent.PrevOdometerDay;
            journalEventCsv.ChassisNuber = newEvent.ChassisNuber;
            if (newEvent.NameNumber != null)
            {
                journalEventCsv.UserName = newEvent.NameNumber.Name;
                journalEventCsv.UserNumber = newEvent.NameNumber.Number;
            }
            journalEventCsv.IsOverride = newEvent.IsOverride;
            if (newEvent.TireModel != null)
            {
                journalEventCsv.TireSize = newEvent.TireModel.Size;
                journalEventCsv.TireName = newEvent.TireModel.Name;
                journalEventCsv.TireDR = newEvent.TireModel.DR;
            }
            if (newEvent.Transmission != null)
            {
                journalEventCsv.TransmissionType = newEvent.Transmission.Type;
                journalEventCsv.TransmissionName = newEvent.Transmission.Name;
                journalEventCsv.TransmissionN = newEvent.Transmission.N;
            }
            journalEventCsv.K = newEvent.K;
            journalEventCsv.GPM = newEvent.GPM;
            journalEventCsv.SN = newEvent.SN;
            journalEventCsv.Manufacturer = newEvent.Manufacturer;
            journalEventCsv.Model = newEvent.Model;
            journalEventCsv.Crc = newEvent.Crc;
            journalEventCsv.IsOdometerSet = newEvent.IsOdometerSet;
            journalEventCsv.IsUsdDeviceWritten = newEvent.IsUsdDeviceWritten;
            journalEventCsv.USDDeviceSN = newEvent.USDDeviceSN;
            return journalEventCsv;
        }

        internal Event JournalEventCSVToEvent(JournalEventCSV j)
        {
            if (j == null)
            {
                Trace.TraceWarning(new ArgumentNullException(nameof(j)).ToString());
                return null;
            }
            var newEvent = new Event {
                UserMachineName = j.UserMachineName,
                OdometerTotal = CodeHelpers.KmToDeviceMeters(j.OdometerTotal),
                OdometerDay = CodeHelpers.KmToDeviceMeters(j.OdometerDay),
                PrevOdometerTotal = CodeHelpers.KmToDeviceMeters(j.PrevOdometerTotal),
                PrevOdometerDay = CodeHelpers.KmToDeviceMeters(j.PrevOdometerDay),
                Date = j.Date,
                ChassisNuber = j.ChassisNuber,
                NameNumber = new NameNumber(j.UserName, j.UserNumber),
                IsOverride = j.IsOverride,
                TireModel = new TireModel(j.TireSize, j.TireName, j.TireDR),
                Transmission = new Transmission(j.TransmissionType, j.TransmissionName, j.TransmissionN),
                K = j.K,
                GPM = j.GPM,
                Manufacturer = j.Manufacturer,
                Crc = j.Crc,
                Model = j.Model,
                IsOdometerSet = j.IsOdometerSet,
                SN = j.SN,
                IsUsdDeviceWritten = j.IsUsdDeviceWritten,
                USDDeviceSN = j.USDDeviceSN,
            };
            return newEvent;
        }

        public bool TryParseCSVDate(string csvDate, out DateTime outDate)
        {
            outDate = DateTime.Now;
            return DateTime.TryParseExact(csvDate, _journalDateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out outDate);
        }

        public void Dispose()
        {
            if (_canWorker != null)
                _canWorker.Dispose();

            Trace.Listeners.Remove(_traceListener);
            if (_traceListener != null)
            {
                _traceListener.Flush();
                _traceListener.Dispose();
            }
            if (_streamWriter != null)
            {
                _streamWriter.Dispose();
            }
            if (_logFileStream != null)
                _logFileStream.Dispose();
        }
    }
}
