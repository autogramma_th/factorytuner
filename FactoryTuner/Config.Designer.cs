﻿namespace FactoryTuner
{
    partial class Config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config));
            this.bnSave = new System.Windows.Forms.Button();
            this.tbData = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.tbJournal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bnCancel = new System.Windows.Forms.Button();
            this.bnSetDataPath = new System.Windows.Forms.Button();
            this.bnSetUsersPath = new System.Windows.Forms.Button();
            this.bnSetJournalPath = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // bnSave
            // 
            this.bnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSave.Location = new System.Drawing.Point(756, 170);
            this.bnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bnSave.Name = "bnSave";
            this.bnSave.Size = new System.Drawing.Size(147, 44);
            this.bnSave.TabIndex = 0;
            this.bnSave.Text = "Сохранить";
            this.bnSave.UseVisualStyleBackColor = true;
            this.bnSave.Click += new System.EventHandler(this.bnSave_Click);
            // 
            // tbData
            // 
            this.tbData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbData.Location = new System.Drawing.Point(298, 19);
            this.tbData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(603, 30);
            this.tbData.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 36);
            this.label1.TabIndex = 2;
            this.label1.Text = "Состав автомобиля:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(18, 59);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 36);
            this.label2.TabIndex = 3;
            this.label2.Text = "Список пользователей:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbUser
            // 
            this.tbUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUser.Location = new System.Drawing.Point(298, 61);
            this.tbUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(603, 30);
            this.tbUser.TabIndex = 4;
            // 
            // tbJournal
            // 
            this.tbJournal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbJournal.Location = new System.Drawing.Point(298, 105);
            this.tbJournal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbJournal.Name = "tbJournal";
            this.tbJournal.Size = new System.Drawing.Size(603, 30);
            this.tbJournal.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(18, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(272, 36);
            this.label3.TabIndex = 5;
            this.label3.Text = "Журнал:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bnCancel
            // 
            this.bnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bnCancel.Location = new System.Drawing.Point(912, 170);
            this.bnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(112, 44);
            this.bnCancel.TabIndex = 7;
            this.bnCancel.Text = "Выход";
            this.bnCancel.UseVisualStyleBackColor = true;
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // bnSetDataPath
            // 
            this.bnSetDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSetDataPath.Location = new System.Drawing.Point(912, 14);
            this.bnSetDataPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bnSetDataPath.Name = "bnSetDataPath";
            this.bnSetDataPath.Size = new System.Drawing.Size(112, 44);
            this.bnSetDataPath.TabIndex = 8;
            this.bnSetDataPath.Text = "Обзор";
            this.bnSetDataPath.UseVisualStyleBackColor = true;
            this.bnSetDataPath.Click += new System.EventHandler(this.bnSetDataPath_Click);
            // 
            // bnSetUsersPath
            // 
            this.bnSetUsersPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSetUsersPath.Location = new System.Drawing.Point(912, 56);
            this.bnSetUsersPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bnSetUsersPath.Name = "bnSetUsersPath";
            this.bnSetUsersPath.Size = new System.Drawing.Size(112, 44);
            this.bnSetUsersPath.TabIndex = 9;
            this.bnSetUsersPath.Text = "Обзор";
            this.bnSetUsersPath.UseVisualStyleBackColor = true;
            this.bnSetUsersPath.Click += new System.EventHandler(this.bnSetUsersPath_Click);
            // 
            // bnSetJournalPath
            // 
            this.bnSetJournalPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bnSetJournalPath.Location = new System.Drawing.Point(912, 98);
            this.bnSetJournalPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bnSetJournalPath.Name = "bnSetJournalPath";
            this.bnSetJournalPath.Size = new System.Drawing.Size(112, 44);
            this.bnSetJournalPath.TabIndex = 10;
            this.bnSetJournalPath.Text = "Обзор";
            this.bnSetJournalPath.UseVisualStyleBackColor = true;
            this.bnSetJournalPath.Click += new System.EventHandler(this.bnSetJournalPath_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 233);
            this.Controls.Add(this.bnSetJournalPath);
            this.Controls.Add(this.bnSetUsersPath);
            this.Controls.Add(this.bnSetDataPath);
            this.Controls.Add(this.bnCancel);
            this.Controls.Add(this.tbJournal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.bnSave);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1060, 224);
            this.Name = "Config";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка источников";
            this.Load += new System.EventHandler(this.Config_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bnSave;
        private System.Windows.Forms.TextBox tbData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbUser;
        private System.Windows.Forms.TextBox tbJournal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bnCancel;
        private System.Windows.Forms.Button bnSetDataPath;
        private System.Windows.Forms.Button bnSetUsersPath;
        private System.Windows.Forms.Button bnSetJournalPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}