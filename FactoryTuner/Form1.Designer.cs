﻿namespace FactoryTuner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lbFontSize = new System.Windows.Forms.Label();
            this.bnFontPlus = new System.Windows.Forms.Button();
            this.bnFontMinus = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cbTireModel = new System.Windows.Forms.ComboBox();
            this.cbTireMarking = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbChassis = new System.Windows.Forms.TextBox();
            this.cbMechanic = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbMTB = new System.Windows.Forms.ComboBox();
            this.gbSensorLocation = new System.Windows.Forms.GroupBox();
            this.rbTC = new System.Windows.Forms.RadioButton();
            this.rbAuto = new System.Windows.Forms.RadioButton();
            this.rbManual = new System.Windows.Forms.RadioButton();
            this.cbTKs = new System.Windows.Forms.ComboBox();
            this.cbAutos = new System.Windows.Forms.ComboBox();
            this.cbManuals = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDemo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bnOpenJournalBySN = new System.Windows.Forms.Button();
            this.dgDeviceStatus = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbPathJournal = new System.Windows.Forms.Label();
            this.lbPathData = new System.Windows.Forms.Label();
            this.lbPathAuth = new System.Windows.Forms.Label();
            this.lbJournalCaption = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bnReadParamsFromTacho = new System.Windows.Forms.Button();
            this.bnSettings = new System.Windows.Forms.Button();
            this.gbConstants = new System.Windows.Forms.GroupBox();
            this.tbK = new System.Windows.Forms.TextBox();
            this.tbDynRadius = new System.Windows.Forms.TextBox();
            this.tbPulses = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bnWrite = new System.Windows.Forms.Button();
            this.bnClear = new System.Windows.Forms.Button();
            this.bnJournal = new System.Windows.Forms.Button();
            this.cbManualInput = new System.Windows.Forms.CheckBox();
            this.bnExit = new System.Windows.Forms.Button();
            this.checkTimer = new System.Windows.Forms.Timer(this.components);
            this.timerSaveToRemoteJournal = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbSensorLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceStatus)).BeginInit();
            this.gbConstants.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lbFontSize);
            this.splitContainer1.Panel1.Controls.Add(this.bnFontPlus);
            this.splitContainer1.Panel1.Controls.Add(this.bnFontMinus);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.cbMTB);
            this.splitContainer1.Panel1.Controls.Add(this.gbSensorLocation);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cbDemo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.bnOpenJournalBySN);
            this.splitContainer1.Panel2.Controls.Add(this.dgDeviceStatus);
            this.splitContainer1.Panel2.Controls.Add(this.lbPathJournal);
            this.splitContainer1.Panel2.Controls.Add(this.lbPathData);
            this.splitContainer1.Panel2.Controls.Add(this.lbPathAuth);
            this.splitContainer1.Panel2.Controls.Add(this.lbJournalCaption);
            this.splitContainer1.Panel2.Controls.Add(this.label13);
            this.splitContainer1.Panel2.Controls.Add(this.label11);
            this.splitContainer1.Panel2.Controls.Add(this.bnReadParamsFromTacho);
            this.splitContainer1.Panel2.Controls.Add(this.bnSettings);
            this.splitContainer1.Panel2.Controls.Add(this.gbConstants);
            this.splitContainer1.Panel2.Controls.Add(this.bnWrite);
            this.splitContainer1.Panel2.Controls.Add(this.bnClear);
            this.splitContainer1.Panel2.Controls.Add(this.bnJournal);
            this.splitContainer1.Panel2.Controls.Add(this.cbManualInput);
            this.splitContainer1.Panel2.Controls.Add(this.bnExit);
            this.splitContainer1.TabStop = false;
            // 
            // lbFontSize
            // 
            resources.ApplyResources(this.lbFontSize, "lbFontSize");
            this.lbFontSize.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lbFontSize.Name = "lbFontSize";
            // 
            // bnFontPlus
            // 
            resources.ApplyResources(this.bnFontPlus, "bnFontPlus");
            this.bnFontPlus.Name = "bnFontPlus";
            this.bnFontPlus.UseVisualStyleBackColor = true;
            this.bnFontPlus.Click += new System.EventHandler(this.bnFontPlus_Click);
            // 
            // bnFontMinus
            // 
            resources.ApplyResources(this.bnFontMinus, "bnFontMinus");
            this.bnFontMinus.Name = "bnFontMinus";
            this.bnFontMinus.UseVisualStyleBackColor = true;
            this.bnFontMinus.Click += new System.EventHandler(this.bnFontMinus_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.cbTireModel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbTireMarking, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // cbTireModel
            // 
            resources.ApplyResources(this.cbTireModel, "cbTireModel");
            this.cbTireModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTireModel.FormattingEnabled = true;
            this.cbTireModel.Name = "cbTireModel";
            this.cbTireModel.SelectedIndexChanged += new System.EventHandler(this.cbTireModel_SelectedIndexChanged);
            // 
            // cbTireMarking
            // 
            resources.ApplyResources(this.cbTireMarking, "cbTireMarking");
            this.cbTireMarking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTireMarking.FormattingEnabled = true;
            this.cbTireMarking.Name = "cbTireMarking";
            this.cbTireMarking.SelectedIndexChanged += new System.EventHandler(this.cbTireMarking_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.tbChassis);
            this.groupBox2.Controls.Add(this.cbMechanic);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // tbChassis
            // 
            resources.ApplyResources(this.tbChassis, "tbChassis");
            this.tbChassis.Name = "tbChassis";
            this.tbChassis.TextChanged += new System.EventHandler(this.tbChassis_TextChanged);
            // 
            // cbMechanic
            // 
            resources.ApplyResources(this.cbMechanic, "cbMechanic");
            this.cbMechanic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMechanic.FormattingEnabled = true;
            this.cbMechanic.Name = "cbMechanic";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // cbMTB
            // 
            resources.ApplyResources(this.cbMTB, "cbMTB");
            this.cbMTB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMTB.FormattingEnabled = true;
            this.cbMTB.Name = "cbMTB";
            this.cbMTB.SelectedIndexChanged += new System.EventHandler(this.cbMTB_SelectedIndexChanged);
            // 
            // gbSensorLocation
            // 
            resources.ApplyResources(this.gbSensorLocation, "gbSensorLocation");
            this.gbSensorLocation.Controls.Add(this.rbTC);
            this.gbSensorLocation.Controls.Add(this.rbAuto);
            this.gbSensorLocation.Controls.Add(this.rbManual);
            this.gbSensorLocation.Controls.Add(this.cbTKs);
            this.gbSensorLocation.Controls.Add(this.cbAutos);
            this.gbSensorLocation.Controls.Add(this.cbManuals);
            this.gbSensorLocation.Name = "gbSensorLocation";
            this.gbSensorLocation.TabStop = false;
            // 
            // rbTC
            // 
            resources.ApplyResources(this.rbTC, "rbTC");
            this.rbTC.Name = "rbTC";
            this.rbTC.TabStop = true;
            this.rbTC.UseVisualStyleBackColor = true;
            this.rbTC.CheckedChanged += new System.EventHandler(this.rbTC_CheckedChanged);
            // 
            // rbAuto
            // 
            resources.ApplyResources(this.rbAuto, "rbAuto");
            this.rbAuto.Name = "rbAuto";
            this.rbAuto.TabStop = true;
            this.rbAuto.UseVisualStyleBackColor = true;
            this.rbAuto.CheckedChanged += new System.EventHandler(this.rbAuto_CheckedChanged);
            // 
            // rbManual
            // 
            resources.ApplyResources(this.rbManual, "rbManual");
            this.rbManual.Checked = true;
            this.rbManual.Name = "rbManual";
            this.rbManual.TabStop = true;
            this.rbManual.UseVisualStyleBackColor = true;
            this.rbManual.CheckedChanged += new System.EventHandler(this.rbManual_CheckedChanged);
            // 
            // cbTKs
            // 
            resources.ApplyResources(this.cbTKs, "cbTKs");
            this.cbTKs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTKs.FormattingEnabled = true;
            this.cbTKs.Name = "cbTKs";
            this.cbTKs.SelectedIndexChanged += new System.EventHandler(this.Transmission_SelectedIndexChanged);
            // 
            // cbAutos
            // 
            resources.ApplyResources(this.cbAutos, "cbAutos");
            this.cbAutos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAutos.FormattingEnabled = true;
            this.cbAutos.Name = "cbAutos";
            this.cbAutos.SelectedIndexChanged += new System.EventHandler(this.Transmission_SelectedIndexChanged);
            // 
            // cbManuals
            // 
            resources.ApplyResources(this.cbManuals, "cbManuals");
            this.cbManuals.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbManuals.FormattingEnabled = true;
            this.cbManuals.Name = "cbManuals";
            this.cbManuals.SelectedIndexChanged += new System.EventHandler(this.Transmission_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cbDemo
            // 
            resources.ApplyResources(this.cbDemo, "cbDemo");
            this.cbDemo.Name = "cbDemo";
            this.cbDemo.UseVisualStyleBackColor = true;
            this.cbDemo.CheckedChanged += new System.EventHandler(this.cbDemo_CheckedChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // bnOpenJournalBySN
            // 
            resources.ApplyResources(this.bnOpenJournalBySN, "bnOpenJournalBySN");
            this.bnOpenJournalBySN.Name = "bnOpenJournalBySN";
            this.bnOpenJournalBySN.UseVisualStyleBackColor = true;
            this.bnOpenJournalBySN.Click += new System.EventHandler(this.bnOpenJournalBySN_Click);
            // 
            // dgDeviceStatus
            // 
            this.dgDeviceStatus.AllowUserToAddRows = false;
            this.dgDeviceStatus.AllowUserToDeleteRows = false;
            this.dgDeviceStatus.AllowUserToResizeColumns = false;
            this.dgDeviceStatus.AllowUserToResizeRows = false;
            resources.ApplyResources(this.dgDeviceStatus, "dgDeviceStatus");
            this.dgDeviceStatus.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgDeviceStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDeviceStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDeviceStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDeviceStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDeviceStatus.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgDeviceStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgDeviceStatus.EnableHeadersVisualStyles = false;
            this.dgDeviceStatus.Name = "dgDeviceStatus";
            this.dgDeviceStatus.ReadOnly = true;
            this.dgDeviceStatus.RowHeadersVisible = false;
            this.dgDeviceStatus.RowTemplate.Height = 24;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            resources.ApplyResources(this.Column1, "Column1");
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.Column2, "Column2");
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.Column3, "Column3");
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // lbPathJournal
            // 
            resources.ApplyResources(this.lbPathJournal, "lbPathJournal");
            this.lbPathJournal.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbPathJournal.Name = "lbPathJournal";
            // 
            // lbPathData
            // 
            resources.ApplyResources(this.lbPathData, "lbPathData");
            this.lbPathData.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbPathData.Name = "lbPathData";
            // 
            // lbPathAuth
            // 
            resources.ApplyResources(this.lbPathAuth, "lbPathAuth");
            this.lbPathAuth.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbPathAuth.Name = "lbPathAuth";
            // 
            // lbJournalCaption
            // 
            resources.ApplyResources(this.lbJournalCaption, "lbJournalCaption");
            this.lbJournalCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbJournalCaption.Name = "lbJournalCaption";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Name = "label13";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Name = "label11";
            // 
            // bnReadParamsFromTacho
            // 
            resources.ApplyResources(this.bnReadParamsFromTacho, "bnReadParamsFromTacho");
            this.bnReadParamsFromTacho.Name = "bnReadParamsFromTacho";
            this.bnReadParamsFromTacho.UseVisualStyleBackColor = true;
            this.bnReadParamsFromTacho.Click += new System.EventHandler(this.bnReadParamsFromTacho_Click);
            // 
            // bnSettings
            // 
            resources.ApplyResources(this.bnSettings, "bnSettings");
            this.bnSettings.Name = "bnSettings";
            this.bnSettings.UseVisualStyleBackColor = true;
            this.bnSettings.Click += new System.EventHandler(this.bnSettings_Click);
            // 
            // gbConstants
            // 
            resources.ApplyResources(this.gbConstants, "gbConstants");
            this.gbConstants.Controls.Add(this.tbK);
            this.gbConstants.Controls.Add(this.tbDynRadius);
            this.gbConstants.Controls.Add(this.tbPulses);
            this.gbConstants.Controls.Add(this.label7);
            this.gbConstants.Controls.Add(this.label5);
            this.gbConstants.Controls.Add(this.label6);
            this.gbConstants.Name = "gbConstants";
            this.gbConstants.TabStop = false;
            // 
            // tbK
            // 
            resources.ApplyResources(this.tbK, "tbK");
            this.tbK.Name = "tbK";
            // 
            // tbDynRadius
            // 
            resources.ApplyResources(this.tbDynRadius, "tbDynRadius");
            this.tbDynRadius.Name = "tbDynRadius";
            this.tbDynRadius.TextChanged += new System.EventHandler(this.tbDynRadius_TextChanged);
            // 
            // tbPulses
            // 
            resources.ApplyResources(this.tbPulses, "tbPulses");
            this.tbPulses.Name = "tbPulses";
            this.tbPulses.TextChanged += new System.EventHandler(this.tbPulses_TextChanged);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // bnWrite
            // 
            resources.ApplyResources(this.bnWrite, "bnWrite");
            this.bnWrite.Name = "bnWrite";
            this.bnWrite.UseVisualStyleBackColor = true;
            this.bnWrite.Click += new System.EventHandler(this.bnWrite_Click);
            // 
            // bnClear
            // 
            resources.ApplyResources(this.bnClear, "bnClear");
            this.bnClear.Name = "bnClear";
            this.bnClear.UseVisualStyleBackColor = true;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // bnJournal
            // 
            resources.ApplyResources(this.bnJournal, "bnJournal");
            this.bnJournal.Name = "bnJournal";
            this.bnJournal.UseVisualStyleBackColor = true;
            this.bnJournal.Click += new System.EventHandler(this.bnJournal_Click);
            // 
            // cbManualInput
            // 
            resources.ApplyResources(this.cbManualInput, "cbManualInput");
            this.cbManualInput.Name = "cbManualInput";
            this.cbManualInput.UseVisualStyleBackColor = true;
            this.cbManualInput.CheckedChanged += new System.EventHandler(this.cbManualInput_CheckedChanged);
            // 
            // bnExit
            // 
            resources.ApplyResources(this.bnExit, "bnExit");
            this.bnExit.Name = "bnExit";
            this.bnExit.UseVisualStyleBackColor = true;
            this.bnExit.Click += new System.EventHandler(this.bnExit_Click);
            // 
            // checkTimer
            // 
            this.checkTimer.Enabled = true;
            this.checkTimer.Interval = 1000;
            this.checkTimer.Tick += new System.EventHandler(this.checkTimer_Tick);
            // 
            // timerSaveToRemoteJournal
            // 
            this.timerSaveToRemoteJournal.Enabled = true;
            this.timerSaveToRemoteJournal.Interval = 5000;
            this.timerSaveToRemoteJournal.Tick += new System.EventHandler(this.timerSaveToRemoteJournal_Tick);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbSensorLocation.ResumeLayout(false);
            this.gbSensorLocation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceStatus)).EndInit();
            this.gbConstants.ResumeLayout(false);
            this.gbConstants.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bnExit;
        private System.Windows.Forms.Button bnWrite;
        private System.Windows.Forms.Button bnClear;
        private System.Windows.Forms.Button bnJournal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbMechanic;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbSensorLocation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbConstants;
        private System.Windows.Forms.Timer checkTimer;
        private System.Windows.Forms.Label lbPathJournal;
        private System.Windows.Forms.Label lbPathAuth;
        private System.Windows.Forms.Label lbPathData;
        private System.Windows.Forms.Timer timerSaveToRemoteJournal;
        private System.Windows.Forms.Button bnSettings;
        private System.Windows.Forms.Button bnFontPlus;
        private System.Windows.Forms.Button bnFontMinus;
        private System.Windows.Forms.Label lbFontSize;
        private System.Windows.Forms.CheckBox cbDemo;
        private System.Windows.Forms.Button bnReadParamsFromTacho;
        private System.Windows.Forms.Label lbJournalCaption;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgDeviceStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button bnOpenJournalBySN;
        public System.Windows.Forms.ComboBox cbMTB;
        public System.Windows.Forms.TextBox tbChassis;
        public System.Windows.Forms.ComboBox cbTireMarking;
        public System.Windows.Forms.ComboBox cbTireModel;
        public System.Windows.Forms.ComboBox cbTKs;
        public System.Windows.Forms.ComboBox cbAutos;
        public System.Windows.Forms.ComboBox cbManuals;
        public System.Windows.Forms.RadioButton rbTC;
        public System.Windows.Forms.RadioButton rbAuto;
        public System.Windows.Forms.RadioButton rbManual;
        public System.Windows.Forms.TextBox tbK;
        public System.Windows.Forms.TextBox tbPulses;
        public System.Windows.Forms.TextBox tbDynRadius;
        public System.Windows.Forms.CheckBox cbManualInput;
    }
}

