﻿using FactoryDataConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace FactoryTuner
{
    public partial class Config : Form
    {
        public static Config Current;

        public Config()
        {
            Current = this;
            InitializeComponent();
        }

        private void Config_Load(object sender, EventArgs e)
        {
            if (CodeHelpers.IsScreenIsOld())
                this.Location = new Point(0, 0);
            this.Text = this.Text + " " + CodeHelpers.GetAppVersionString();

            if (A.I.TryLoadConfig())
            {
                tbData.Text = A.I.Configuration.MainDataLocation.Path;
                tbUser.Text = A.I.Configuration.AuthDataLocation.Path;
                tbJournal.Text = A.I.Configuration.JournalLocation.Path;
            }
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            A.I.Configuration.MainDataLocation.Path = tbData.Text;
            A.I.Configuration.AuthDataLocation.Path = tbUser.Text;
            A.I.Configuration.JournalLocation.Path = tbJournal.Text;
            if (A.I.TrySaveConfig())
            {
                MessageBox.Show(Current, "Saved");
            }
            else
            {
                MessageBox.Show(Current, "Not saved. See log file");
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnSetJournalPath_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(Path.GetFullPath(tbJournal.Text));
            openFileDialog1.FileName = Path.GetFileName(tbJournal.Text);
            var diagResult = openFileDialog1.ShowDialog();
            if (diagResult == DialogResult.OK || diagResult == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(openFileDialog1.FileName))
                    tbJournal.Text = openFileDialog1.FileName;
            }
        }

        private void bnSetUsersPath_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(Path.GetFullPath(tbUser.Text));
            openFileDialog1.FileName = Path.GetFileName(tbUser.Text);
            var diagResult = openFileDialog1.ShowDialog();
            if (diagResult == DialogResult.OK || diagResult == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(openFileDialog1.FileName))
                    tbUser.Text = openFileDialog1.FileName;
            }
        }

        private void bnSetDataPath_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(Path.GetFullPath(tbData.Text));
            openFileDialog1.FileName = Path.GetFileName(tbData.Text);
            var diagResult = openFileDialog1.ShowDialog();
            if (diagResult == DialogResult.OK || diagResult == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(openFileDialog1.FileName))
                    tbData.Text = openFileDialog1.FileName;
            }
        }
    }
}
