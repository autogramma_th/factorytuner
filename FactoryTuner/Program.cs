﻿using FactoryDataConfig;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace FactoryTuner
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        static Mutex mutex = new Mutex(false, SharedConstants.FactoryTunerGuid.ToString());
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SetProcessDPIAware();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                if (args[0] == "d")
                {
                    CodeHelpers.DeleteSelfExeAndShortcut(SharedConstants.FactoryTunerGuid, SharedConstants.FactoryTunerShortkutName);
                    var lib1Path = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Usbcan32.dll");
                    if (File.Exists(lib1Path))
                        CodeHelpers.TryDeleteFileAsAdmin(lib1Path);
                    Application.Exit();
                }
                else if (args[0] == "c")
                    RunForm(() => Application.Run(new Config()));
                else
                    RunForm(() => Application.Run(new Form1()));
            }
            else
                RunForm(() => Application.Run(new Form1()));
        }

        static void RunForm(Action createFormAction)
        {
            // if you like to wait a few seconds in case that the instance is just 
            // shutting down
            if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
            {
                CodeHelpers.SwitchToAlreadyOpenedApp();
                return;
            }

            try
            {
                createFormAction();
            }
            finally { mutex.ReleaseMutex(); } // I find this more explicit
        }
    }
}
