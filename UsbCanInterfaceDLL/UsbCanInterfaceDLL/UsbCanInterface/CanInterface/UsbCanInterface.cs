﻿using System;
using System.Text;
using Asap1B.UsbCan;
using System.Collections.Generic;

namespace UsbCan_Interface
{
    public class UsbCanInterface : AbstractUsbInterface
    {
        /// <summary>
        ///     Возвращаемый код операций
        /// </summary>
        private UsbCanReturn _returnCode;

        /// <summary>
        ///     Сервер по отправке сообщений по CAN
        /// </summary>
        private readonly UsbCanServer _usbCanServer;

        private readonly UsbCanChannel _usedChannel;

        public UsbCanInterface(BitRate bitRate = BitRate.b_500) : base(bitRate)
        {
            _usedChannel = UsbCanChannel.Ch0;
            _usbCanServer = new UsbCanServer();
            _returnCode = _usbCanServer.InitHardware(0);
            _interfaceType = CanUsbInterfaceType.Can;
        }

        public override void Init(BitRate bitRate)
        {
            _bitRate = bitRate;
            Init();
        }

        public override void Init()
        {
            _returnCode = _usbCanServer.InitCan(_usedChannel,
                GetBaudRate(),
                GetUsbCanBaudrateEx(),
                0x0,
                0xffffffff,
                tUcanMode.kUcanModeNormal,
                eUcanOutputControl.USBCAN_OCR_DEFAULT);
            _usbCanServer.CanMsgReceivedEvent += RecieveEvent;

            SetFilterConfig(FilterConfigType.All);
        }

        public override void DeInit()
        {
            _usbCanServer.CanMsgReceivedEvent -= RecieveEvent;
            var res = _usbCanServer.Shutdown(_usedChannel, true);
            _usbCanServer.Dispose();
        }

        public override bool IsReady()
        {
            if (_usbCanServer == null)
                return false;

            if (_usedChannel.Equals(UsbCanChannel.Ch0))
                return _usbCanServer.IsCan0Initialized;
            if (_usedChannel.Equals(UsbCanChannel.Ch1))
                return _usbCanServer.IsCan1Initialized;
            return false;
        }

        public override bool SetBitRate(BitRate bitRate)
        {
            _bitRate = bitRate;
            var ret = _usbCanServer.SetBaudrate(_usedChannel, GetBaudRate(), GetUsbCanBaudrateEx());
            return IsSuccessful(ret);
        }

        private UsbCanBaudrate GetBaudRate()
        {
            switch (_bitRate)
            {
                case BitRate.b_1000:
                    return UsbCanBaudrate.Baud1MBit;
                case BitRate.b_125:
                    return UsbCanBaudrate.Baud125KBit;
                case BitRate.b_250:
                    return UsbCanBaudrate.Baud250KBit;
                case BitRate.b_500:
                    return UsbCanBaudrate.Baud500KBit;
            }
            throw new Exception();
        }

        private UsbCanBaudrateEx GetUsbCanBaudrateEx()
        {
            switch (_bitRate)
            {
                case BitRate.b_1000:
                    return UsbCanBaudrateEx.Baudex1MBit;
                case BitRate.b_125:
                    return UsbCanBaudrateEx.Baudex125KBit;
                case BitRate.b_250:
                    return UsbCanBaudrateEx.Baudex250KBit;
                case BitRate.b_500:
                    return UsbCanBaudrateEx.Baudex500KBit;
            }
            throw new Exception();
        }

        public override bool SendMessage(object message, bool waitAnswer, uint waitId, out object recivedCode, int timeoutMs = -1)
        {
            return SendMessage(message, waitAnswer, new uint[] { waitId }, out recivedCode, timeoutMs);
        }

        public override bool SendMessage(object message, bool waitAnswer, uint[] waitId, out object recivedCode, int timeoutMs = -1)
        {
            lock (locker)
            { //блокируем, что бы во время посылки нельзя было принимать сообщения            
                var mess = new CanMessage[] { (CanMessage)message };

                uint count;
                recivedCode = _usbCanServer.WriteCanMsg(_usedChannel, mess, out count);

                if (waitAnswer)
                {
                    _wait = true;
                    _waitIds = waitId;
                }
            }
            if (waitAnswer)
            {
                var timeout = timeoutMs > 0 ? timeoutMs : _defaultTimeoutMs;
                return WaitAction(timeout);
            }
            return recivedCode.Equals(UsbCanReturn.Successful);
        }

        public override object SendReadMessage(object message)
        {
            var mess = new CanMessage[] { (CanMessage)message };
            uint count;
            return _usbCanServer.WriteCanMsg(_usedChannel, mess, out count);
        }

        private void RecieveEvent(byte bDeviceNrP, UsbCanChannel bChannelP)
        {
            lock (locker)
            { //блокируем, что бы во время приёма нельзя было посылать сообщения и наоборот
                var canMessages = new CanMessage[10];

                uint count;
                _usbCanServer.ReadCanMsg(_usedChannel, canMessages, out count);

                _recievedData = canMessages[0].Data[1];

                //Проверяем все сообщения
                for (int i = 0; i < count; i++)
                {
                    if (OnReceiveCanMessage != null)
                        OnReceiveCanMessage.Invoke(canMessages[i].CanID, canMessages[i].Data);

                    //ожидаем ли полученное сообщение
                    if (IsExpectedMessage(canMessages[i].CanID))
                    {
                        _wait = false; //если да, то завершаем ожидание
                        break;
                    }
                }
            }
        }

        public override bool IsSuccessful(object code)
        {
            var c = (UsbCanReturn)code;
            return c == UsbCanReturn.Successful;
        }

        public override string GetReturnMessage(string mes, object code, bool answerWasRecieved)
        {
            var c = (UsbCanReturn)code;
            var sb = new StringBuilder();
            sb.Append(mes);

            if (answerWasRecieved)
            {
                sb.Append(c == UsbCanReturn.Successful ? " was successful " : " fail with code: " + code);
            }
            else
            {
                sb.Append("Время ожидания истекло");
            }
            return sb.ToString();
        }

        protected override uint ComputeFilterMask(bool ext, uint fromId, uint toId)
        {
            bool rtrToo = false;
            bool rtrOnly = false;
            byte offset = 0;
            uint c1 = 0;
            uint c2 = 0;
            if (ext)
            {
                offset = 3;
                c1 = 0x000004;
                c2 = 0x00003;
            }
            else
            {
                offset = 21;
                c1 = 0x100000;
                c2 = 0xfffff;
            }
            uint amr = ((((uint)(fromId) ^ (toId)) << offset) | ((rtrToo & !rtrOnly) ? c1 : 0) | c2);
            return amr;
        }

        protected override uint ComputeFilterCode(bool ext, uint fromId, uint toId)
        {
            bool rtrOnly = false;
            byte offset = 0;
            uint c1 = 0;
            if (ext)
            {
                offset = 3;
                c1 = 0x000004;
            }
            else
            {
                offset = 21;
                c1 = 0x100000;
            }
            uint acr = ((((uint)(fromId) & (toId)) << offset) | ((rtrOnly) ? c1 : 0));
            return acr;
        }

        protected override bool SetFilterConfig(FilterConfig filterConfig)
        {
            if (!IsReady())
                return false;

            var ret = _usbCanServer.SetAcceptance(_usedChannel, filterConfig.Mask, filterConfig.Code); //устанавливаем маску и код из конфига            
            return ret == UsbCanReturn.Successful;
        }

        protected override object GetCanMessage(uint id, byte[] data, CanMessageFormat msgFormat)
        {
            CanMessage canMessage = new CanMessage();
            canMessage.TimeStamp = 0;
            canMessage.CanID = id;
            canMessage.DataLengthCode = Convert.ToByte(data.Length);
            canMessage.FrameFormat = msgFormat == CanMessageFormat.Ext ? UsbCanMsgFrameFormat.Ext : UsbCanMsgFrameFormat.Std;
            canMessage.Data = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
                canMessage.Data[i] = data[i];

            return canMessage;
        }
    }
}