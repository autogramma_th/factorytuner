﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using Ixxat;
using System.Threading;
using System.Collections;

namespace UsbCan_Interface
{
    public class UsbCanVciInterface : AbstractUsbInterface
    {
        public UsbCanVciInterface(BitRate bitRate = BitRate.b_500) : base(bitRate)
        {
            _interfaceType = CanUsbInterfaceType.CanVci;
        }

        /// <summary>
        ///   Используемый девайс.
        /// </summary>
        private VciDevice _usedDevice;

        /// <summary>
        ///   Когтроллер CAN.
        /// </summary>
        private VciCanControl _usedCanControl;

        /// <summary>
        ///   Канал сообщений CAN.
        /// </summary>
        private VciCanChannel _usedCanChannel;

        /// <summary>
        ///   Трэд, который обрабатывает получение сообщений
        /// </summary>
        private Thread _receiveThread;

        /// <summary>
        ///     Эвент, который устанавливается при получении данных
        /// </summary>
        private AutoResetEvent _receiveEvent;

        public override void Init(BitRate bitRate)
        {
            _bitRate = bitRate;
            Init();
        }

        public override void Init()
        {
            if (SelectDevice())
                if (SelectSocket())
                {
                    _receiveThread = new Thread(ReceiveThreadProc);
                    _receiveThread.Start();
                }
        }

        public override void DeInit()
        {
            if (_receiveThread != null && _receiveThread.IsAlive)
            {
                _receiveThread.Abort();
            }
            DisposeAllObjectsOnDeInit();
        }

        public override bool IsReady()
        {
            if (_usedDevice == null || _usedCanControl == null || _usedCanChannel == null)
                return false;

            return _usedDevice.IsReady() && _usedCanControl.IsReady() && _usedCanChannel.IsReady();
        }

        public override bool SetBitRate(BitRate bitRate)
        {
            var tmpBitRate = _bitRate;
            _bitRate = bitRate;
            var res = _usedCanControl.SetBaudRate(GetBaudRate(), GetBaudRateEx());
            if (!res)
                _bitRate = tmpBitRate;

            return res;
        }

        private VciBitRate GetBaudRate()
        {
            switch (_bitRate)
            {
                case BitRate.b_1000:
                    return VciBitRate.Baud1000KBit;
                case BitRate.b_125:
                    return VciBitRate.Baud125KBit;
                case BitRate.b_250:
                    return VciBitRate.Baud250KBit;
                case BitRate.b_500:
                    return VciBitRate.Baud500KBit;
            }
            throw new Exception();
        }

        private VciBitRateEx GetBaudRateEx()
        {
            switch (_bitRate)
            {
                case BitRate.b_1000:
                    return VciBitRateEx.Baud1000KBit;
                case BitRate.b_125:
                    return VciBitRateEx.Baud125KBit;
                case BitRate.b_250:
                    return VciBitRateEx.Baud250KBit;
                case BitRate.b_500:
                    return VciBitRateEx.Baud500KBit;
            }
            throw new Exception();
        }

        private bool SelectDevice()
        {
            var devicesList = VciServer.GetDevicesList();

            if (devicesList == null || devicesList.Count == 0)
                return false;

            _usedDevice = devicesList[0];

            var res = _usedDevice.Init();
            return res;
        }

        private bool SelectSocket()
        {
            _usedCanChannel = new VciCanChannel(_usedDevice);
            if (!_usedCanChannel.Init(0, 1024, 1, 128, 1, 100))
                return false;

            _receiveEvent = new AutoResetEvent(false);
            _usedCanChannel.AssignReceiveEvent(_receiveEvent);

            _usedCanControl = new VciCanControl(_usedDevice);
            if (!_usedCanControl.Init(0, VciCanOperatingModes.Extended | VciCanOperatingModes.Standard, VciBitRate.Baud500KBit, VciBitRateEx.Baud500KBit))
                return false;

            return true;
        }

        private void ReceiveThreadProc()
        {
            VciCanMessage[] canMessages = new VciCanMessage[10];

            while (true)
            {
                if (_receiveEvent.WaitOne())
                { //ждём, пока прийдёт сообщение   
                    _receiveEvent.Reset();
                    int count = _usedCanChannel.ReadMessages(canMessages); //считываем сообщения из очереди
                    if (count > 0)
                    { //если удалось считать сообщения
                        lock (locker)
                        {
                            //Проверяем все сообщения
                            for (int i = 0; i < count; i++)
                            {
                                if (canMessages[i].FrameType == VciCanMsgFrameType.Data)
                                {
                                    if (OnReceiveCanMessage != null)
                                        OnReceiveCanMessage.Invoke(canMessages[i].Identifier, canMessages[i].Data);

                                    //ожидаем ли полученное сообщение
                                    if (IsExpectedMessage(canMessages[i].Identifier))
                                    {
                                        _wait = false; //если да, то завершаем ожидание
                                        _recievedData = canMessages[i].Data[1];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public override bool IsSuccessful(object code)
        {
            return ((int)code == 0);
        }

        public override bool SendMessage(object message, bool waitAnswer, uint waitId, out object recivedCode, int timeoutMs)
        {
            return SendMessage(message, waitAnswer, new uint[] { waitId }, out recivedCode, timeoutMs);
        }

        public override bool SendMessage(object message, bool waitAnswer, uint[] waitIds, out object recivedCode, int timeoutMs)
        {
            lock (locker)
            {
                VciCanMessage canMessage = (VciCanMessage)message;

                var res = _usedCanChannel.SendMessage(canMessage);
                recivedCode = res ? 0 : 1;

                if (waitAnswer)
                {
                    _waitIds = waitIds;
                    _wait = true;
                }
            }
            if (waitAnswer)
            {
                var timeout = timeoutMs > 0 ? timeoutMs : _defaultTimeoutMs;
                return WaitAction(timeout);
            }
            return true;
        }

        public override object SendReadMessage(object message)
        {
            VciCanMessage canMessage = (VciCanMessage)message;
            return _usedCanChannel.SendMessage(canMessage);
        }

        public override string GetReturnMessage(string mes, object code, bool answerWasRecieved)
        {
            var c = (int)code;
            var sb = new StringBuilder();
            sb.Append(mes);

            if (answerWasRecieved)
            {
                sb.Append(c == 0 ? " was successful " : " fail with code: " + code);
            }
            else
            {
                sb.Append("Время ожидания истекло");
            }
            return sb.ToString();
        }

        private void DisposeAllObjectsOnDeInit()
        {
            DisposeVCiObject(_usedCanChannel);
            DisposeVCiObject(_usedCanControl);
            DisposeVCiObject(_usedDevice);
        }

        private void DisposeVCiObject(object obj)
        {
            if (obj != null)
            {
                IDisposable dObj = obj as IDisposable;
                if (dObj != null)
                {
                    dObj.Dispose();
                    dObj = null;
                }
            }
        }

        protected override uint ComputeFilterMask(bool ext, uint fromId, uint toId)
        {
            uint mask = (((uint)VciCanAccMask.None) ^ (uint)((fromId ^ toId) << 1)) | 0x1;
            return mask;
        }

        protected override uint ComputeFilterCode(bool ext, uint fromId, uint toId)
        {
            uint code = (uint)((fromId & toId) << 1) | 0x0;
            return code;
        }

        protected override bool SetFilterConfig(FilterConfig filterConfig)
        {
            if (!IsReady())
                return false;

            return _usedCanControl.SetAccFilter(filterConfig.Ext, filterConfig.Code, filterConfig.Mask);
        }

        protected override object GetCanMessage(uint id, byte[] data, CanMessageFormat msgFormat)
        {
            VciCanMessage canMessage = new VciCanMessage();
            canMessage.TimeStamp = 0;
            canMessage.Identifier = id;
            canMessage.DataLength = Convert.ToByte(data.Length);
            canMessage.ExtendedFrameFormat = msgFormat == CanMessageFormat.Ext;
            canMessage.Data = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
                canMessage.Data[i] = data[i];

            return canMessage;
        }
    }
}
