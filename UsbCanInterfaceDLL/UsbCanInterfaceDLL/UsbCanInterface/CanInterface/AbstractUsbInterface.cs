﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace UsbCan_Interface
{
    /// <summary>
    ///     Тип интерфейса
    /// </summary>
    public enum CanUsbInterfaceType
    {
        Can,
        CanVci,
        CanFd,
        None
    }

    public enum BitRate
    {
        b_125,
        b_250,
        b_500,
        b_1000
    }

    public enum CanMessageFormat
    {
        /// <summary>
        /// Стандартный формат CAN сообщений (ID имеется длину 11 бит)
        /// </summary>
        Std,

        /// <summary>
        /// Расширенный формат CAN сообщений (ID имеется длину 29 бит)
        /// </summary>
        Ext
    }

    public enum FilterConfigType
    {
        /// <summary>
        /// Фмльтровать все сообщения
        /// </summary>
        None,

        /// <summary>
        /// Пропускать все сообщения
        /// </summary>
        All,

        /// <summary>
        /// 
        /// </summary>
        OnlyProgData,

        /// <summary>
        /// Пропускать только сообщения
        /// </summary>
        OnlyDataMsg
    }

    public abstract class AbstractUsbInterface
    {

        /// <summary>
        /// Получить интерфейс заданного типа
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <returns></returns>
        public static AbstractUsbInterface GetUsbInterface(CanUsbInterfaceType interfaceType)
        {
            switch (interfaceType)
            {
                case CanUsbInterfaceType.Can:
                    return new UsbCanInterface();
                case CanUsbInterfaceType.CanVci:
                    return new UsbCanVciInterface();
                default:
                    return null;
            }
        }


        /// <summary>
        /// Время, которое ждём ответа после отправки сообщения, мс
        /// </summary>
        protected const int _defaultTimeoutMs = 100;

        protected BitRate _bitRate;
        protected CanUsbInterfaceType _interfaceType;

        protected AbstractUsbInterface(BitRate bitRate)
        {
            _bitRate = bitRate;
            Counter = 0;

            _filterConfigs = new Dictionary<FilterConfigType, FilterConfig>()
            {
                { FilterConfigType.All, ComputeFilterConfig(true, 0, 0xffffffff) },
                { FilterConfigType.None, ComputeFilterConfig(true, 0, 0) },
                { FilterConfigType.OnlyProgData, ComputeFilterConfig(true, 0, 0xff) },
                { FilterConfigType.OnlyDataMsg, ComputeFilterConfig(true, 0x400, 0x4ff) },
            };
        }

        /// <summary>
        /// Счётчик отправленных сообщений
        /// </summary>
        public byte Counter { get; private set; }

        /// <summary>
        /// Экшн, который вызывается при получении сообщения, возвращает массив Can сообщений
        /// </summary>
        public Action<uint, byte[]> OnReceiveCanMessage;

        /// <summary>
        /// Объект, который используется для блокировки для избежания обработки принятого сообщения во время посылки и наоборот
        /// </summary>
        protected object locker = new object();

        /// <summary>
        /// Служит для инкремента счётчика
        /// </summary>
        public void IncrementCounter()
        {
            Counter++;
        }

        /// <summary>
        /// Инициализация интерфейса передачи данных
        /// </summary>
        /// <returns>Код выполнения операции</returns>
        public abstract void Init();
        public virtual void Init(BitRate bitRate)
        {
        }

        /// <summary>
        /// Деинициализация интерфейса
        /// </summary>
        public abstract void DeInit();

        /// <summary>
        /// Готов ли к работе интерфейс
        /// </summary>
        public abstract bool IsReady();

        /// <summary>
        /// Установить скорость интерфейса
        /// </summary>
        /// <param name="bitRate"></param>
        public abstract bool SetBitRate(BitRate bitRate);

        /// <summary>
        /// Отправка сообщения по CAN'у
        /// </summary>
        /// <param name="id">Идентификатор сообщения</param>
        /// <param name="data">Массив данных</param>
        /// <param name="msgFormat">Формат сообщения, по умолчанию расширенный</param>
        /// <returns></returns>
        public bool SendMessage(uint id, byte[] data, CanMessageFormat msgFormat = CanMessageFormat.Ext, int timeoutMs = -1)
        {
            object recivedCode;
            return SendMessage(GetCanMessage(id, data, msgFormat), false, 0, out recivedCode, timeoutMs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="waitAnswer"></param>
        /// <param name="waitId"></param>
        /// <param name="recivedCode"></param>
        /// <returns></returns>
        /// 

        /// <summary>
        /// Отправка сообщения по CAN'у
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="waitAnswer">Нужно ли ожидать получение ответа</param>
        /// <param name="recivedCode">Результат ответа</param>
        /// <returns></returns>
        public abstract bool SendMessage(object message, bool waitAnswer, uint waitId, out object recivedCode, int timeoutMs = -1);

        /// <summary>
        /// Отправка сообщения по CAN'у
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="waitAnswer">Нужно ли ожидать получение ответа</param>
        /// <param name="recivedCode">Результат ответа</param>
        /// <returns></returns>
        public abstract bool SendMessage(object message, bool waitAnswer, uint[] waitIds, out object recivedCode, int timeoutMs = -1);

        /// <summary>
        /// Отправка сообщения на считывания информации 
        /// </summary>
        /// <returns>Код выполнения операции</returns>
        public abstract object SendReadMessage(object message);

        /// <summary>
        /// Возвращает успешность или неуспешность кода. 
        /// Передаётся код, возвращённый из других операций
        /// </summary>
        public abstract bool IsSuccessful(object code);

        /// <summary>
        ///Печатает сообщение на основании полученного кода.
        ///Это может быть успех или провал. 
        /// </summary>
        public abstract string GetReturnMessage(string mes, object code, bool answerWasRecieved);

        public static bool ErrorControl = false;

        public CanUsbInterfaceType GetInterfaceType()
        {
            return _interfaceType;
        }

        /// <summary>
        /// Если true, то ожидаем ответ
        /// </summary>
        protected bool _wait;

        /// <summary>
        /// id ожидаемого сообщения в ответ
        /// </summary>
        protected uint[] _waitIds;

        protected byte _recievedData;

        protected bool WaitAction(int timeoutMs)
        {
            var startTime = DateTime.Now;
            while (true)
            {
                if (!_wait)
                {
                    break;
                }
                if ((DateTime.Now - startTime) >= TimeSpan.FromMilliseconds(timeoutMs))
                {
                    return false;
                }
            }
            return IsRecievedAnswerSuccessful(_recievedData);
        }

        protected readonly Func<int, bool> IsRecievedAnswerSuccessful = v =>
        {
            if (!ErrorControl) { return true; }
            if (v == 0 || v == 1) { return true; }
            var errorMessage = "Error: " + v;
            //MessageBox.Show(errorMessage);
            //LogManager.AddLog(errorMessage, LogManager.LogColor.Red, true);
            return false;
        };

        /// <summary>
        /// Установить фильтр для сообщений в заданном диапазоне
        /// </summary>
        /// <param name="ext"></param>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <returns></returns>
        public bool SetFilterMessages(bool ext, uint fromId, uint toId)
        {
            if (fromId > toId)
                return false;
            return SetFilterConfig(ComputeFilterConfig(ext, fromId, toId));
        }

        /// <summary>
        /// Установить заданный конфиг для фильтрации
        /// </summary>
        /// <param name="filterConfig"></param>
        /// <returns></returns>
        protected abstract bool SetFilterConfig(FilterConfig filterConfig);


        /// <summary>
        /// Вычислить маску для заданного диапазона
        /// </summary>
        /// <param name="ext"></param>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <returns></returns>
        protected abstract uint ComputeFilterMask(bool ext, uint fromId, uint toId);

        /// <summary>
        /// Вычислдить код для заданного диапазона
        /// </summary>
        /// <param name="ext"></param>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <returns></returns>
        protected abstract uint ComputeFilterCode(bool ext, uint fromId, uint toId);

        /// <summary>
        /// Получить Can сообщение
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <param name="msgFormat"></param>
        /// <returns></returns>
        protected abstract object GetCanMessage(uint id, byte[] data, CanMessageFormat msgFormat);


        /// <summary>
        /// Словарь со всем ивозможными конфигами для фильтрации
        /// </summary>
        private static Dictionary<FilterConfigType, FilterConfig> _filterConfigs;

        /// <summary>
        /// Получить конфиг для приёма сообщений в диапазоне
        /// </summary>
        /// <param name="ext"></param>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <returns></returns>
        protected FilterConfig ComputeFilterConfig(bool ext, uint fromId, uint toId)
        {
            var mask = ComputeFilterMask(ext, fromId, toId);
            var code = ComputeFilterCode(ext, fromId, toId);
            return new FilterConfig(ext, mask, code, fromId, toId);
        }

        public bool SetFilterConfig(FilterConfigType type)
        {
            FilterConfig filterCondig = GetFilterConfig(type);
            if (filterCondig == null)
                return false;

            return SetFilterConfig(filterCondig);
        }

        public static FilterConfig GetFilterConfig(FilterConfigType type)
        {
            if (!_filterConfigs.ContainsKey(type))
                return null;

            return _filterConfigs[type];
        }

        protected bool IsExpectedMessage(uint id)
        {
            if (_waitIds == null)
                return false;

            return _waitIds.Contains(id);
        }
    }

    public class FilterConfig
    {
        public uint Mask { get; set; }
        public uint Code { get; set; }
        public bool Ext { get; set; }
        public uint FromId { get; set; }
        public uint ToId { get; set; }

        public FilterConfig(bool ext, uint mask, uint code, uint fromId, uint toId)
        {
            Mask = mask;
            Code = code;
            Ext = ext;
            FromId = fromId;
            ToId = toId;
        }
    }
}
