﻿using System;
using System.Runtime.InteropServices;

namespace Ixxat
{
    struct VCIID
    {
        LUID AsLuid;
        Int64 AsInt64;
    }

    public struct LUID
    {
        uint LowPart;
        long HighPart;
    }

    public struct GUID
    {
        uint Data1;
        uint Data2;
        uint Data3;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        byte[] Data4;
    }

    public struct UniqueHardwareId
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        char[] AsChar;
        GUID AsGuid;
    }
    public struct VCIDEVICEINFO
    {
        internal VCIID VciObjectId;          // unique VCI object identifier
        internal GUID DeviceClass;          // device class identifier

        internal UInt16 DriverMajorVersion;   // major driver version number
        internal UInt16 DriverMinorVersion;   // minor driver version number

        internal UInt16 HardwareMajorVersion; // major hardware version number
        internal UInt16 HardwareMinorVersion; // minor hardware version number

        internal UniqueHardwareId _UniqueHardwareId;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        internal char[] Description;// = new char[128];       // device description (e.g: "PC-I04-PCI")
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        internal char[] Manufacturer;// = new char[128];      // device manufacturer (e.g: "IXXAT Automation")
    }
}
