﻿using System;
using System.Collections.Generic;

namespace Ixxat {
    internal static class VciServer {
        public static List<VciDevice> GetDevicesList() {
            List<VciDevice> devicesList = new List<VciDevice>();

            uint res;

            IntPtr phEnum;
            res = Unmanaged.vciEnumDeviceOpen(out phEnum); //открываем список девайсов
            if (!ResultIsSuccess(res))
                return null;
            
            res = Unmanaged.vciEnumDeviceReset(phEnum);
            if (!ResultIsSuccess(res))
                return null;

            VCIDEVICEINFO pInfo;
            res = VciConstants.VCI_OK;

            while (ResultIsSuccess(res)) {
                res = Unmanaged.vciEnumDeviceNext(phEnum, out pInfo); //пробегаем по всем девайсам и добавляем их в список
                if (ResultIsSuccess(res)) {
                    VciDevice device = new VciDevice(pInfo);
                    devicesList.Add(device);
                }
            }
            
            res = Unmanaged.vciEnumDeviceClose(phEnum); //закрываем список девайсов
            
            return devicesList;
        }

        public static bool ResultIsSuccess(uint result) {
            return result == VciConstants.VCI_OK;
        }
    }
}
