﻿using System;

namespace Ixxat {
    internal class VciCanControl : IDisposable {
        private IntPtr _hDevice;
        private IntPtr _hCanCtl;
        private byte _operationMode;

        public VciCanControl(VciDevice device) {
            _hDevice = device.GetHandle();
        }

        public bool Init(uint canNumber, VciCanOperatingModes operatingMode, VciBitRate baudRate, VciBitRateEx baudRateEx) {
            uint res;
            res = Unmanaged.canControlOpen(_hDevice, canNumber, out _hCanCtl);

            if (VciServer.ResultIsSuccess(res)) {
                _operationMode = (byte)operatingMode;
                res = Unmanaged.canControlInitialize(_hCanCtl, _operationMode, (byte)baudRate, (byte)baudRateEx);
                if (VciServer.ResultIsSuccess(res)) {
                    res = Unmanaged.canControlSetAccFilter(_hCanCtl, true, VciConstants.CAN_ACC_CODE_NONE, VciConstants.CAN_ACC_MASK_ALL);
                    if (VciServer.ResultIsSuccess(res)) {
                        res = Unmanaged.canControlStart(_hCanCtl, true);
                        return VciServer.ResultIsSuccess(res);
                    }
                }
            }

            return false;
        }

        public bool IsReady() {
            CANLINESTATUS stat;
            var res = Unmanaged.canControlGetStatus(_hCanCtl, out stat);
            
            return VciServer.ResultIsSuccess(res);
        }


        public bool SetBaudRate(VciBitRate baudRate, VciBitRateEx baudRateEx) {
            uint res;
            res = Unmanaged.canControlStart(_hCanCtl, false);
            if (VciServer.ResultIsSuccess(res)) {
                res = Unmanaged.canControlInitialize(_hCanCtl, _operationMode, (byte)baudRate, (byte)baudRateEx);
                if (VciServer.ResultIsSuccess(res)) {
                    res = Unmanaged.canControlStart(_hCanCtl, true);
                    return VciServer.ResultIsSuccess(res);
                }
            }

            return false;
        }

        public bool SetAccFilter(bool extended, uint code, uint mask) {
            uint res;
            res = Unmanaged.canControlStart(_hCanCtl, false);
            if (VciServer.ResultIsSuccess(res)) {
                res = Unmanaged.canControlSetAccFilter(_hCanCtl, extended, code, mask);
                if (VciServer.ResultIsSuccess(res)) {
                    res = Unmanaged.canControlStart(_hCanCtl, true);
                    return VciServer.ResultIsSuccess(res);
                }                
            }

            return false;
        }

        public void Dispose() {
            Unmanaged.canControlClose(_hCanCtl);
        }
    }
}
