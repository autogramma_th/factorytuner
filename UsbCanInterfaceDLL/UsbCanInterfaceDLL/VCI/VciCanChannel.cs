﻿using System;
using System.Threading;

namespace Ixxat {
    internal class VciCanChannel : IDisposable {
        private IntPtr _hDevice;
        private IntPtr _hChannel;
        private uint _sendMessageTimeoutMs;
        private Thread _waitReceiveThread;
        private AutoResetEvent _receiveEvent;
        private uint _waitReceiveEventTimeout = 0;


        public VciCanChannel(VciDevice device) {
            _hDevice = device.GetHandle();
        }

        public bool Init(uint channelNumber, UInt16 receiveFifoSize, UInt16 receiveThreshold, UInt16 transmitFifoSize, UInt16 transmitThreshold, uint sendMessageTimeoutMs) {
            _sendMessageTimeoutMs = sendMessageTimeoutMs;

            uint res;
            res = Unmanaged.canChannelOpen(_hDevice, channelNumber, false, out _hChannel);
            if (VciServer.ResultIsSuccess(res)) {
                res = Unmanaged.canChannelInitialize(_hChannel, receiveFifoSize, receiveThreshold, transmitFifoSize, transmitThreshold);
                if (VciServer.ResultIsSuccess(res)) {
                    res = Unmanaged.canChannelActivate(_hChannel, true);
                    var success = VciServer.ResultIsSuccess(res);
                    if (success) {
                        _waitReceiveThread = new Thread(WaitReceiveProc);
                        _waitReceiveThread.Start();
                    }
                    return success;
                }
            }
            return false;
        }

        public bool IsReady() {
            CANCHANSTATUS stat;
            var res = Unmanaged.canChannelGetStatus(_hChannel, out stat);

            return VciServer.ResultIsSuccess(res);
        }

        public bool ReadMessage(out VciCanMessage canMessage) {            
            mngtCANMSG msg;
            var res = Unmanaged.canChannelPeekMessage(_hChannel, out msg);
            canMessage = new VciCanMessage();
            canMessage.FromMCanMsg(msg);

            return VciServer.ResultIsSuccess(res);
        }

        public int ReadMessages(VciCanMessage[] canMessages) {
            bool res = true;
            int readCount = 0; //Количество считанных сообщений
            while (res && (readCount < canMessages.Length)) {
                VciCanMessage canMessage;
                res = ReadMessage(out canMessage);
                if (res) {
                    canMessages[readCount] = canMessage;
                    readCount ++;
                }
            }
            return readCount;
        }


        public bool SendMessage(VciCanMessage canMessage) {
            var res = Unmanaged.canChannelSendMessage(_hChannel, _sendMessageTimeoutMs, canMessage.ToMCanMsg());
            return VciServer.ResultIsSuccess(res);
        }
        
        public int SendMessages(VciCanMessage[] canMessages) {
            int sendCount = 0;

            for (int i = 0; i < canMessages.Length; i++) {
                if (SendMessage(canMessages[i]))
                    sendCount++;
            }
            return sendCount;
        }

        public void AssignReceiveEvent(AutoResetEvent receiveEvent) {
            _receiveEvent = receiveEvent;
        }

        public void Dispose() {            
            if (_waitReceiveThread != null && _waitReceiveThread.IsAlive) {
                _waitReceiveThread.Abort();
            }
            _receiveEvent = null;
            Unmanaged.canChannelClose(_hChannel);
        }

        private void WaitReceiveProc() {
            while (true) {
                if (_receiveEvent != null) {
                    var res = Unmanaged.canChannelWaitRxEvent(_hChannel, _waitReceiveEventTimeout);
                    if (VciServer.ResultIsSuccess(res)) {
                        _receiveEvent.Set();
                    }
                }
                Thread.Sleep(0);
            }
        }
    }
}
