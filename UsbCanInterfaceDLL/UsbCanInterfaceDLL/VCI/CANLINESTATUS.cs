﻿

namespace Ixxat
{
    internal struct CANLINESTATUS {
        byte bOpMode;
        byte bBtReg0;
        byte bBtReg1;
        byte bBusLoad;
        uint dwStatus;
    }
}
