﻿using System;
using System.Runtime.InteropServices;

namespace Ixxat {
    internal class VciDevice : IDisposable {
        private IntPtr _hDevice;
        private VCIDEVICEINFO _deviceInfo;

        public VciDevice(VCIDEVICEINFO deviceInfo) {
            _deviceInfo = deviceInfo;
        }

        public bool Init() {            
            uint res = Unmanaged.vciDeviceOpen(_deviceInfo.VciObjectId, out _hDevice);
            return VciServer.ResultIsSuccess(res);
        }

        public bool IsReady() {
            VCIDEVICEINFO info;
            var res = Unmanaged.vciDeviceGetInfo(_hDevice, out info);
            return VciServer.ResultIsSuccess(res);
        }

        public void Dispose() {
            Unmanaged.vciDeviceClose(_hDevice);
        }

        internal IntPtr GetHandle() {
            return _hDevice;
        }
    }
}
