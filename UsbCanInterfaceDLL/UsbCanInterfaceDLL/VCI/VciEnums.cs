﻿using System;

namespace Ixxat
{
    public enum VciCanOperatingModes : byte {
        Undefined = 0x00,
        Standard = 0x01,
        Extended = 0x02,
        ErrFrame = 0x04,
        ListOnly = 0x08,
        LowSpeed = 0x10,
    }

    public enum VciBitRate : byte {
        Baud1000KBit = VciConstants.CAN_BT0_1000KB,
        Baud500KBit = VciConstants.CAN_BT0_500KB,
        Baud250KBit = VciConstants.CAN_BT0_250KB,
        Baud125KBit = VciConstants.CAN_BT0_125KB
    }

    public enum VciBitRateEx : byte {
        Baud1000KBit = VciConstants.CAN_BT1_1000KB,
        Baud500KBit = VciConstants.CAN_BT1_500KB,
        Baud250KBit = VciConstants.CAN_BT1_250KB,
        Baud125KBit = VciConstants.CAN_BT1_125KB
    }

    public enum VciCanAccMask : uint {
        All = 0,
        None = 0xFFFFFFFF,
    }

    public enum VciCanMsgFrameType {
        Data,
        Info,
        Error,
        Status,
        Wakeup,
        TimeOverrun,
        TimeReset
    }
}
