﻿using System;

namespace Ixxat
{
    internal static class VciConstants
    {
        private const uint SEV_ERROR = 0xC0000000;
        private const uint CUSTOMER_FLAG = 0x20000000;
        private const uint FACILITY_VCI = 0x00010000;
        private const uint SEV_VCI_ERROR = SEV_ERROR | CUSTOMER_FLAG | FACILITY_VCI;

        internal const uint VCI_OK = 0;  
                
        internal const uint VCI_E_NO_MORE_ITEMS = SEV_VCI_ERROR | 0x000F;
        internal const uint VCI_E_TIMEOUT = SEV_VCI_ERROR | 0x000B;


        /*****************************************************************************
         * predefined CiA bit rates
         ****************************************************************************/

        /*const uint CAN_BT0_10KB = 0x31
        const uint CAN_BT1_10KB = 0x1C
        #define CAN_BT0_20KB     0x18
        #define CAN_BT1_20KB     0x1C
        #define CAN_BT0_50KB     0x09
        #define CAN_BT1_50KB     0x1C
        #define CAN_BT0_100KB    0x04
        #define CAN_BT1_100KB    0x1C*/
        internal const byte CAN_BT0_125KB = 0x03;
        internal const byte CAN_BT1_125KB = 0x1C;
        internal const byte CAN_BT0_250KB = 0x01;
        internal const byte CAN_BT1_250KB = 0x1C;
        internal const byte CAN_BT0_500KB = 0x00;
        internal const byte CAN_BT1_500KB = 0x1C;
        /*#define CAN_BT0_800KB    0x00
        #define CAN_BT1_800KB    0x16*/
        internal const byte CAN_BT0_1000KB = 0x00;
        internal const byte  CAN_BT1_1000KB = 0x14;

        /*#define CAN_BT01_10KB    0x31,0x1C
        #define CAN_BT01_20KB    0x18,0x1C
        #define CAN_BT01_50KB    0x09,0x1C
        #define CAN_BT01_100KB   0x04,0x1C
        #define CAN_BT01_125KB   0x03,0x1C
        #define CAN_BT01_250KB   0x01,0x1C
        #define CAN_BT01_500KB   0x00,0x1C
        #define CAN_BT01_800KB   0x00,0x16
        #define CAN_BT01_1000KB  0x00,0x14*/


        /*****************************************************************************
         * controller operating modes
         ****************************************************************************/
        
        //
        // acceptance code and mask to accept all CAN IDs
        //

        internal const UInt32 CAN_ACC_MASK_ALL = 0x00000000;
        internal const UInt32 CAN_ACC_CODE_ALL = 0x00000000;

        //
        // acceptance code and mask to reject all CAN IDs
        //

        internal const UInt32 CAN_ACC_MASK_NONE = 0xFFFFFFFF;
        internal const UInt32 CAN_ACC_CODE_NONE = 0x80000000;


        internal const uint INFINITE = 0xFFFFFFFF;

    }
}
