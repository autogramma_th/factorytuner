﻿

namespace Ixxat {
    internal struct CANCHANSTATUS {
        CANLINESTATUS sLineStatus;
        bool fActivated;
        bool fRxOverrun;
        byte bRxFifoLoad;
        byte bTxFifoLoad;
    }
}