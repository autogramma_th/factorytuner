﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Ixxat {
    
    public struct VciCanMessage {
        
        public byte DataLength { get; set; }
        public VciCanMsgFrameType FrameType { get; set; }
        public bool ExtendedFrameFormat { get; set; }
        public uint Identifier { get; set; }
        public uint TimeStamp { get; set; }
        public byte[] Data { get; set; }


        internal void FromMCanMsg(mngtCANMSG mCanMsg) {
            Identifier = mCanMsg.dwMsgId;
            TimeStamp = mCanMsg.dwTime;

            ExtendedFrameFormat = (mCanMsg.uMsgInfo.bFlags & 0x80) == 0x80;
            FrameType = (VciCanMsgFrameType)mCanMsg.uMsgInfo.bType;
            DataLength = Convert.ToByte(mCanMsg.uMsgInfo.bFlags & 0x0F);

            Data = new byte[DataLength];
            for (int i = 0; i < DataLength; i++)
                Data[i] = mCanMsg.abData[i];
        }

        internal mngtCANMSG ToMCanMsg() {
            mngtCANMSG mCanMsg = new mngtCANMSG();
            mCanMsg.dwMsgId = Identifier;
            mCanMsg.dwTime = TimeStamp;

            mCanMsg.uMsgInfo.bFlags = Convert.ToByte(Convert.ToByte(DataLength & 0x0F) | Convert.ToByte(ExtendedFrameFormat) << 7);

            mCanMsg.abData = new byte[DataLength];
            for (int i = 0; i < DataLength; i++)
                mCanMsg.abData[i] = Data[i];
            return mCanMsg;
        }
    }

    internal struct mngtCANMSG {
        public UInt32 dwTime;

        public UInt32 dwMsgId;

        public mngtCANMSGINFO uMsgInfo;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] abData;
    }

    internal struct mngtCANMSGINFO {
        public byte bType;
        public byte bReserved;
        public byte bFlags;
        public byte bAccept;
    }
}
