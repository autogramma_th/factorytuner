﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Ixxat
{
    internal static class Unmanaged
    {    
        [DllImport("vcinpl.dll")]
        public static extern uint vciEnumDeviceOpen(out IntPtr phEnum);

        [DllImport("vcinpl.dll")]
        public static extern uint vciEnumDeviceReset(IntPtr hEnum);        
        
        [DllImport("vcinpl.dll")]
        public static extern uint vciEnumDeviceNext(IntPtr hEnum, out VCIDEVICEINFO pInfo);

        [DllImport("vcinpl.dll")]
        public static extern uint vciEnumDeviceClose(IntPtr phEnum);

        [DllImport("vcinpl.dll")]
        public static extern uint vciDeviceOpen(VCIID rVciidDevice, out IntPtr phDevice);
                

        [DllImport("vcinpl.dll")]
        public static extern uint vciDeviceOpenDlg(IntPtr hwndParent, out IntPtr phDevice);

        [DllImport("vcinpl.dll")]
        public static extern uint canChannelOpen(IntPtr hDevice, UInt32 dwCanNo, bool fExclusive, out IntPtr phChannel);
        
        [DllImport("vcinpl.dll")]
        public static extern uint canChannelInitialize(IntPtr hChannel, UInt16 wRxFifoSize, UInt16 wRxThreshold, UInt16 wTxFifoSize, UInt16 wTxThreshold);
        
        [DllImport("vcinpl.dll")]
        public static extern uint canChannelActivate(IntPtr hChannel, bool fEnable);
                
        [DllImport("vcinpl.dll")]
        public static extern uint canControlOpen(IntPtr hDevice, UInt32 dwCanNo, out IntPtr phControl);

        [DllImport("vcinpl.dll")]
        public static extern uint canControlInitialize(IntPtr hControl, byte bMode, byte bBtr0, byte bBtr1);
        
        [DllImport("vcinpl.dll")]
        public static extern uint canControlSetAccFilter(IntPtr hControl, bool fExtended, UInt32 dwCode, UInt32 dwMask);

        [DllImport("vcinpl.dll")]
        public static extern uint canControlStart(IntPtr hControl, bool fStart);
        

        [DllImport("vcinpl.dll")]
        public static extern uint canChannelReadMessage(IntPtr hChannel, UInt32 dwMsTimeout, out mngtCANMSG pCanMsg);
        
        [DllImport("vcinpl.dll")]
        public static extern uint canChannelPeekMessage(IntPtr hChannel, out mngtCANMSG pCanMsg);

        [DllImport("vcinpl.dll")]
        public static extern uint canChannelWaitRxEvent(IntPtr hChannel, UInt32 dwMsTimeout);
        
        [DllImport("vcinpl.dll")]
        public static extern uint canChannelSendMessage(IntPtr hChannel, UInt32 dwMsTimeout, mngtCANMSG pCanMsg);

        [DllImport("vcinpl.dll")]
        public static extern uint canChannelPostMessage(IntPtr hChannel, mngtCANMSG pCanMsg);

        
        [DllImport("vcinpl.dll")]
        public static extern uint canChannelClose(IntPtr hChannel);

        [DllImport("vcinpl.dll")]
        public static extern uint canControlClose(IntPtr hControl);

        [DllImport("vcinpl.dll")]
        public static extern uint vciDeviceClose(IntPtr phDevice);



        [DllImport("vcinpl.dll")]
        public static extern uint vciDeviceGetInfo(IntPtr hDevice, out VCIDEVICEINFO pInfo);

        [DllImport("vcinpl.dll")]
        public static extern uint canControlGetStatus(IntPtr hControl, out CANLINESTATUS pStatus);

        [DllImport("vcinpl.dll")]
        public static extern uint canChannelGetStatus(IntPtr hChannel, out CANCHANSTATUS pStatus);

        [DllImport("vcinpl.dll")]
        public static extern void vciFormatError(uint hrError, [MarshalAs(UnmanagedType.LPStr)] out string pszText);
    }
}