﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies flags for cyclical CAN messages.
    ///     These flags can be used in combinations with methode <see cref="EnableCyclicCanMsg" />.
    /// </summary>
    internal enum eUcanCyclicFlags : uint {
        /// <summary>Stopps the transmission of cyclic CAN messages.</summary>
        USBCAN_CYCLIC_FLAG_STOPP = 0x0,

        /// <summary>Global enable of transmission of cyclic CAN messages.</summary>
        USBCAN_CYCLIC_FLAG_START = 0x80000000,

        /// <summary>List of cyclcic CAN messages will be processed in sequential mode (otherwise in parallel mode).</summary>
        USBCAN_CYCLIC_FLAG_SEQUMODE = 0x40000000,

        /// <summary>
        ///     No echo will be sent back if echo mode is enabled with methode <see cref="InitCan" />.
        /// </summary>
        USBCAN_CYCLIC_FLAG_NOECHO = 0x10000,

        /// <summary>CAN message with index 0 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_0 = 0x1,

        /// <summary>CAN message with index 1 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_1 = 0x2,

        /// <summary>CAN message with index 2 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_2 = 0x4,

        /// <summary>CAN message with index 3 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_3 = 0x8,

        /// <summary>CAN message with index 4 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_4 = 0x10,

        /// <summary>CAN message with index 5 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_5 = 0x20,

        /// <summary>CAN message with index 6 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_6 = 0x40,

        /// <summary>CAN message with index 7 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_7 = 0x80,

        /// <summary>CAN message with index 8 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_8 = 0x100,

        /// <summary>CAN message with index 9 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_9 = 0x200,

        /// <summary>CAN message with index 10 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_10 = 0x400,

        /// <summary>CAN message with index 11 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_11 = 0x800,

        /// <summary>CAN message with index 12 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_12 = 0x1000,

        /// <summary>CAN message with index 13 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_13 = 0x2000,

        /// <summary>CAN message with index 14 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_14 = 0x4000,

        /// <summary>CAN message with index 15 of the list will not be sent.</summary>
        USBCAN_CYCLIC_FLAG_LOCK_15 = 0x8000
    }

}