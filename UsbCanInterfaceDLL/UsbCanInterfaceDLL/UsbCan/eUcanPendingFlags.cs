﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies flags for methode <see cref="UsbCanServer.GetMsgPending" />.
    ///     These flags can be uses in combinations.
    /// </summary>
    internal enum eUcanPendingFlags : uint {
        /// <summary>number of pending CAN messages in receive buffer of USB-CAN-library</summary>
        USBCAN_PENDING_FLAG_RX_DLL = 0x1,

        /// <summary>reserved</summary>
        USBCAN_PENDING_FLAG_RX_SYS = 0x2,

        /// <summary>number of pending CAN messages in receive buffer of firmware</summary>
        USBCAN_PENDING_FLAG_RX_FW = 0x4,

        /// <summary>number of pending CAN messages in transmit buffer of USB-CAN-library</summary>
        USBCAN_PENDING_FLAG_TX_DLL = 0x10,

        /// <summary>reserved</summary>
        USBCAN_PENDING_FLAG_TX_SYS = 0x20,

        /// <summary>number of pending CAN messages in transmit buffer of firmware</summary>
        USBCAN_PENDING_FLAG_TX_FW = 0x40,

        /// <summary>number of pending CAN messages in all receive buffers</summary>
        USBCAN_PENDING_FLAG_RX_ALL = (USBCAN_PENDING_FLAG_RX_DLL | USBCAN_PENDING_FLAG_RX_SYS | USBCAN_PENDING_FLAG_RX_FW),

        /// <summary>number of pending CAN messages in all transmit buffers</summary>
        USBCAN_PENDING_FLAG_TX_ALL = (USBCAN_PENDING_FLAG_TX_DLL | USBCAN_PENDING_FLAG_TX_SYS | USBCAN_PENDING_FLAG_TX_FW),

        /// <summary>number of pending CAN messages in all buffers</summary>
        USBCAN_PENDING_FLAG_ALL = (USBCAN_PENDING_FLAG_RX_ALL | USBCAN_PENDING_FLAG_TX_ALL)
    }

}