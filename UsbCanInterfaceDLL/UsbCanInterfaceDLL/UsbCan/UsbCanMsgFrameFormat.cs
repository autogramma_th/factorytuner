﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies values for the frame format of CAN messages for member <see cref="CanMessage.FrameFormat" />
    ///     in structure <see cref="CanMessage" />. These values can be combined.
    /// </summary>
    public enum UsbCanMsgFrameFormat : byte {
        /// <summary>standard CAN data frame with 11 bit ID (CAN2.0A spec.)</summary>
        Std = 0x0,

        /// <summary>transmit echo</summary>
        Echo = 0x20,

        /// <summary>CAN remote request frame with</summary>
        Rtr = 0x40,

        /// <summary>extended CAN data frame with 29 bit ID (CAN2.0B spec.)</summary>
        Ext = 0x80
    }

}