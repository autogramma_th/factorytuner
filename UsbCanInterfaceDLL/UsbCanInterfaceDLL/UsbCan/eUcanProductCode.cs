﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     These values defines product codes for all known USB-CANmodul derivates received in member
    ///     <see cref="tUcanHardwareInfoEx.m_dwProductCode" /> of structure <see cref="tUcanHardwareInfoEx" />
    ///     with methode <see cref="UsbCanServer.GetHardwareInfo" />.
    /// </summary>
    internal enum eUcanProductCode : uint {
        /// <summary>Product code for GW-001 (outdated).</summary>
        USBCAN_PRODCODE_PID_GW001 = 0x1100,

        /// <summary>Product code for GW-002 (outdated).</summary>
        USBCAN_PRODCODE_PID_GW002 = 0x1102,

        /// <summary>Product code for Multiport CAN-to-USB.</summary>
        USBCAN_PRODCODE_PID_MULTIPORT = 0x1103,

        /// <summary>Product code for USB-CANmodul1.</summary>
        USBCAN_PRODCODE_PID_BASIC = 0x1104,

        /// <summary>Product code for USB-CANmodul2.</summary>
        USBCAN_PRODCODE_PID_ADVANCED = 0x1105,

        /// <summary>Product code for USB-CANmodul8.</summary>
        USBCAN_PRODCODE_PID_USBCAN8 = 0x1107,

        /// <summary>Product code for USB-CANmodul16.</summary>
        USBCAN_PRODCODE_PID_USBCAN16 = 0x1109,

        /// <summary>Reserved.</summary>
        USBCAN_PRODCODE_PID_RESERVED1 = 0x1144,

        /// <summary>Reserved.</summary>
        USBCAN_PRODCODE_PID_RESERVED2 = 0x1145
    }

}