﻿namespace Asap1B.UsbCan {

    internal static class Constants {
        /// <summary>Maximum number of modules that are supported.</summary>
        public const uint USBCAN_MAX_MODULES = 64;

        /// <summary>Maximum number of applications that can use the USB-CAN-library.</summary>
        public const uint USBCAN_MAX_INSTANCES = 64;

        /// <summary>
        ///     With the methode <see cref="InitCan" /> the module is used, which is detected at first.
        ///     This value only should be used in case only one module is connected to the computer.
        /// </summary>
        public const byte USBCAN_ANY_MODULE = 255;

        /// <summary>
        ///     no valid USB-CAN Handle (only used internaly)
        /// </summary>
        private const byte USBCAN_INVALID_HANDLE = 0xff;


        /// <summary>Specifies the acceptance mask for receiving all CAN messages.</summary>
        public const uint USBCAN_AMR_ALL = 0xffffffff;

        /// <summary>Specifies the acceptance code for receiving all CAN messages.</summary>
        public const uint USBCAN_ACR_ALL = 0x0;

        /// <summary>
        ///     Specifies pre-defined values for the Output Control Register of SJA1000 on GW-001 and GW-002.
        ///     These values are only important for GW-001 and GW-002.
        ///     They does not have an effect on sysWORXX USB-CANmoduls.
        /// </summary>
        public const ushort USBCAN_DEFAULT_BUFFER_ENTRIES = 4096;

        // combinations of flags for UcanResetCanEx()
        //      NOTE: for combinations use OR (example: USBCAN_RESET_NO_COUNTER_ALL or USBCAN_RESET_NO_BUFFER_ALL)

        public const uint USBCAN_PRODCODE_MASK_PID = 0xffff;

        public const uint USBCAN_PRODCODE_MASK_DID = 0xffff0000;

        public const uint USBCAN_PRODCODE_PID_TWO_CHA = 0x1;

        public const uint USBCAN_PRODCODE_PID_TERM = 0x1;

        public const uint USBCAN_PRODCODE_PID_RBUSER = 0x1;

        public const uint USBCAN_PRODCODE_PID_RBCAN = 0x1;


        /// <summary>
        ///     definitions for cyclic CAN messages
        /// </summary>
        public const uint USBCAN_MAX_CYCLIC_CANMSG = 16;
    }

}