﻿namespace Asap1B.UsbCan {

    /// <summary>Specifies pre-defined baud rate values for GW-001, GW-002 and all sysWORXX USB-CANmoduls.</summary>
    internal enum UsbCanBaudrate : short
    {
        /// <summary>1000 kBit/sec</summary>
        Baud1MBit = 0x14,

        /// <summary>800 kBit/sec</summary>
        Baud800KBit = 0x16,

        /// <summary>500 kBit/sec</summary>
        Baud500KBit = 0x1c,

        /// <summary>250 kBit/sec</summary>
        Baud250KBit = 0x11c,

        /// <summary>125 kBit/sec</summary>
        Baud125KBit = 0x31c,

        /// <summary>100 kBit/sec</summary>
        Baud100KBit = 0x432f,

        /// <summary>50 kBit/sec</summary>
        Baud50KBit = 0x472f,

        /// <summary>20 kBit/sec</summary>
        Baud20KBit = 0x532f,

        /// <summary>10 kBit/sec</summary>
        Baud10KBit = 0x672f,

        /// <summary>Uses pre-defined extended values of baudrate for all sysWORXX USB-CANmoduls.</summary>
        BaudUseBtrex = 0x0,

        /// <summary>Automatic baud rate detection (not implemented in this version).</summary>
        BaudAuto = -1
    }

}