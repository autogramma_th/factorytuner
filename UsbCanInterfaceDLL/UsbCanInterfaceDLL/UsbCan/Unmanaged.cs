﻿
using System;
using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    internal static class Unmanaged {
#if X64
        private const string CanDLL = "USBCAN64.dll";
#else
        private const string CanDLL = "Usbcan32.dll";
#endif
        /// <summary> void PUBLIC UcanCallbackFktEx (tUcanHandle UcanHandle_p, DWORD dwEvent_p, byte bChannel_p, void* pArg_p); </summary>
        public delegate void tCallbackFktEx(byte tUcanHandle_p, eUcanCbEvent dwEvent_p, UsbCanChannel bChannel_p, IntPtr pArg_p);

        /// <summary> void (PUBLIC *tConnectControlFktEx) (DWORD dwEvent_p, DWORD dwParam_p, void* pArg_p); </summary>
        public delegate void tConnectControlFktEx(eUcanCbEvent dwEvent_p, uint dwParam_p, IntPtr pArg_p);

        /// <summary> BOOL PUBLIC UcanSetDebugMode (DWORD dwDbgLevel_p, _TCHAR* pszFilePathName_p, DWORD dwFlags_p); </summary>
        [DllImport(CanDLL)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UcanSetDebugMode(int dwDbgLevel_p, [MarshalAs(UnmanagedType.LPStr)] string pszFilePathName_p, int dwFlags_p);


        /// <summary> DWORD PUBLIC UcanGetVersionEx (tUcanVersionType VerType_p); </summary>
        [DllImport(CanDLL)]
        public static extern int UcanGetVersionEx(tUcanVersionType VerType_p);


        /// <summary> DWORD PUBLIC UcanGetFwVersion (tUcanHandle UcanHandle_p); </summary>
        [DllImport(CanDLL)]
        public static extern int UcanGetFwVersion(byte UcanHandle_p);


        /// <summary> byte PUBLIC UcanInitHwConnectControlEx (tConnectControlFktEx fpConnectControlFktEx_p, void* pCallbackArg_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanInitHwConnectControlEx([MarshalAs(UnmanagedType.FunctionPtr)] tConnectControlFktEx fpConnectControlFktEx_p, IntPtr pCallbackArg_p);


        /// <summary> byte PUBLIC UcanDeinitHwConnectControl (void) </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanDeinitHwConnectControl();


        /// <summary> byte PUBLIC UcanInitHardwareEx (tUcanHandle* pUcanHandle_p, byte bDeviceNr_p, tCallbackFktEx fpCallbackFktEx_p, void* pCallbackArg_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanInitHardwareEx(ref byte pUcanHandle_p, byte bDeviceNr_p, [MarshalAs(UnmanagedType.FunctionPtr)] tCallbackFktEx fpCallbackFktEx_p, IntPtr pCallbackArg_p);


        /// <summary> UCANRET PUBLIC UcanGetModuleTime (tUcanHandle UcanHandle_p, DWORD* pdwTime_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetModuleTime(byte UcanHandle_p, ref uint pdwTime_p);


        /// <summary> byte PUBLIC UcanGetHardwareInfoEx2 (tUcanHandle UcanHandle_p, tUcanHardwareInfoEx* pHwInfo_p, tUcanChannelInfo* pCanInfoCh0_p, tUcanChannelInfo* pCanInfoCh1_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetHardwareInfoEx2(byte UcanHandle_p, ref tUcanHardwareInfoEx pHwInfo_p, ref tUcanChannelInfo pCanInfoCh0_p, ref tUcanChannelInfo pCanInfoCh1_p);


        /// <summary> byte PUBLIC UcanInitCanEx2 (tUcanHandle UcanHandle_p, byte bChannel_p, tUcanInitCanParam* pInitCanParam_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanInitCanEx2(byte UcanHandle_p, UsbCanChannel bChannel_p, ref tUcanInitCanParam pInitCanParam_p);


        /// <summary> byte PUBLIC UcanSetBaudrateEx (tUcanHandle UcanHandle_p, byte bChannel_p, byte bBTR0_p, byte bBTR1_p, DWORD dwBaudrate_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanSetBaudrateEx(byte UcanHandle_p, UsbCanChannel bChannel_p, byte bBTR0_p, byte bBTR1_p, uint dwBaudrate_p);


        /// <summary> byte PUBLIC UcanSetAcceptanceEx (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD dwAMR_p, DWORD dwACR_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanSetAcceptanceEx(byte UcanHandle_p, UsbCanChannel bChannel_p, uint dwAMR_p, uint dwACR_p);


        /// <summary> byte PUBLIC UcanResetCanEx (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD dwResetFlags_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanResetCanEx(byte UcanHandle_p, UsbCanChannel bChannel_p, uint dwResetFlags_p);


        /// <summary> byte PUBLIC UcanReadCanMsgEx (tUcanHandle UcanHandle_p, byte* pbChannel_p, tCanMsgStruct* pCanMsg_p, DWORD* pdwCount_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanReadCanMsgEx(byte UcanHandle_p, ref UsbCanChannel pbChannel_p, [Out] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] CanMessage[] pCanMsg_p,
                                                           ref uint pdwCount_p);


        /// <summary> byte PUBLIC UcanWriteCanMsgEx (tUcanHandle UcanHandle_p, byte bChannel_p, tCanMsgStruct* pCanMsg_p, DWORD* pdwCount_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanWriteCanMsgEx(byte UcanHandle_p, UsbCanChannel bChannel_p, [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] CanMessage[] pCanMsg_p,
                                                            ref uint pdwCount_p);


        /// <summary>   byte PUBLIC UcanGetStatusEx (tUcanHandle UcanHandle_p, byte bChannel_p, tStatusStruct* pStatus_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetStatusEx(byte UcanHandle_p, UsbCanChannel bChannel_p, ref tStatusStruct pStatus_p);


        /// <summary> byte PUBLIC UcanGetMsgCountInfoEx (tUcanHandle UcanHandle_p, byte bChannel_p, tUcanMsgCountInfo* pMsgCountInfo_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetMsgCountInfoEx(byte UcanHandle_p, UsbCanChannel bChannel_p, ref tUcanMsgCountInfo pMsgCountInfo_p);


        /// <summary> UCANRET PUBLIC UcanGetMsgPending (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD dwFlags_p, DWORD* pdwPendingCount_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetMsgPending(byte UcanHandle_p, UsbCanChannel bChannel_p, uint dwFlags_p, ref uint pdwPendingCount_p);


        /// <summary> UCANRET PUBLIC UcanGetCanErrorCounter (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD* pdwTxErrorCounter_p, DWORD* pdwRxErrorCounter_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanGetCanErrorCounter(byte UcanHandle_p, UsbCanChannel bChannel_p, ref uint pdwTxErrorCounter_p, ref uint pdwRxErrorCounter_p);


        /// <summary> UCANRET PUBLIC UcanSetTxTimeout (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD dwTxTimeout_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanSetTxTimeout(byte UcanHandle_p, UsbCanChannel bChannel_p, uint dwTxTimeout_p);


        /// <summary> byte PUBLIC UcanDeinitCanEx (tUcanHandle UcanHandle_p, byte bChannel_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanDeinitCanEx(byte UcanHandle_p, UsbCanChannel bChannel_p);


        /// <summary> byte PUBLIC UcanDeinitHardware (tUcanHandle UcanHandle_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanDeinitHardware(byte UcanHandle_p);


        /// <summary> UCANRET PUBLIC UcanDefineCyclicCanMsg (tUcanHandle UcanHandle_p, byte bChannel_p, tCanMsgStruct* pCanMsgList_p, DWORD dwCount_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanDefineCyclicCanMsg(byte UcanHandle_p, UsbCanChannel bChannel_p, [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 16)] CanMessage[] pCanMsgList_p,
                                                                 uint dwCount_p);


        /// <summary> UCANRET PUBLIC UcanReadCyclicCanMsg (tUcanHandle UcanHandle_p, byte bChannel_p, tCanMsgStruct* pCanMsgList_p, DWORD* pdwCount_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanReadCyclicCanMsg(byte UcanHandle_p, UsbCanChannel bChannel_p, [Out] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 16)] CanMessage[] pCanMsgList_p,
                                                               ref uint pdwCount_p);


        /// <summary> UCANRET PUBLIC UcanEnableCyclicCanMsg (tUcanHandle UcanHandle_p, byte bChannel_p, DWORD dwFlags_p); </summary>
        [DllImport(CanDLL)]
        public static extern UsbCanReturn UcanEnableCyclicCanMsg(byte UcanHandle_p, UsbCanChannel bChannel_p, eUcanCyclicFlags dwFlags_p);
    }

}