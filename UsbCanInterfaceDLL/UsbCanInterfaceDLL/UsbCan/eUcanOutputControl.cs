﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies pre-defined values for the Output Control Register of SJA1000 on GW-001 and GW-002.
    ///     These values are only important for GW-001 and GW-002.
    ///     They does not have an effect on sysWORXX USB-CANmoduls.
    /// </summary>
    internal enum eUcanOutputControl : byte {
        /// <summary>default OCR value for the standard USB-CANmodul GW-001/GW-002</summary>
        USBCAN_OCR_DEFAULT = 0x1a,

        /// <summary>OCR value for RS485 interface and galvanic isolation</summary>
        USBCAN_OCR_RS485_ISOLATED = 0x1e,

        /// <summary>OCR value for RS485 interface but without galvanic isolation</summary>
        USBCAN_OCR_RS485_NOT_ISOLATED = 0xa
    }

}