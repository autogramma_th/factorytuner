﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies flags for resetting USB-CANmodul with methode <see cref="UsbCanServer.ResetCan" />.
    ///     These flags can be used in combination.
    /// </summary>
    internal enum eUcanResetFlags : uint {
        /// <summary>reset everything</summary>
        USBCAN_RESET_ALL = 0x0,

        /// <summary>no CAN status reset (only supported for sysWORXX USB-CANmoduls)</summary>
        USBCAN_RESET_NO_STATUS = 0x1,

        /// <summary>no CAN controller reset</summary>
        USBCAN_RESET_NO_CANCTRL = 0x2,

        /// <summary>no transmit message counter reset</summary>
        USBCAN_RESET_NO_TXCOUNTER = 0x4,

        /// <summary>no receive message counter reset</summary>
        USBCAN_RESET_NO_RXCOUNTER = 0x8,

        /// <summary>no transmit message buffer reset at channel level</summary>
        USBCAN_RESET_NO_TXBUFFER_CH = 0x10,

        /// <summary>no transmit message buffer reset at USB-CAN-library level</summary>
        USBCAN_RESET_NO_TXBUFFER_DLL = 0x20,

        /// <summary>no transmit message buffer reset at firmware level</summary>
        USBCAN_RESET_NO_TXBUFFER_FW = 0x80,

        /// <summary>no receive message buffer reset at channel level</summary>
        USBCAN_RESET_NO_RXBUFFER_CH = 0x100,

        /// <summary>no receive message buffer reset at USB-CAN-library level</summary>
        USBCAN_RESET_NO_RXBUFFER_DLL = 0x200,

        /// <summary>no receive message buffer reset at kernel driver level</summary>
        USBCAN_RESET_NO_RXBUFFER_SYS = 0x400,

        /// <summary>no receive message buffer reset at firmware level</summary>
        USBCAN_RESET_NO_RXBUFFER_FW = 0x800,

        /// <summary>complete firmware reset (module will automatically reconnect at USB port in 500msec)</summary>
        USBCAN_RESET_FIRMWARE = 0xffffffff,

        /// <summary>no reset of all message counters</summary>
        USBCAN_RESET_NO_COUNTER_ALL = (USBCAN_RESET_NO_TXCOUNTER | USBCAN_RESET_NO_RXCOUNTER),

        /// <summary>no reset of transmit message buffers at communication level (firmware, kernel and library)</summary>
        USBCAN_RESET_NO_TXBUFFER_COMM = (USBCAN_RESET_NO_TXBUFFER_DLL | 0x40 | USBCAN_RESET_NO_TXBUFFER_FW),

        /// <summary>no reset of receive message buffers at communication level (firmware, kernel and library)</summary>
        USBCAN_RESET_NO_RXBUFFER_COMM = (USBCAN_RESET_NO_RXBUFFER_DLL | USBCAN_RESET_NO_RXBUFFER_SYS | USBCAN_RESET_NO_RXBUFFER_FW),

        /// <summary>no reset of all transmit message buffers</summary>
        USBCAN_RESET_NO_TXBUFFER_ALL = (USBCAN_RESET_NO_TXBUFFER_CH | USBCAN_RESET_NO_TXBUFFER_COMM),

        /// <summary>no reset of all receive message buffers</summary>
        USBCAN_RESET_NO_RXBUFFER_ALL = (USBCAN_RESET_NO_RXBUFFER_CH | USBCAN_RESET_NO_RXBUFFER_COMM),

        /// <summary>no reset of all message buffers at communication level (firmware, kernel and library)</summary>
        USBCAN_RESET_NO_BUFFER_COMM = (USBCAN_RESET_NO_TXBUFFER_COMM | USBCAN_RESET_NO_RXBUFFER_COMM),

        /// <summary>no reset of all message buffers</summary>
        USBCAN_RESET_NO_BUFFER_ALL = (USBCAN_RESET_NO_TXBUFFER_ALL | USBCAN_RESET_NO_RXBUFFER_ALL),

        /// <summary>reset of the CAN status only</summary>
        USBCAN_RESET_ONLY_STATUS = (0xffff & ~(USBCAN_RESET_NO_STATUS)),

        /// <summary>reset of the CAN controller only</summary>
        USBCAN_RESET_ONLY_CANCTRL = (0xffff & ~(USBCAN_RESET_NO_CANCTRL)),

        /// <summary>reset of the transmit buffer in firmware only</summary>
        USBCAN_RESET_ONLY_TXBUFFER_FW = (0xffff & ~(USBCAN_RESET_NO_TXBUFFER_FW)),

        /// <summary>reset of the receive buffer in firmware only</summary>
        USBCAN_RESET_ONLY_RXBUFFER_FW = (0xffff & ~(USBCAN_RESET_NO_RXBUFFER_FW)),

        /// <summary>reset of the specified channel of the receive buffer only</summary>
        USBCAN_RESET_ONLY_RXCHANNEL_BUFF = (0xffff & ~(USBCAN_RESET_NO_RXBUFFER_CH)),

        /// <summary>reset of the specified channel of the transmit buffer only</summary>
        USBCAN_RESET_ONLY_TXCHANNEL_BUFF = (0xffff & ~(USBCAN_RESET_NO_TXBUFFER_CH)),

        /// <summary>reset of the receive buffer and receive message counter only</summary>
        USBCAN_RESET_ONLY_RX_BUFF = (0xffff & ~(USBCAN_RESET_NO_RXBUFFER_ALL | USBCAN_RESET_NO_RXCOUNTER)),

        /// <summary>reset of the receive buffer and receive message counter (for GW-002) only</summary>
        USBCAN_RESET_ONLY_RX_BUFF_GW002 = (0xffff & ~(USBCAN_RESET_NO_RXBUFFER_ALL | USBCAN_RESET_NO_RXCOUNTER | USBCAN_RESET_NO_TXBUFFER_FW)),

        /// <summary>reset of the transmit buffer and transmit message counter only</summary>
        USBCAN_RESET_ONLY_TX_BUFF = (0xffff & ~(USBCAN_RESET_NO_TXBUFFER_ALL | USBCAN_RESET_NO_TXCOUNTER)),

        /// <summary>reset of all buffers and all message counters only</summary>
        USBCAN_RESET_ONLY_ALL_BUFF = (USBCAN_RESET_ONLY_RX_BUFF & USBCAN_RESET_ONLY_TX_BUFF),

        /// <summary>reset of all message counters only</summary>
        USBCAN_RESET_ONLY_ALL_COUNTER = (0xffff & ~(USBCAN_RESET_NO_COUNTER_ALL))
    }

}