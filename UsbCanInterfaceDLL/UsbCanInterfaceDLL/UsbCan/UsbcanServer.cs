﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;


namespace Asap1B.UsbCan {

    /// <summary>
    ///     USBcanServer is a wrapper class for using the USBCAN64.DLL with .NET programming languages
    ///     such as Visual Basic .NET, Managed C++ and C#.
    /// </summary>
    internal sealed class UsbCanServer : IDisposable {
        public delegate void CanMsgReceivedEventEventHandler(byte bDeviceNr_p, UsbCanChannel bChannel_p);

        public delegate void ConnectEventEventHandler();

        public delegate void DeinitCanEventEventHandler(byte bDeviceNr_p, UsbCanChannel bChannel_p);

        public delegate void DeinitHwEventEventHandler(byte bDeviceNr_p);

        public delegate void DisconnectEventEventHandler();

        public delegate void FatalDisconnectEventEventHandler(byte bDeviceNr_p);

        public delegate void InitCanEventEventHandler(byte bDeviceNr_p, UsbCanChannel bChannel_p);

        public delegate void InitHwEventEventHandler();

        public delegate void StatusEventEventHandler(byte bDeviceNr_p, UsbCanChannel bChannel_p);

        private static readonly List<UsbCanServer> ColUcanHandleL = new List<UsbCanServer>();

        // count running object instances
        private static int _dwPlugAndPlayCountL;

        // delegate of ConnectControl callback function
        private static readonly Unmanaged.tConnectControlFktEx PfnUcanConnectControlL = UcanConnectControl;

        // protect delegate from being destroyed by the garbage collector
        private static GCHandle _gchUcanConnectControlL;

        private readonly Unmanaged.tCallbackFktEx _pfnUcanCallback;

        private byte _bDeviceNr = Constants.USBCAN_ANY_MODULE;

        private bool _disposed;

        private bool _fCh0IsInitialized;

        private bool _fCh1IsInitialized;

        private bool _fHwIsInitialized;

        private bool _fIsInitialized;

        // handle from USBCAN64.DLL
        // protect delegate from being destroyed by garbage collector before called with event DEINITHW
        private GCHandle _gchUcanCallback;

        private byte _ucanHandle;

        public UsbCanServer() {
            _pfnUcanCallback = HandleUcanCallback;

            // if first instance register ConnectControll callback function
            if ((_dwPlugAndPlayCountL == 0)) {
                _gchUcanConnectControlL = GCHandle.Alloc(PfnUcanConnectControlL, GCHandleType.Normal);
                Unmanaged.UcanInitHwConnectControlEx(PfnUcanConnectControlL, IntPtr.Zero);
            }

            _dwPlugAndPlayCountL += 1;
        }

        /// <summary>
        ///     Returnes whether hardware interface is initialsized.
        /// </summary>
        /// <value></value>
        /// <returns>True if initialized, otherwise False</returns>
        public bool IsHardwareInitialized {
            get { return _fHwIsInitialized; }
        }


        /// <summary>
        ///     Returnes whether CAN interface for channel 0 is initialsized.
        /// </summary>
        /// <value></value>
        /// <returns>True if initialized, otherwise False</returns>
        public bool IsCan0Initialized {
            get { return _fCh0IsInitialized; }
        }


        /// <summary>
        ///     Returnes whether CAN interface for channel 1 is initialsized.
        /// </summary>
        /// <value></value>
        /// <returns>True if initialized, otherwise False</returns>
        public bool IsCan1Initialized {
            get { return _fCh1IsInitialized; }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Event occurs when an USB-CANmodul has been initialized (see methode <see cref="InitHardware" />).
        /// </summary>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event InitHwEventEventHandler InitHwEvent;

        /// <summary>Event occurs when a CAN interface of an USB-CANmodul has been initialized.</summary>
        /// <param name="bChannel_p">
        ///     Specifies the CAN channel which was initialized (see methode <see cref="InitCan" />).
        /// </param>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event InitCanEventEventHandler InitCanEvent;

        /// <summary>Event occurs when at leas one CAN message has been received.</summary>
        /// <param name="bChannel_p">Specifies the CAN channel which received CAN messages.</param>
        /// <remarks>
        ///     <para>
        ///         Call the methode <see cref="ReadCanMsg" /> to receive the CAN messages.
        ///     </para>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event CanMsgReceivedEventEventHandler CanMsgReceivedEvent;

        /// <summary>Event occurs when the error status of a module has been changed.</summary>
        /// <param name="bChannel_p">Specifies the CAN channel which status has been changed.</param>
        /// <remarks>
        ///     <para>
        ///         Call the methode <see cref="GetStatus" /> to receive the error status.
        ///     </para>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event StatusEventEventHandler StatusEvent;

        /// <summary>
        ///     Event occurs when a CAN interface has been deinitialized (see methode <see cref="Shutdown" />).
        /// </summary>
        /// <param name="bChannel_p">Specifies the CAN channel which status has been changed.</param>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event DeinitCanEventEventHandler DeinitCanEvent;

        /// <summary>
        ///     Event occurs when an USB-CANmodul has been deinitialized (see methode <see cref="Shutdown" />).
        /// </summary>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event DeinitHwEventEventHandler DeinitHwEvent;

        /// <summary>Event occurs when a new USB-CANmodul has been connected to the host.</summary>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public static event ConnectEventEventHandler ConnectEvent;

        /// <summary>Event occurs when an USB-CANmodul has been disconnected from the host.</summary>
        /// <remarks>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public static event DisconnectEventEventHandler DisconnectEvent;

        /// <summary>Event occurs when an USB-CANmodul has been disconnected from the host which was currently initialized.</summary>
        /// <remarks>
        ///     <para>No methode can be called for this module.</para>
        ///     <para>Do not call any class methode within an event handler to prevent dead locks.</para>
        /// </remarks>
        public event FatalDisconnectEventEventHandler FatalDisconnectEvent;


        /// <summary>
        ///     Initializes the device with the corresponding device number.
        /// </summary>
        /// <param name="deviceNumber">
        ///     [IN] device number (0 – 254, or <see cref="Constants.USBCAN_ANY_MODULE" /> for the first device)
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>
        ///     The event <see cref="InitHwEvent" /> will occur on success.
        /// </remarks>
        public UsbCanReturn InitHardware(byte deviceNumber) {
            var bRet = UsbCanReturn.Successful;

            // check if already initialized

            if ((_fHwIsInitialized == false)) {
                // initialize hardware
                _gchUcanCallback = GCHandle.Alloc(_pfnUcanCallback, GCHandleType.Normal);

                try {
                    bRet = Unmanaged.UcanInitHardwareEx(ref _ucanHandle, deviceNumber, _pfnUcanCallback, IntPtr.Zero);
                }
                catch (SEHException) {
                    // sometimes this exception occures in UcanInitHardwareEx:
                    // System.Runtime.InteropServices.SEHException: Eine externe Komponente hat eine Ausnahme ausgelцst.
                    bRet = UsbCanReturn.Reserved;
                    // &HC0
                }
                catch (NullReferenceException) {
                    // this exception should never occur anymore
                    // bug in USBCAN64.DLL is solved where pUcanEntry->m_fpCallbackFktEx contains an old and wrong value in UcanInitHardware()
                    _fHwIsInitialized = true;
                    bRet = UsbCanReturn.Reserved + 1;
                    // &HC1
                }
                if ((bRet == UsbCanReturn.Successful)) {
                    tUcanHardwareInfoEx hwInfo = default(tUcanHardwareInfoEx);
                    tUcanChannelInfo canInfoCh0 = default(tUcanChannelInfo);
                    tUcanChannelInfo canInfoCh1 = default(tUcanChannelInfo);

                    bRet = GetHardwareInfo(ref hwInfo, ref canInfoCh0, ref canInfoCh1);
                    _bDeviceNr = hwInfo.m_bDeviceNr;

                    _fHwIsInitialized = true;
                    // remember this object instance in the static collection
                    lock ((ColUcanHandleL)) {
                        ColUcanHandleL.Add(this);
                    }
                }
                else {
                    _gchUcanCallback.Free();
                }
            }

            return bRet;
        }


        /// <summary>
        ///     Initializes a specific CAN channel of a device.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be initialized (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="wBTR_p">
        ///     [IN] Baud rate register BTR0 as high byte, baud rate register BTR1 as low byte (see enum <see cref="UsbCanBaudrate" />).
        /// </param>
        /// <param name="dwBaudrate_p">
        ///     [IN] Baud rate register for all sysWORXX USB-CANmoduls (see enum <see cref="UsbCanBaudrateEx" />).
        /// </param>
        /// <param name="dwAMR_p">
        ///     [IN] Acceptance filter mask (see methode <see cref="SetAcceptance" />). <see cref="Constants.USBCAN_AMR_ALL" />
        /// </param>
        /// <param name="dwACR_p">
        ///     [IN] Acceptance filter code (see methode <see cref="SetAcceptance" />). <see cref="Constants.USBCAN_ACR_ALL" />
        /// </param>
        /// <param name="bMode_p">
        ///     [IN] Transmission mode of CAN channel (see enum <see cref="tUcanMode" />). <see cref="tUcanMode.kUcanModeNormal" />
        /// </param>
        /// <param name="bOCR_p">
        ///     [IN] Output Control Register (see enum <see cref="eUcanOutputControl" />).
        ///     <see
        ///         cref="eUcanOutputControl.USBCAN_OCR_DEFAULT" />
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>
        ///     <para>With GW-001, GW-002 and USB-CANmodul1 only channel 0 is available.</para>
        ///     <para>
        ///         The event <see cref="InitCanEvent" /> will occur on success.
        ///     </para>
        /// </remarks>
        public UsbCanReturn InitCan(UsbCanChannel bChannel_p, UsbCanBaudrate wBTR_p, UsbCanBaudrateEx dwBaudrate_p, uint dwAMR_p, uint dwACR_p, tUcanMode bMode_p, eUcanOutputControl bOCR_p) {
            var bRet = UsbCanReturn.Successful;
            var initParam = default(tUcanInitCanParam);

            // check if specified CAN channel was already initialized

            if (((bChannel_p == UsbCanChannel.Ch0) && (_fCh0IsInitialized == false))
                || ((bChannel_p == UsbCanChannel.Ch1) && (_fCh1IsInitialized == false))) {
                // fill out initialisation struct
                initParam.m_dwSize = Marshal.SizeOf(initParam);
                // size of this struct
                initParam.m_bMode = bMode_p;
                // normal operation mode
                initParam.m_bBTR0 = (byte) (((ushort) wBTR_p >> 8) & 0xff);
                // baudrate
                initParam.m_bBTR1 = (byte) ((ushort) wBTR_p & 0xff);
                // baudrate
                initParam.m_bOCR = (byte) bOCR_p;
                // standard output
                initParam.m_dwAMR = dwAMR_p;
                // CAN message filter
                initParam.m_dwACR = dwACR_p;
                initParam.m_dwBaudrate = (uint) dwBaudrate_p;
                // baudrate Ex
                initParam.m_wNrOfRxBufferEntries = Constants.USBCAN_DEFAULT_BUFFER_ENTRIES;
                initParam.m_wNrOfTxBufferEntries = Constants.USBCAN_DEFAULT_BUFFER_ENTRIES;

                // initialize CAN interface
                bRet = Unmanaged.UcanInitCanEx2(_ucanHandle, bChannel_p, ref initParam);

                if ((bRet == UsbCanReturn.Successful)) {
                    // remember the successful init
                    if ((bChannel_p == UsbCanChannel.Ch0)) {
                        _fCh0IsInitialized = true;
                    }
                    else {
                        _fCh1IsInitialized = true;
                    }

                    // initialisation complete
                    _fIsInitialized = true;
                }
            }

            return bRet;
        }


        /// <summary>
        ///     Reads one or more CAN-messages from the buffer of the specified CAN channel.
        /// </summary>
        /// <param name="channel">
        ///     [IN/OUT] Reference to a variable containing the CAN-Channel to read from
        ///     (<see cref="UsbCanChannel.Ch0" />, <see cref="UsbCanChannel.Ch1" /> or <see cref="UsbCanChannel.Any" />).
        ///     When USBCAN_CHANNEL_ANY was specified this referenced variable receives the CAN channel where the read CAN messages came from.
        /// </param>
        /// <param name="msgs">
        ///     [OUT] Array of several CAN message structure (see structure <see cref="CanMessage" />). This address must not be NULL.
        /// </param>
        /// <param name="readCount">[OUT] Reference to a variable that receives the number of read CAN messages.</param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>This function writes the actual number of CAN messages that were read from the device to the referenced variable <paramref name="readCount"/>.</remarks>
        public UsbCanReturn ReadCanMsg(UsbCanChannel channel, CanMessage[] msgs, out uint readCount) {
            // set message counter to actual array length
            var count = (uint)msgs.Length;

            // call unmanaged function
            var bRet = Unmanaged.UcanReadCanMsgEx(_ucanHandle, ref channel, msgs, ref count);
            Debug.WriteLine("ReadCanMsg: count = " + count);

            readCount = count;
            return bRet;
        }


        /// <summary>
        ///     Transmits one ore more CAN messages through the specified CAN channel of the device.
        /// </summary>
        /// <param name="channel">
        ///     [IN] CAN-Channel, which is to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="msgs">
        ///     [IN] Array of CAN message structure (see structure <see cref="CanMessage" />).
        /// </param>
        /// <param name="transmittedCount">[OUT] Reference to a variable for receiving the number of successfuly transmitted CAN messages.</param>
        /// <returns>Error code of the function.</returns>
        /// <seealso cref="CanMessage" />
        public UsbCanReturn WriteCanMsg(UsbCanChannel channel, CanMessage[] msgs, out uint transmittedCount) {
            // set message counter to actual array length
            var count = (uint) msgs.Length;

            // call unmanaged function
            var bRet = Unmanaged.UcanWriteCanMsgEx(_ucanHandle, channel, msgs, ref count);

            transmittedCount = count;
            return bRet;
        }


        /// <summary>
        ///     This function is used to configure the baud rate of specific CAN channel of a device.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, which is to be configured (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="wBTR_p">
        ///     [IN] Baud rate register BTR0 as high byte, baud rate register BTR1 as low byte (see enum <see cref="UsbCanBaudrate" />).
        /// </param>
        /// <param name="dwBaudrate_p">
        ///     [IN] Baud rate register for all sysWORXX USB-CANmoduls (see enum <see cref="UsbCanBaudrateEx" />).
        ///     <see
        ///         cref="UsbCanBaudrateEx.BaudexUseBtr01" />
        /// </param>
        /// <returns>Error code of the function.</returns>
        public UsbCanReturn SetBaudrate(UsbCanChannel bChannel_p, UsbCanBaudrate wBTR_p, UsbCanBaudrateEx dwBaudrate_p) {
            // call unmanaged function
            return Unmanaged.UcanSetBaudrateEx(_ucanHandle, bChannel_p, (byte) (((short) wBTR_p >> 8) & 0xff), (byte) ((short) wBTR_p & 0xff), (uint) dwBaudrate_p);
        }


        /// <summary>
        ///     This function is used to change the acceptance filter values for a specific CAN channel on a device.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, which is to be configured (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="dwAMR_p">
        ///     [IN] Acceptance filter mask (AMR). <see cref="Constants.USBCAN_AMR_ALL" />
        /// </param>
        /// <param name="dwACR_p">
        ///     [IN] Acceptance filter code (ACR). <see cref="Constants.USBCAN_ACR_ALL" />
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>
        ///     <para>The format of the acceptance filter mask and code is regarding the SJA1000 CAN controller.</para>
        ///     <para>For standard CAN messages (CAN2.0A means 11 bit identifier):</para>
        ///     <para>
        ///         <list type="table">
        ///             <listheader>
        ///                 <term>bits of AMR/ACR</term><description>description</description>
        ///             </listheader>
        ///             <item>
        ///                 <term>0-7:</term><description>Second data byte of the CAN message.</description>
        ///             </item>
        ///             <item>
        ///                 <term>8-15:</term><description>First data byte of the CAN message.</description>
        ///             </item>
        ///             <item>
        ///                 <term>16-19:</term><description>unused</description>
        ///             </item>
        ///             <item>
        ///                 <term>20:</term><description>RTR</description>
        ///             </item>
        ///             <item>
        ///                 <term>21-31:</term><description>11 bit CAN identifier</description>
        ///             </item>
        ///         </list>
        ///     </para>
        ///     <para>For extended CAN messages (CAN2.0B means 29 bit identifier):</para>
        ///     <para>
        ///         <list type="table">
        ///             <listheader>
        ///                 <term>bits of AMR/ACR</term><description>description</description>
        ///             </listheader>
        ///             <item>
        ///                 <term>0-1:</term><description>unused</description>
        ///             </item>
        ///             <item>
        ///                 <term>2:</term><description>RTR</description>
        ///             </item>
        ///             <item>
        ///                 <term>3-31:</term><description>29 bit CAN identifier</description>
        ///             </item>
        ///         </list>
        ///     </para>
        ///     <para>
        ///         The CAN message will be received when the term <c>(CAN-ID xor ACR) and AMR</c> results the value <c>0xFFFFFFFF</c>.
        ///     </para>
        ///     <para>
        ///         The values <see cref="Constants.USBCAN_AMR_ALL" /> and <see cref="Constants.USBCAN_ACR_ALL" /> will receive all CAN messages.
        ///     </para>
        /// </remarks>
        public UsbCanReturn SetAcceptance(UsbCanChannel bChannel_p, uint dwAMR_p, uint dwACR_p) {
            // call unmanaged function
            return Unmanaged.UcanSetAcceptanceEx(_ucanHandle, bChannel_p, dwAMR_p, dwACR_p);
        }


        /// <summary>
        ///     Returns the error status of a specific CAN channel.
        /// </summary>
        /// <param name="pbChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="pStatus_p">
        ///     [OUT] Reference to Status structure (see structure <see cref="tStatusStruct" />).
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <seealso cref="GetCanStatusMessage" />
        /// <seealso cref="tStatusStruct" />
        /// <seealso cref="UsbCanCanStatus" />
        /// <seealso cref="eUcanUsbStatus" />
        public UsbCanReturn GetStatus(UsbCanChannel pbChannel_p, ref tStatusStruct pStatus_p) {
            // call unmanaged function
            var bRet = Unmanaged.UcanGetStatusEx(_ucanHandle, pbChannel_p, ref pStatus_p);
            return bRet;
        }


        /// <summary>
        ///     Reads the message counters of the specified CAN channel.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, which is to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="wRecvdMsgCount_p">[OUT] Number of CAN messages received.</param>
        /// <param name="wSentMsgCount_p">[OUT] Number of CAN messages sent.</param>
        /// <returns>Error code of the function.</returns>
        public UsbCanReturn GetMsgCountInfo(UsbCanChannel bChannel_p, ref short wRecvdMsgCount_p, ref short wSentMsgCount_p) {
            tUcanMsgCountInfo msgCountInfo = default(tUcanMsgCountInfo);

            // call unmanaged function
            var bRet = Unmanaged.UcanGetMsgCountInfoEx(_ucanHandle, bChannel_p, ref msgCountInfo);

            // copy the result to the output parameters
            wRecvdMsgCount_p = msgCountInfo.m_wRecvdMsgCount;
            wSentMsgCount_p = msgCountInfo.m_wSentMsgCount;

            return bRet;
        }


        /// <summary>
        ///     Resets a CAN channel of a device (hardware reset, empty buffer, and so on).
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be reset (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="dwFlags_p">
        ///     [IN] Flags defines what should be reset (see enum <see cref="eUcanResetFlags" />).
        ///     <see
        ///         cref="eUcanResetFlags.USBCAN_RESET_ALL" />
        /// </param>
        /// <returns>Error code of the function.</returns>
        public UsbCanReturn ResetCan(UsbCanChannel bChannel_p, uint dwFlags_p) {
            // call unmanaged function
            var bRet = Unmanaged.UcanResetCanEx(_ucanHandle, bChannel_p, dwFlags_p);
            return bRet;
        }


        /// <summary>
        ///     Returns the extended hardware information of a device. With multi-channel
        ///     USB-CANmoduls the information for both CAN channels are returned separately.
        /// </summary>
        /// <param name="pHwInfo_p">
        ///     [OUT] Reference to structure where the extended hardware
        ///     information is to be stored (see structure <see cref="tUcanHardwareInfoEx" />).
        /// </param>
        /// <param name="pCanInfoCh0_p">
        ///     [OUT] Reference to structure where the information of CAN channel 0
        ///     is to be stored (see structure <see cref="tUcanChannelInfo" />).
        /// </param>
        /// <param name="pCanInfoCh1_p">
        ///     [OUT] Reference to structure where the information of CAN channel 1
        ///     is to be stored (see structure <see cref="tUcanChannelInfo" />).
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <seealso cref="tUcanHardwareInfoEx" />
        /// <seealso cref="tUcanChannelInfo" />
        public UsbCanReturn GetHardwareInfo(ref tUcanHardwareInfoEx pHwInfo_p, ref tUcanChannelInfo pCanInfoCh0_p, ref tUcanChannelInfo pCanInfoCh1_p) {
            // set the structure sizes to the actual sizes of the unmanaged data
            pHwInfo_p.m_dwSize = Marshal.SizeOf(pHwInfo_p);

            pCanInfoCh0_p.m_dwSize = Marshal.SizeOf(pCanInfoCh0_p);
            pCanInfoCh1_p.m_dwSize = Marshal.SizeOf(pCanInfoCh1_p);

            // call unmanaged function
            var bRet = Unmanaged.UcanGetHardwareInfoEx2(_ucanHandle, ref pHwInfo_p, ref pCanInfoCh0_p, ref pCanInfoCh1_p);
            return bRet;
        }


        /// <summary>
        ///     Returns the firmware version number of the device.
        /// </summary>
        /// <returns>
        ///     firmware version number
        ///     <list type="table">
        ///         <listheader>
        ///             <term>bits</term><description>description</description>
        ///         </listheader>
        ///         <item>
        ///             <term>0-7:</term><description>major version</description>
        ///         </item>
        ///         <item>
        ///             <term>8-15:</term><description>minor version</description>
        ///         </item>
        ///         <item>
        ///             <term>16-31:</term><description>release version</description>
        ///         </item>
        ///     </list>
        /// </returns>
        public int GetFwVersion() {
            // call unmanaged function
            return Unmanaged.UcanGetFwVersion(_ucanHandle);
        }


        /// <summary>
        ///     Defines a list of CAN messages for automatic transmission.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="pCanMsgList_p">
        ///     [IN] Array of CAN messages (up to 16, see structure <see cref="CanMessage" />), or nothing to delete an older list.
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>When this function is called an older list will be deleted.</remarks>
        /// <seealso cref="CanMessage" />
        public UsbCanReturn DefineCyclicCanMsg(UsbCanChannel bChannel_p, CanMessage[] pCanMsgList_p) {
            int dwCount = 0;

            if ((pCanMsgList_p != null)) {
                // set message counter to actual array length
                dwCount = pCanMsgList_p.Length;
            }

            // call unmanaged function
            var bRet = Unmanaged.UcanDefineCyclicCanMsg(_ucanHandle, bChannel_p, pCanMsgList_p, (uint) dwCount);
            return bRet;
        }


        /// <summary>
        ///     Reads back the list of CAN messages for automatically sending.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or <see  cref="UsbCanChannel.Ch1" />).
        /// </param>
        /// <param name="pCanMsgList_p">
        ///     [OUT] Array of CAN messages (up to 16, see structure <see cref="CanMessage" />) for receiving the cyclic CAN messages.
        /// </param>
        /// <param name="pdwCount_p">[OUT] Reference to a variable to receive the number of cyclic CAN messages.</param>
        /// <returns>Error code of the function.</returns>
        /// <seealso cref="CanMessage" />
        public UsbCanReturn ReadCyclicCanMsg(UsbCanChannel bChannel_p, ref CanMessage[] pCanMsgList_p, ref uint pdwCount_p) {
            // set message counter to actual array length
            pdwCount_p = (uint) pCanMsgList_p.Length;

            // call unmanaged function
            var bRet = Unmanaged.UcanReadCyclicCanMsg(_ucanHandle, bChannel_p, pCanMsgList_p, ref pdwCount_p);
            Debug.WriteLine("ReadCyclicCanMsg: count=" + pdwCount_p.ToString());
            return bRet;
        }

        public override string ToString() {
            return
                string.Format(
                    "PfnUcanCallback: {0}, BDeviceNr: {1}, Disposed: {2}, FCh0IsInitialized: {3}, FCh1IsInitialized: {4}, FHwIsInitialized: {5}, FIsInitialized: {6}, GchUcanCallback: {7}, UcanHandle: {8}, IsHardwareInitialized: {9}, IsCan0Initialized: {10}, IsCan1Initialized: {11}",
                    _pfnUcanCallback, _bDeviceNr, _disposed, _fCh0IsInitialized, _fCh1IsInitialized, _fHwIsInitialized,
                    _fIsInitialized, _gchUcanCallback, _ucanHandle, IsHardwareInitialized, IsCan0Initialized,
                    IsCan1Initialized);
        }

        /// <summary>
        ///     Enables or disables the automatically sending.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="dwFlags_p">
        ///     [IN] Flags for enabling or disabling (see enum <see cref="eUcanCyclicFlags" />).
        /// </param>
        /// <returns>Error code of the function.</returns>
        /// <seealso cref="eUcanCyclicFlags" />
        public UsbCanReturn EnableCyclicCanMsg(UsbCanChannel bChannel_p, eUcanCyclicFlags dwFlags_p) {
            // call unmanaged function
            return Unmanaged.UcanEnableCyclicCanMsg(_ucanHandle, bChannel_p, dwFlags_p);
        }


        /// <summary>
        ///     Returns the number of pending CAN messages.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="dwFlags_p">
        ///     [IN] Flags specifies which buffers shoulb be checked (see enum <see cref="eUcanPendingFlags" />).
        /// </param>
        /// <param name="pdwPendingCount_p">[OUT] Reference to a variable to receive the number of pending messages.</param>
        /// <returns>Error code of the function.</returns>
        public UsbCanReturn GetMsgPending(UsbCanChannel bChannel_p, uint dwFlags_p, ref uint pdwPendingCount_p) {
            // call unmanaged function
            return Unmanaged.UcanGetMsgPending(_ucanHandle, bChannel_p, dwFlags_p, ref pdwPendingCount_p);
        }


        /// <summary>
        ///     Reads the current value of the error counters within the CAN controller.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="pdwTxErrorCounter_p">[OUT] Reference to a variable to receive the TX error counter.</param>
        /// <param name="pdwRxErrorCounter_p">[OUT] Reference to a variable to receive the RX error counter.</param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>Only available for sysWORXX USB-CANmoduls (NOT for GW-001 and GW-002 !!!).</remarks>
        public UsbCanReturn GetCanErrorCounter(UsbCanChannel bChannel_p, ref uint pdwTxErrorCounter_p, ref uint pdwRxErrorCounter_p) {
            // call unmanaged function
            return Unmanaged.UcanGetCanErrorCounter(_ucanHandle, bChannel_p, ref pdwTxErrorCounter_p, ref pdwRxErrorCounter_p);
        }


        /// <summary>
        ///     Sets the transmission timeout.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" /> or
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     ).
        /// </param>
        /// <param name="dwTxTimeout_p">[IN] transmit timeout in milleseconds (value 0 disables this feature).</param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>
        ///     NOTE: When a transmission timeout is set the firmware tries to send
        ///     a message within this timeout. If it could not be sent the firmware sets
        ///     the "auto delete" state. Within this state all transmit CAN messages for
        ///     this channel will be deleted automatically for not blocking the other
        ///     channel. When firmware does delete a transmit CAN message then a new
        ///     error status will be set: USBCAN_CANERR_TXMSGLOST (red LED is blinking).
        /// </remarks>
        public UsbCanReturn SetTxTimeout(UsbCanChannel bChannel_p, uint dwTxTimeout_p) {
            // call unmanaged function
            return Unmanaged.UcanSetTxTimeout(_ucanHandle, bChannel_p, dwTxTimeout_p);
        }


        /// <summary>
        ///     Shuts down all CAN interfaces and/or the hardware interface.
        /// </summary>
        /// <param name="bChannel_p">
        ///     [IN] CAN channel, to be used (<see cref="UsbCanChannel.Ch0" />,
        ///     <see
        ///         cref="UsbCanChannel.Ch1" />
        ///     or <see cref="UsbCanChannel.All" />).
        /// </param>
        /// <param name="fShutDownHardware_p">[IN] If true then the hardware interface will be closed too. true</param>
        /// <returns>Error code of the function.</returns>
        /// <remarks>
        ///     <para>
        ///         The events <see cref="DeinitCanEvent" /> and <see cref="DeinitHwEvent" /> will occur on success.
        ///     </para>
        /// </remarks>
        public UsbCanReturn Shutdown(UsbCanChannel bChannel_p, bool fShutDownHardware_p) {
            byte bRet = 0;

            lock ((this)) {
                Debug.WriteLine("Shutdown");

                // shutdown each channel if it's initialized
                if (_fCh0IsInitialized && ((bChannel_p == UsbCanChannel.All) || (bChannel_p == UsbCanChannel.Ch0) || fShutDownHardware_p)) {
                    bRet += (byte) Unmanaged.UcanDeinitCanEx(_ucanHandle, UsbCanChannel.Ch0);
                    _fCh0IsInitialized = false;
                }

                if (_fCh1IsInitialized && ((bChannel_p == UsbCanChannel.All) || (bChannel_p == UsbCanChannel.Ch1) || fShutDownHardware_p)) {
                    bRet += (byte) Unmanaged.UcanDeinitCanEx(_ucanHandle, UsbCanChannel.Ch1);
                    _fCh1IsInitialized = false;
                }


                if (_fHwIsInitialized && fShutDownHardware_p) {
                    // sleep 200ms to prevent calling ReadCanMsg() by a pending callback handler
                    Thread.Sleep(200);

                    // shutdown hardware
                    try {
                        bRet += (byte) Unmanaged.UcanDeinitHardware(_ucanHandle);
                        // sometimes the following exception occures:
                        //Eine nicht behandelte Ausnahme des Typs 'System.NullReferenceException' ist in UcanDotNET.dll aufgetreten.
                        //Zusдtzliche Informationen: Der Objektverweis wurde nicht auf eine Objektinstanz festgelegt.
                    }
                    catch (NullReferenceException) {
                        Debug.WriteLine("catched NullReferenceException while executing UcanDeinithardware");
                    }

                    _fHwIsInitialized = false;
                    _ucanHandle = 0;
                }
            }

            return (UsbCanReturn) bRet;
        }


        /// <summary>
        ///     <para>Returns the version number of the USBCAN-library.</para>
        /// </summary>
        /// <returns>
        ///     software version number
        ///     <list type="table">
        ///         <listheader>
        ///             <term>bits</term><description>description</description>
        ///         </listheader>
        ///         <item>
        ///             <term>0-7:</term><description>major version</description>
        ///         </item>
        ///         <item>
        ///             <term>8-15:</term><description>minor version</description>
        ///         </item>
        ///         <item>
        ///             <term>16-31:</term><description>release version</description>
        ///         </item>
        ///     </list>
        /// </returns>
        public static int GetUserDllVersion() {
            // call unmanaged function
            return Unmanaged.UcanGetVersionEx(tUcanVersionType.kVerTypeUserDll);
        }


        /// <summary>
        ///     This function enables the creation of a debug log file out of the USBCAN-library. If this
        ///     feature has already been activated via the USB-CANmodul Control, the content of the
        ///     “old” log file will be copied to the new file. Further debug information will be appended to
        ///     the new file.
        /// </summary>
        /// <param name="dwDbgLevel_p">[IN] debug level (bit format)</param>
        /// <param name="pszFilePathName_p">[IN] file path to debug log file</param>
        /// <param name="dwFlags_p">[IN] additional flags (bit0: file append mode). 0</param>
        /// <returns>FALSE if logfile not created otherwise TRUE</returns>
        public static bool SetDebugMode(int dwDbgLevel_p, string pszFilePathName_p, int dwFlags_p) {
            // call unmanaged function
            return Unmanaged.UcanSetDebugMode(dwDbgLevel_p, pszFilePathName_p, dwFlags_p);
        }


        /// <summary>
        ///     Converts a given CAN status value to the appropriate message string.
        /// </summary>
        /// <param name="wCanCanStatusP">
        ///     [IN] CAN status value from methode <see cref="GetStatus" />) (see enum <see cref="UsbCanCanStatus" />)
        /// </param>
        /// <returns>Status message string.</returns>
        public static string GetCanStatusMessage(UsbCanCanStatus wCanCanStatusP) {
            string strStatus = string.Empty;

            if ((wCanCanStatusP == UsbCanCanStatus.Ok)) {
                strStatus = "OK";
            }
            else {
                if (((wCanCanStatusP & UsbCanCanStatus.TxmsgLost) != 0)) {
                    strStatus += "Transmit message lost,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.MemTest) != 0)) {
                    strStatus += "Memory test failed,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.RegTest) != 0)) {
                    strStatus += "Register test failed,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.QxmtFull) != 0)) {
                    strStatus += "Transmit queue is full,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.Qoverrun) != 0)) {
                    strStatus += "Receive queue overrun,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.QrcvEmpty) != 0)) {
                    strStatus += "Receive queue is empty,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.BusOff) != 0)) {
                    strStatus += "Bus Off,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.BusHeavy) != 0)) {
                    strStatus += "Error Passive,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.BusLight) != 0)) {
                    strStatus += "Warning Limit,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.Overrun) != 0)) {
                    strStatus += "Rx-buffer is full,";
                }
                if (((wCanCanStatusP & UsbCanCanStatus.XmtFull) != 0)) {
                    strStatus += "Tx-buffer is full,";
                }
            }

            return strStatus.TrimEnd(',');
        }


        /// <summary>
        ///     Converts a given baud rate value for GW-001/GW-002 to the appropriate message string.
        /// </summary>
        /// <param name="bBTR0_p">[IN] Bus Timing Register 0 (SJA1000)</param>
        /// <param name="bBTR1_p">[IN] Bus Timing Register 1 (SJA1000)</param>
        /// <returns>Baud rate message string.</returns>
        public static string GetBaudrateMessage(byte bBTR0_p, byte bBTR1_p) {
            short wBTR0 = bBTR0_p;
            short wBTR1 = bBTR1_p;
            short wBTR = (short) ((wBTR0 << 8) + wBTR1);
            return GetBaudrateMessage((UsbCanBaudrate) wBTR);
        }


        /// <summary>
        ///     Converts a given baud rate value for GW-001/GW-002 to the appropriate message string.
        /// </summary>
        /// <param name="wBTR_p">
        ///     [IN] Bus Timing Registers (SJA1000), BTR0 in high order byte and BTR1 in low order byte (see enum
        ///     <see
        ///         cref="UsbCanBaudrate" />
        ///     ).
        /// </param>
        /// <returns>Baud rate message string.</returns>
        public static string GetBaudrateMessage(UsbCanBaudrate wBTR_p) {
            switch (wBTR_p) {
                case UsbCanBaudrate.BaudAuto:
                    return "auto baudrate";
                case UsbCanBaudrate.Baud10KBit:
                    return "10 kBit/sec";
                case UsbCanBaudrate.Baud20KBit:
                    return "20 kBit/sec";
                case UsbCanBaudrate.Baud50KBit:
                    return "50 kBit/sec";
                case UsbCanBaudrate.Baud100KBit:
                    return "100 kBit/sec";
                case UsbCanBaudrate.Baud125KBit:
                    return "125 kBit/sec";
                case UsbCanBaudrate.Baud250KBit:
                    return "250 kBit/sec";
                case UsbCanBaudrate.Baud500KBit:
                    return "500 kBit/sec";
                case UsbCanBaudrate.Baud800KBit:
                    return "800 kBit/sec";
                case UsbCanBaudrate.Baud1MBit:
                    return "1 MBit/s";
                case UsbCanBaudrate.BaudUseBtrex:
                    return "BTR Ext is used";
                default:
                    return "BTR is unknown (userspecific)";
            }
        }


        /// <summary>
        ///     Converts a given baud rate value for sysWORXX USB-CANmoduls to the appropriate message string.
        /// </summary>
        /// <param name="dwBTR_p">
        ///     [IN] Bus Timing Registers (see enum <see cref="UsbCanBaudrateEx" />).
        /// </param>
        /// <returns>Baud rate message string.</returns>
        public static string GetBaudrateExMessage(UsbCanBaudrateEx dwBTR_p) {
            switch (dwBTR_p) {
                case UsbCanBaudrateEx.BaudexAuto:
                    return "auto baudrate";
                case UsbCanBaudrateEx.BaudexSp10KBit:
                case UsbCanBaudrateEx.Baudex10KBit:
                    return "10 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp20KBit:
                case UsbCanBaudrateEx.Baudex20KBit:
                    return "20 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp50KBit:
                case UsbCanBaudrateEx.Baudex50KBit:
                    return "50 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp100KBit:
                case UsbCanBaudrateEx.Baudex100KBit:
                    return "100 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp125KBit:
                case UsbCanBaudrateEx.Baudex125KBit:
                    return "125 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp250KBit:
                case UsbCanBaudrateEx.Baudex250KBit:
                    return "250 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp500KBit:
                case UsbCanBaudrateEx.Baudex500KBit:
                    return "500 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp800KBit:
                case UsbCanBaudrateEx.Baudex800KBit:
                    return "800 kBit/sec";
                case UsbCanBaudrateEx.BaudexSp1MBit:
                case UsbCanBaudrateEx.Baudex1MBit:
                    return "1000 kBit/sec";
                case UsbCanBaudrateEx.BaudexUseBtr01:
                    return "BTR0/BTR1 is used";
                default:
                    return "BTR is unknown (userspecific)";
            }
        }


        /// <summary>
        ///     Converts the a version number into the major version.
        /// </summary>
        /// <param name="dwVersion_p">[IN] Version number to be converted.</param>
        /// <returns>major version</returns>
        public static int ConvertToMajorVer(int dwVersion_p) {
            return (dwVersion_p & 0xff);
        }


        /// <summary>
        ///     Converts the a version number into the minor version.
        /// </summary>
        /// <param name="dwVersion_p">[IN] Version number to be converted.</param>
        /// <returns>minor version</returns>
        public static int ConvertToMinorVer(int dwVersion_p) {
            return (dwVersion_p & 0xff00) >> 8;
        }


        /// <summary>
        ///     Converts the a version number into the release version.
        /// </summary>
        /// <param name="dwVersion_p">[IN] Version number to be converted.</param>
        /// <returns>release version</returns>
        public static int ConvertToReleaseVer(int dwVersion_p) {
            return (int) ((dwVersion_p & 0xffff0000) >> 16);
        }


        /// <summary>
        ///     Checks if the version is equal or higher than a specified value.
        /// </summary>
        /// <param name="dwVersion_p">[IN] Version number to be checked.</param>
        /// <param name="dwCmpMajor_p">[IN] Major version to be compared with.</param>
        /// <param name="dwCmpMinor_p">[IN] Minor version to be compared with.</param>
        /// <returns>True if equal or higher, otherwise False.</returns>
        private static bool CheckVersionIsEqualOrHigher(int dwVersion_p, int dwCmpMajor_p, int dwCmpMinor_p) {
            return ((ConvertToMajorVer(dwVersion_p) > dwCmpMajor_p) || ((ConvertToMajorVer(dwVersion_p) == dwCmpMajor_p) && ((ConvertToMinorVer(dwVersion_p) >= dwCmpMinor_p))));
        }


        /// <summary>
        ///     Checks whether the module is a sysWORXX USB-CANmodul.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module is a sysWORXX USB-CANmodul, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_IS_SYSWORXX() in USBCAN64.H.</remarks>
        public static bool CheckIs_sysWORXX(tUcanHardwareInfoEx HwInfoEx_p) {
            return (((uint) HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_MASK_PID) >= (uint) eUcanProductCode.USBCAN_PRODCODE_PID_MULTIPORT);
        }


        /// <summary>
        ///     Checks whether the module supports automatically transmission of cyclic CAN messages.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module does support cyclic CAN messages, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_CYCLIC_MSG() in USBCAN64.H.</remarks>
        public static bool CheckSupportCyclicMsg(tUcanHardwareInfoEx HwInfoEx_p) {
            return (CheckIs_sysWORXX(HwInfoEx_p) && CheckVersionIsEqualOrHigher(HwInfoEx_p.m_dwFwVersionEx, 3, 6));
        }


        /// <summary>
        ///     Checks whether the module supports two CAN channels (at logical device).
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module (logical device) does support two CAN channels, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_TWO_CHANNEL() in USBCAN64.H.</remarks>
        public static bool CheckSupportTwoChannel(tUcanHardwareInfoEx HwInfoEx_p) {
            return (CheckIs_sysWORXX(HwInfoEx_p) && ((HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_PID_TWO_CHA) != 0));
        }


        /// <summary>
        ///     Checks whether the module supports a termination resistor at the CAN bus.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module does support a termination resistor.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_TERM_RESISTOR() in USBCAN64.H.</remarks>
        public static bool CheckSupportTermResistor(tUcanHardwareInfoEx HwInfoEx_p) {
            return ((HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_PID_TERM) != 0);
        }


        /// <summary>
        ///     Checks whether the module supports a user I/O port.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module supports a user I/O port, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_USER_PORT() in USBCAN64.H.</remarks>
        public static bool CheckSupportUserPort(tUcanHardwareInfoEx HwInfoEx_p) {
            return ((((uint) HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_MASK_PID) != (uint) eUcanProductCode.USBCAN_PRODCODE_PID_BASIC) &&
                    (((uint) HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_MASK_PID) != (uint) eUcanProductCode.USBCAN_PRODCODE_PID_RESERVED1) &&
                    CheckVersionIsEqualOrHigher(HwInfoEx_p.m_dwFwVersionEx, 2, 16));
        }


        /// <summary>
        ///     Checks whether the module supports a user I/O port including read back feature.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module does support a user I/O port including the read back feature, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_RBUSER_PORT() in USBCAN64.H.</remarks>
        public static bool CheckSupportRbUserPort(tUcanHardwareInfoEx HwInfoEx_p) {
            return ((HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_PID_RBUSER) != 0);
        }


        /// <summary>
        ///     Checks whether the module supports a CAN I/O port including read back feature.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module does support a CAN I/O port including the read back feature, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_RBCAN_PORT() in USBCAN64.H.</remarks>
        public static bool CheckSupportRbCanPort(tUcanHardwareInfoEx HwInfoEx_p) {
            return ((HwInfoEx_p.m_dwProductCode & Constants.USBCAN_PRODCODE_PID_RBCAN) != 0);
        }


        /// <summary>
        ///     Checks whether the module supports the usage of USB-CANnetwork driver.
        /// </summary>
        /// <param name="HwInfoEx_p">
        ///     [IN] Extended hardware information structure (see methode <see cref="GetHardwareInfo" />).
        /// </param>
        /// <returns>True when the module does support the usage of the USB-CANnetwork driver, otherwise False.</returns>
        /// <remarks>Comparable with macro USBCAN_CHECK_SUPPORT_UCANNET() in USBCAN64.H.</remarks>
        public static bool CheckSupportUcannet(tUcanHardwareInfoEx HwInfoEx_p) {
            return (CheckIs_sysWORXX(HwInfoEx_p) && CheckVersionIsEqualOrHigher(HwInfoEx_p.m_dwFwVersionEx, 3, 8));
        }


        /// <summary>
        ///     is the actual callback function for UcanInitHwConnectControlEx()
        /// </summary>
        /// <param name="dwEvent_p">
        ///     [IN] event
        ///     USBCAN_EVENT_CONNECT
        ///     USBCAN_EVENT_DISCONNECT
        ///     USBCAN_EVENT_FATALDISCON
        /// </param>
        /// <param name="dwParam_p">
        ///     [IN] additional parameter (depends on bEvent_p)
        ///     USBCAN_EVENT_CONNECT:       always 0
        ///     USBCAN_EVENT_DISCONNECT.    always 0
        ///     USBCAN_EVENT_FATALDISCON:   USB-CAN-Handle of the disconnected module
        /// </param>
        /// <param name="pArg_p">
        ///     [IN] additional parameter
        ///     Parameter which was defined with UcanInitHardwareEx() not used in this wrapper class
        /// </param>
        private static void UcanConnectControl(eUcanCbEvent dwEvent_p, uint dwParam_p, IntPtr pArg_p) {
            // sometimes the console output crashes because the Textwriter object is already disposed
            Debug.WriteLine("Event: " + dwEvent_p + ", Param: " + dwParam_p);

            // do for each event type the appropriate action,
            // e.g. raise the appropriate .NET event

            if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_FATALDISCON)) {
                // Search for equivalent object in colUcanHandle_l and call object's handle method
                foreach (UsbCanServer obj in ColUcanHandleL) {
                    if ((obj._ucanHandle == dwParam_p)) {
                        obj.HandleFatalDisconnect(dwParam_p, pArg_p);
                        break;
                    }
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_CONNECT)) {
                if (ConnectEvent != null) {
                    ConnectEvent();
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_DISCONNECT)) {
                if (DisconnectEvent != null) {
                    DisconnectEvent();
                }
            }
        }


        /// <summary>
        ///     Is called from the USBCAN64.DLL if a working event occured.
        /// </summary>
        /// <param name="UcanHandle_p">
        ///     [IN] USB-CAN-Handle
        ///     Handle, which is returned by the function UcanInitHardware()
        /// </param>
        /// <param name="dwEvent_p">
        ///     [IN] event type
        ///     USBCAN_EVENT_INITHW
        ///     USBCAN_EVENT_INITCAN
        ///     USBCAN_EVENT_RECIEVE
        ///     USBCAN_EVENT_STATUS
        ///     USBCAN_EVENT_DEINITCAN
        ///     USBCAN_EVENT_DEINITHW
        /// </param>
        /// <param name="bChannel_p"> [IN] CAN channel (USBCAN_CHANNEL_CH0 or USBCAN_CHANNEL_CH1, USBCAN_CHANNEL_ANY)</param>
        /// <param name="pArg_p">
        ///     [IN] additional parameter
        ///     Parameter which was defined with UcanInitHardwareEx() not used in this wrapper class
        /// </param>
        private void HandleUcanCallback(byte UcanHandle_p, eUcanCbEvent dwEvent_p, UsbCanChannel bChannel_p, IntPtr pArg_p) {
            Debug.WriteLine("im Server: Handle: " + UcanHandle_p + ", Event: " + dwEvent_p + ", Channel: " + bChannel_p);

            // do for each event type the appropriate action,
            // e.g. raise the appropriate .NET event
            if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_INITHW)) {
                if (InitHwEvent != null) {
                    InitHwEvent();
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_INITCAN)) {
                if (InitCanEvent != null) {
                    InitCanEvent(_bDeviceNr, bChannel_p);
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_RECEIVE)) {
                if (CanMsgReceivedEvent != null) {
                    CanMsgReceivedEvent(_bDeviceNr, bChannel_p);
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_STATUS)) {
                if (StatusEvent != null) {
                    StatusEvent(_bDeviceNr, bChannel_p);
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_DEINITCAN)) {
                if (DeinitCanEvent != null) {
                    DeinitCanEvent(_bDeviceNr, bChannel_p);
                }
            }
            else if ((dwEvent_p == eUcanCbEvent.USBCAN_EVENT_DEINITHW)) {
                // raise event before or after clean up?
                _gchUcanCallback.Free();

                // delete this object instance from static collection
                lock ((ColUcanHandleL)) {
                    for (var i = 0; i < ColUcanHandleL.Count; i++) {
                        if ((ColUcanHandleL[i]._ucanHandle == UcanHandle_p)) {
                            ColUcanHandleL.RemoveAt(i);
                            break;
                        }
                    }
                }

                if (DeinitHwEvent != null) {
                    DeinitHwEvent(_bDeviceNr);
                }
            }
        }


        /// <summary>
        ///     is called from UcanConnectControl() and cleans up internal structures and raises the member event FatalDisconnectEvent()
        /// </summary>
        /// <param name="dwParam_p">
        ///     [IN] additional parameter (depends on bEvent_p)
        ///     USBCAN_EVENT_FATALDISCON:   USB-CAN-Handle of the disconnected module
        /// </param>
        /// <param name="pArg_p">[IN] additional parameter Parameter which was defined with UcanInitHardwareEx() not used in this wrapper class</param>
        private void HandleFatalDisconnect(uint dwParam_p, IntPtr pArg_p) {
            _gchUcanCallback.Free();

            // reset the initialization states
            _fHwIsInitialized = false;
            _fCh0IsInitialized = false;
            _fCh1IsInitialized = false;
            _fIsInitialized = false;


            // delete this object instance from static collection
            lock (ColUcanHandleL) {
                var servers = ColUcanHandleL.ToArray();
                for (var i = 0; i < ColUcanHandleL.Count; i++) {
                    //If CType(colUcanHandle_l.Item(i), USBcanServer).m_UcanHandle = dwParam_p Then
                    if ((ReferenceEquals(servers[i], this))){
                        Debug.WriteLine("FatalDisconnect: object removed");
                        ColUcanHandleL.RemoveAt(i);
                        break;
                    }
                }
            }

            _ucanHandle = 0;
            if (FatalDisconnectEvent != null) {
                FatalDisconnectEvent(_bDeviceNr);
            }
        }

        private void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.

            if (!(_disposed)) {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if ((disposing)) {
                    // Dispose managed resources.
                }

                // Release unmanaged resources. If disposing is false,
                // only the following code is executed.

                // if last active instance deregister ConnectControl callback function
                _dwPlugAndPlayCountL -= 1;
                if ((_dwPlugAndPlayCountL == 0)) {
                    Unmanaged.UcanDeinitHwConnectControl();
                    _gchUcanConnectControlL.Free();
                }


                lock ((this)) {
                    // shutdown each channel if it's initialized
                    if (_fCh0IsInitialized) {
                        Unmanaged.UcanDeinitCanEx(_ucanHandle, UsbCanChannel.Ch0);
                        _fCh0IsInitialized = false;
                    }

                    if (_fCh1IsInitialized) {
                        Unmanaged.UcanDeinitCanEx(_ucanHandle, UsbCanChannel.Ch1);
                        _fCh1IsInitialized = false;
                    }


                    if (_fHwIsInitialized) {
                        // shutdown hardware
                        try {
                            Unmanaged.UcanDeinitHardware(_ucanHandle);
                            //Eine nicht behandelte Ausnahme des Typs 'System.NullReferenceException' ist in UcanDotNET.dll aufgetreten.
                            //Zusдtzliche Informationen: Der Objektverweis wurde nicht auf eine Objektinstanz festgelegt.
                        }
                        catch (NullReferenceException) {
                            Debug.WriteLine("catched NullReferenceException while executing UcanDeinithardware");
                        }

                        _fHwIsInitialized = false;
                        _ucanHandle = 0;

                        // test if this object instance is already deleted from the static collection
                        var servers = ColUcanHandleL.ToArray();
                        for (var i = 0; i < servers.Length; i++){
                            if ((ReferenceEquals(servers[i], this))){
                                ColUcanHandleL.RemoveAt(i);
                                break;
                            }
                        }
                    }
                }
            }

            _disposed = true;
        }

        ~UsbCanServer() {
            Dispose(false);
        }
    }

}