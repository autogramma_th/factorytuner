﻿namespace Asap1B.UsbCan {

    /// <summary>Specifies all return codes of all methodes of this class.</summary>
    internal enum UsbCanReturn : byte {
        /// <summary>no error</summary>
        Successful = 0x0,

        /// <summary>start of error codes comming from USB-CAN-library</summary>
        Err = 0x1,

        /// <summary>start of error codes comming from command interface between host and USB-CANmodul</summary>
        ErrCmd = 0x40,

        /// <summary>start of warning codes</summary>
        Warning = 0x80,

        /// <summary>start of recerved codes which are only used internaly</summary>
        Reserved = 0xc0,

        /// <summary>could not created a resource (memory, handle, ...)</summary>
        ErrResource = 0x1,

        /// <summary>the maximum number of opened modules is reached</summary>
        ErrMaxModules = 0x2,

        /// <summary>the specified module is already in use</summary>
        ErrHwInUse = 0x3,

        /// <summary>the software versions of the module and library are incompatible</summary>
        ErrIllVersion = 0x4,

        /// <summary>the module with the specified device number is not connected (or used by an other application)</summary>
        ErrIllHw = 0x5,

        /// <summary>wrong USB-CAN-Handle handed over to the function</summary>
        ErrIllHandle = 0x6,

        /// <summary>wrong parameter handed over to the function</summary>
        ErrIllParam = 0x7,

        /// <summary>instruction can not be processed at this time</summary>
        ErrBusy = 0x8,

        /// <summary>no answer from module</summary>
        ErrTimeout = 0x9,

        /// <summary>a request to the driver failed</summary>
        ErrIoFailed = 0xa,

        /// <summary>a CAN message did not fit into the transmit buffer</summary>
        ErrDllTxfull = 0xb,

        /// <summary>maximum number of applications is reached</summary>
        ErrMaxInstances = 0xc,

        /// <summary>CAN interface is not yet initialized</summary>
        ErrCannotInit = 0xd,

        /// <summary>USB-CANmodul was disconnected</summary>
        ErrDisconect = 0xe,

        /// <summary>the needed device class does not exist</summary>
        ErrNoHwclass = 0xf,

        /// <summary>illegal CAN channel</summary>
        ErrIllChannel = 0x10,

        /// <summary>reserved</summary>
        ErrReserved1 = 0x11,

        /// <summary>the API function can not be used with this hardware</summary>
        ErrIllHwtype = 0x12,

        /// <summary>the received response does not match to the transmitted command</summary>
        ErrcmdNotEqu = 0x40,

        /// <summary>no access to the CAN controller</summary>
        ErrcmdRegTst = 0x41,

        /// <summary>the module could not interpret the command</summary>
        ErrcmdIllCmd = 0x42,

        /// <summary>error while reading the EEPROM</summary>
        ErrcmdEEPROM = 0x43,

        /// <summary>reserved</summary>
        ErrcmdReserved1 = 0x44,

        /// <summary>reserved</summary>
        ErrcmdReserved2 = 0x45,

        /// <summary>reserved</summary>
        ErrcmdReserved3 = 0x46,

        /// <summary>illegal baud rate value specified in BTR0/BTR1 for sysWORXX USB-CANmoduls</summary>
        ErrcmdIllBdr = 0x47,

        /// <summary>CAN channel is not initialized</summary>
        ErrcmdNotInit = 0x48,

        /// <summary>CAN channel is already initialized</summary>
        ErrcmdAlreadyInit = 0x49,

        /// <summary>illegal sub-command specified</summary>
        ErrcmdIllSubCmd = 0x4a,

        /// <summary>illegal index specified (e.g. index for cyclic CAN messages)</summary>
        ErrcmdIllIdx = 0x4b,

        /// <summary>cyclic CAN message(s) can not be defined because transmission of cyclic CAN messages is already running</summary>
        ErrcmdRunning = 0x4c,

        /// <summary>no CAN messages received</summary>
        WarnNodata = 0x80,

        /// <summary>overrun in receive buffer of the kernel driver</summary>
        WarnSysRxOverrun = 0x81,

        /// <summary>overrun in receive buffer of the USB-CAN-library</summary>
        WarnDllRxOverrun = 0x82,

        /// <summary>reserved</summary>
        WarnReserved1 = 0x83,

        /// <summary>reserved</summary>
        WarnReserved2 = 0x84,

        /// <summary>overrun in transmit buffer of the firmware (but this CAN message was successfully stored in buffer of the library)</summary>
        WarnFwTxOverrun = 0x85,

        /// <summary>overrun in receive buffer of the firmware (but this CAN message was successfully read)</summary>
        WarnFwRxOverrun = 0x86,

        /// <summary>reserved</summary>
        WarnFwTxMsgLost = 0x87,

        /// <summary>pointer is NULL</summary>
        WarnNullPtr = 0x90,

        /// <summary>not all CAN messages could be stored to the transmit buffer in USB-CAN-library (check output of parameter pdwCount_p)</summary>
        WarnTxLimit = 0x91,

        /// <summary>reserved</summary>
        WarnBusy = 0x92
    }

}