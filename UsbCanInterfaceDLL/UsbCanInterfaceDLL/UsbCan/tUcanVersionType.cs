﻿namespace Asap1B.UsbCan {

    /// <summary>Specifies values for receiving the version information of several driver files.</summary>
    /// <remarks>These values are only used internaly.</remarks>
    internal enum tUcanVersionType : short {
        /// <summary>version of the USB-CAN-library</summary>
        kVerTypeUserLib = 1,

        /// <summary>
        ///     equivalent to <c>kVerTypeUserLib</c>
        /// </summary>
        kVerTypeUserDll = 1,

        /// <summary>version of USBCAN.SYS (not supported in this version)</summary>
        kVerTypeSysDrv = 2,

        /// <summary>
        ///     version of firmware in hardware (not supported, use methode <see cref="UsbCanServer.GetFwVersion" />)
        /// </summary>
        kVerTypeFirmware = 3,

        /// <summary>version of UCANNET.SYS</summary>
        kVerTypeNetDrv = 4,

        /// <summary>version of USBCANLD.SYS</summary>
        kVerTypeSysLd = 5,

        /// <summary>version of USBCANL2.SYS</summary>
        kVerTypeSysL2 = 6,

        /// <summary>version of USBCANL3.SYS</summary>
        kVerTypeSysL3 = 7,

        /// <summary>version of USBCANL4.SYS</summary>
        kVerTypeSysL4 = 8,

        /// <summary>version of USBCANL5.SYS</summary>
        kVerTypeSysL5 = 9,

        /// <summary>version of USBCANCP.CPL</summary>
        kVerTypeCpl = 10
    }

}