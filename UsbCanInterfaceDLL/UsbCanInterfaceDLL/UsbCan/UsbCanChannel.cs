﻿namespace Asap1B.UsbCan {

    /// <summary>Specifies values for the CAN channel to be used on multi-channel USB-CANmoduls.</summary>
    internal enum UsbCanChannel : byte {
        /// <summary>
        ///     Specifies the first CAN channel (GW-001/GW-002 and USB-CANmodul1 only can be used with this channel).
        /// </summary>
        Ch0 = 0,

        /// <summary>
        ///     Specifies the second CAN channel (this channel cannot be used with GW-001/GW-002 and USB-CANmodul1).
        /// </summary>
        Ch1 = 1,

        /// <summary>
        ///     Specifies all CAN channels (can only be used with the methode <see cref="UsbCanServer.Shutdown" />).
        /// </summary>
        All = 254,

        /// <summary>
        ///     Specifies the use of any channel (can only be used with the methode <see cref="UsbCanServer.ReadCanMsg" />).
        /// </summary>
        Any = 255,
    }

}