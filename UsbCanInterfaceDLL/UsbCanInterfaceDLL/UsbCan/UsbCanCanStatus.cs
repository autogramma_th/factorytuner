﻿using System;

namespace Asap1B.UsbCan {

    /// <summary>
    ///     CAN error status bits. These bit values occurs in combination with the methode <see cref="GetStatus" />.
    /// </summary>
    [Flags]
    internal enum UsbCanCanStatus : short {
        /// <summary>no error</summary>
        Ok = 0x0,

        /// <summary>Transmit buffer of the CAN controller is full.</summary>
        XmtFull = 0x1,

        /// <summary>Receive buffer of the CAN controller is full.</summary>
        Overrun = 0x2,

        /// <summary>Bus error: Error Limit 1 exceeded (Warning Limit reached)</summary>
        BusLight = 0x4,
        
        /// <summary>Bus error: Error Limit 2 exceeded (Error Passive)</summary>
        BusHeavy = 0x8,

        /// <summary>Bus error: CAN controller has gone into Bus-Off state</summary>
        /// <remarks>
        ///     Methode <see cref="UsbCanServer.ResetCan" /> has to  be called./>
        /// </remarks>
        BusOff = 0x10,

        /// <summary>No CAN message is within the receive buffer.</summary>
        QrcvEmpty = 0x20,

        /// <summary>Receive buffer is full. CAN messages has been lost.</summary>
        Qoverrun = 0x40,

        /// <summary>Transmit buffer is full.</summary>
        QxmtFull = 0x80,

        /// <summary>Register test of the CAN controller failed.</summary>
        RegTest = 0x100,

        /// <summary>Memory test on hardware failed.</summary>
        MemTest = 0x200,

        /// <summary>Transmit CAN message(s) was/were automatically deleted by firmware (transmit timeout).</summary>
        TxmsgLost = 0x400
    }

}