﻿namespace Asap1B.UsbCan {

    /// <summary>This enum defines events for the callback functions of the library.</summary>
    internal enum eUcanCbEvent : uint {
        /// <summary>The USB-CANmodul has been initialized.</summary>
        USBCAN_EVENT_INITHW = 0,

        /// <summary>The CAN interface has been initialized.</summary>
        USBCAN_EVENT_INITCAN = 1,

        /// <summary>A new CAN message has been received.</summary>
        USBCAN_EVENT_RECEIVE = 2,

        /// <summary>The error state in the module has changed.</summary>
        USBCAN_EVENT_STATUS = 3,

        /// <summary>The CAN interface has been deinitialized.</summary>
        USBCAN_EVENT_DEINITCAN = 4,

        /// <summary>The USB-CANmodul has been deinitialized.</summary>
        USBCAN_EVENT_DEINITHW = 5,

        /// <summary>A new USB-CANmodul has been connected.</summary>
        USBCAN_EVENT_CONNECT = 6,

        /// <summary>Any USB-CANmodul has been disconnected.</summary>
        USBCAN_EVENT_DISCONNECT = 7,

        /// <summary>A USB-CANmodul has been disconnected during operation.</summary>
        USBCAN_EVENT_FATALDISCON = 8,

        USBCAN_EVENT_RESERVED1 = 0x80
    }

}