﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     Specifies values for operation mode of a CAN channel.
    ///     These values can be combined by OR operation with the methode <see cref="UsbCanServer.InitCan" />.
    /// </summary>
    internal enum tUcanMode : byte {
        /// <summary>normal operation mode (transmitting and receiving)</summary>
        kUcanModeNormal = 0,

        /// <summary>listen only mode (receiving only, no ACK at CAN bus)</summary>
        kUcanModeListenOnly = 1,

        /// <summary>
        ///     CAN messages which was sent will be received back with methode <see cref="UsbCanServer.ReadCanMsg" />.
        /// </summary>
        kUcanModeTxEcho = 2,

        /// <summary>reserved</summary>
        kUcanModeRxOrderCh = 4
    }

}