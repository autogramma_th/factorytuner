﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>
    ///     Structure including the number of sent and received CAN messages.
    ///     This structure is used with the methode <see cref="UsbCanServer.GetMsgCountInfo" />.
    /// </summary>
    /// <remarks>This structure is only used internaly.</remarks>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct tUcanMsgCountInfo {
        /// <summary>number of sent CAN messages</summary>
        public short m_wSentMsgCount;

        /// <summary>number of received CAN messages</summary>
        public short m_wRecvdMsgCount;
    }

}