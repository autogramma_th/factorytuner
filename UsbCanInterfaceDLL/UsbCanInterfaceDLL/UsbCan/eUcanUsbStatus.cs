﻿namespace Asap1B.UsbCan {

    /// <summary>
    ///     USB error status bits. These bit values occurs in combination with the methode <see cref="UsbCanServer.GetStatus" />.
    /// </summary>
    internal enum eUcanUsbStatus : short {
        /// <summary>no error</summary>
        USBCAN_USBERR_OK = 0x0
    }

}