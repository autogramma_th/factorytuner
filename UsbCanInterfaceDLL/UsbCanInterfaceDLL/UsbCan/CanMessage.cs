﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>Structure of a CAN message.</summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CanMessage {
        /// <summary>CAN Identifier</summary>
        public uint CanID;

        /// <summary>
        ///     CAN Frame Format (see enum <see cref="UsbCanMsgFrameFormat" />)
        /// </summary>
        public UsbCanMsgFrameFormat FrameFormat;

        /// <summary>
        ///     CAN Data Length Code, 8
        /// </summary>
        public byte DataLengthCode;

        /// <summary>
        ///     CAN Data (array of 8 bytes)
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)] public byte[] Data;

        /// <summary>
        ///     Receive time stamp in msec (for transmit messages no meaning)
        /// </summary>
        public uint TimeStamp;

        /// <summary>
        ///     Creates a new instance of a CAN message including 8 data bytes.
        /// </summary>
        /// <param name="canId">CAN Identifier, 0</param>
        /// <param name="frameFormat">CAN Frame Format, 0</param>
        /// <returns>Returns the new instance of a tCanMsgSturct structure.</returns>
        public static CanMessage Create(uint canId, UsbCanMsgFrameFormat frameFormat) {
            var instance = default(CanMessage);
            instance.Data = new byte[8];
            instance.DataLengthCode = 8;
            instance.FrameFormat = frameFormat;
            instance.CanID = canId;
            return instance;
        }
    }

}