﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>Structure includint initialisation paramaters used with UcanInitCanEx() and UcanInitCanEx2()</summary>
    /// <remarks>This structure is only used internaly.</remarks>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct tUcanInitCanParam {
        /// <summary>size of this structure (only used internaly)</summary>
        public int m_dwSize;

        /// <summary>
        ///     selects the mode of CAN controller (see enum <see cref="tUcanMode" />)
        /// </summary>
        public tUcanMode m_bMode;

        // Baudrate Registers for GW-001 or GW-002
        /// <summary>
        ///     Bus Timing Register 0 (see enum <see cref="UsbCanBaudrate" />)
        /// </summary>
        public byte m_bBTR0;

        /// <summary>
        ///     Bus Timing Register 1 (see enum <see cref="UsbCanBaudrate" />)
        /// </summary>
        public byte m_bBTR1;

        /// <summary>
        ///     Output Controll Register (see enum <see cref="eUcanOutputControl" />)
        /// </summary>
        public byte m_bOCR;

        /// <summary>
        ///     Acceptance Mask Register (see methode <see cref="UsbCanServer.SetAcceptance" />)
        /// </summary>
        public uint m_dwAMR;

        /// <summary>
        ///     Acceptance Code Register (see methode <see cref="UsbCanServer.SetAcceptance" />)
        /// </summary>
        public uint m_dwACR;

        /// <summary>
        ///     Baudrate Register for all sysWORXX USB-CANmoduls (see enum <see cref="UsbCanBaudrateEx" />)
        /// </summary>
        public uint m_dwBaudrate;

        /// <summary>number of receive buffer entries (default is 4096)</summary>
        public ushort m_wNrOfRxBufferEntries;

        /// <summary>number of transmit buffer entries (default is 4096)</summary>
        public ushort m_wNrOfTxBufferEntries;
    }

}