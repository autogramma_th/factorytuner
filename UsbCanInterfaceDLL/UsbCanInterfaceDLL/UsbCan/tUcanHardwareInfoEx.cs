﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>
    ///     Structure including hardware information about the USB-CANmodul.
    ///     This structure is used with the methode <see cref="UsbCanServer.GetHardwareInfo" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct tUcanHardwareInfoEx {
        /// <summary>size of this structure (only used internaly)</summary>
        public int m_dwSize;

        /// <summary>USB-CAN-Handle assigned by the DLL</summary>
        public byte m_UcanHandle;

        /// <summary>device number of the USB-CANmodul</summary>
        public byte m_bDeviceNr;

        /// <summary>serial number from USB-CANmodul</summary>
        public int m_dwSerialNr;

        /// <summary>version of firmware</summary>
        public int m_dwFwVersionEx;

        /// <summary>
        ///     product code (see enum <see cref="eUcanProductCode" />)
        /// </summary>
        public int m_dwProductCode;
    }

}