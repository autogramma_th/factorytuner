﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>
    ///     Structure including CAN channel information.
    ///     This structure is used with the methode <see cref="UsbCanServer.GetHardwareInfo" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct tUcanChannelInfo {
        /// <summary>size of this structure (only used internaly)</summary>
        public int m_dwSize;

        /// <summary>
        ///     operation mode of CAN controller (see enum <see cref="tUcanMode" />)
        /// </summary>
        public byte m_bMode;

        /// <summary>
        ///     Bus Timing Register 0 (see enum <see cref="UsbCanBaudrate" />)
        /// </summary>
        public byte m_bBTR0;

        /// <summary>
        ///     Bus Timing Register 1 (see enum <see cref="UsbCanBaudrate" />)
        /// </summary>
        public byte m_bBTR1;

        /// <summary>
        ///     Output Controll Register (see enum <see cref="eUcanOutputControl" />)
        /// </summary>
        public byte m_bOCR;

        /// <summary>
        ///     Acceptance Mask Register (see methode <see cref="UsbCanServer.SetAcceptance" />)
        /// </summary>
        public int m_dwAMR;

        /// <summary>
        ///     Acceptance Code Register (see methode <see cref="UsbCanServer.SetAcceptance" />)
        /// </summary>
        public int m_dwACR;

        /// <summary>
        ///     Baudrate Register for all sysWORXX USB-CANmoduls (see enum <see cref="UsbCanBaudrateEx" />)
        /// </summary>
        public int m_dwBaudrate;

        /// <summary>True if the CAN interface is initialized, otherwise false.</summary>
        [MarshalAs(UnmanagedType.Bool)] public bool m_fCanIsInit;

        /// <summary>
        ///     CAN status (same as received by methode <see cref="UsbCanServer.GetStatus" />).
        /// </summary>
        public short m_wCanStatus;
    }

}