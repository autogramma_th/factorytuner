﻿namespace Asap1B.UsbCan {

    /// <summary>Specifies pre-defined baud rate values for all sysWORXX USB-CANmoduls.</summary>
    /// <remarks>
    ///     These values cannot be used for GW-001 and GW-002! Use values from enum  <see cref="UsbCanBaudrate" /> instead.
    /// </remarks>
    internal enum UsbCanBaudrateEx : uint {
        /// <summary>1000 kBit/sec</summary>
        Baudex1MBit = 0x20354,

        /// <summary>800 kBit/sec</summary>
        Baudex800KBit = 0x30254,

        /// <summary>500 kBit/sec</summary>
        Baudex500KBit = 0x50354,

        /// <summary>250 kBit/sec</summary>
        Baudex250KBit = 0xb0354,

        /// <summary>125 kBit/sec</summary>
        Baudex125KBit = 0x170354,

        /// <summary>100 kBit/sec</summary>
        Baudex100KBit = 0x170466,

        /// <summary>50 kBit/sec</summary>
        Baudex50KBit = 0x2f0466,

        /// <summary>20 kBit/sec</summary>
        Baudex20KBit = 0x770466,

        /// <summary>10 kBit/sec (half CPU clock)</summary>
        Baudex10KBit = 0x80770466,

        /// <summary>1000 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp1MBit = 0x20741,

        /// <summary>800 kBit/sec Sample Point: 86,67%</summary>
        BaudexSp800KBit = 0x30731,

        /// <summary>500 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp500KBit = 0x50741,

        /// <summary>250 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp250KBit = 0xb0741,

        /// <summary>125 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp125KBit = 0x170741,

        /// <summary>100 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp100KBit = 0x1d1741,

        /// <summary>50 kBit/sec Sample Point: 87,50%</summary>
        BaudexSp50KBit = 0x3b1741,

        /// <summary>20 kBit/sec Sample Point: 85,00%</summary>
        BaudexSp20KBit = 0x771772,

        /// <summary>10 kBit/sec Sample Point: 85,00% (half CPU clock)</summary>
        BaudexSp10KBit = 0x80771772,

        /// <summary>Uses pre-defined values of baud rates of eUcanBaudrate.</summary>
        BaudexUseBtr01 = 0x0,

        /// <summary>Automatic baud rate detection (not implemented in this version).</summary>
        BaudexAuto = 0xffffffff
    }

}