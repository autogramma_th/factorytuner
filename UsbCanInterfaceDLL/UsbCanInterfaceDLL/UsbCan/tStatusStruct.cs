﻿using System.Runtime.InteropServices;

namespace Asap1B.UsbCan {

    /// <summary>
    ///     Structure including the error status of CAN and USB.
    ///     Use this structure with the methode <see cref="UsbCanServer.GetStatus" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct tStatusStruct {
        /// <summary>
        ///     CAN error status (see enum <see cref="UsbCanCanStatus" />)
        /// </summary>
        public short m_wCanStatus;

        /// <summary>
        ///     USB error status (see enum <see cref="eUcanUsbStatus" />)
        /// </summary>
        public short m_wUsbStatus;
    }

}