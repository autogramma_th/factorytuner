﻿using FactoryDataConfig;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace BuildALL
{
    static class Program
    {
        private const string msBuildExePath = @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe";

        private const string _FactoryDataConfigProjName = "FactoryDataConfig";
        private const string _FactoryInstallProjName = "FactoryInstall";
        private const string _FactoryTunerProjName = "FactoryTuner";
        private const string _FactoryUserConfigProjName = "FactoryUserConfig";

        static void Main(string[] args)
        {
            Console.WriteLine("MSBuild.exe путь:");
            Console.WriteLine(msBuildExePath);

            var thisExeDir = System.Reflection.Assembly.GetEntryAssembly().Location;
            var rootDirectory = Directory.GetParent(thisExeDir).Parent.Parent.Parent.FullName;
            var installerZipsDirectory = Path.Combine(rootDirectory, _FactoryInstallProjName, "zip");

            try
            {
                //Remove old zip
                CodeHelpers.RemoveAllFilesFromDirector(installerZipsDirectory);

                //FactoryUserCfg
                BuildProject(Path.Combine(rootDirectory, _FactoryUserConfigProjName, _FactoryUserConfigProjName + ".csproj"), "Release");
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryUserConfigProjName + ".zip"),
                    Path.Combine(rootDirectory, _FactoryUserConfigProjName, "bin", "Release", AppName.FactoryUserConfig.ToString() + ".exe"));

                //FactoryDataCfg
                BuildProject(Path.Combine(rootDirectory, _FactoryDataConfigProjName, _FactoryDataConfigProjName + ".csproj"), "Release");
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryDataConfigProjName + ".zip"),
                    Path.Combine(rootDirectory, _FactoryDataConfigProjName, "bin", "Release", AppName.FactoryDataConfig.ToString() + ".exe"));

                //FactoryTuner
                BuildProject(Path.Combine(rootDirectory, _FactoryTunerProjName, _FactoryTunerProjName + ".csproj"), "Release", false);
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryTunerProjName + ".zip"),
                    Path.Combine(rootDirectory, _FactoryTunerProjName, "bin", "Release", AppName.MIKAS_Master.ToString() + ".exe"),
                    Path.Combine(rootDirectory, _FactoryTunerProjName, "bin", "Release", "Usbcan32.dll"));

                //data
                ZipWithDataFolderAndCopyTo(Path.Combine(installerZipsDirectory, "data.zip"),
                    Path.Combine(rootDirectory, _FactoryTunerProjName, "bin", "Release", "data", "base.xml"),
                    Path.Combine(rootDirectory, _FactoryTunerProjName, "bin", "Release", "data", "config.xml"),
                    Path.Combine(rootDirectory, _FactoryTunerProjName, "bin", "Release", "data", "user.xml"));

                //Empty FactoryInstall.zip
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryInstallProjName + ".zip"));

                //Installer (not extended)
                BuildProject(Path.Combine(rootDirectory, _FactoryInstallProjName, _FactoryInstallProjName + ".csproj"), "Release");
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryInstallProjName + ".zip"),
                    Path.Combine(rootDirectory, _FactoryInstallProjName, "bin", "Release", AppName.MIKAS_Manager.ToString() + ".exe"));

                //Installer (Extended)
                BuildProject(Path.Combine(rootDirectory, _FactoryInstallProjName, _FactoryInstallProjName + ".csproj"), "EXTENDED");

                //Empty FactoryInstall.zip - чтобы при следующей компиляции циклично не рос (возможно рудимент из-за msbuild /t:Clean,Build)
                ZipAndCopyTo(Path.Combine(installerZipsDirectory, _FactoryInstallProjName + ".zip"));

                var resultDirectory = Path.Combine(rootDirectory, _FactoryInstallProjName, "bin", "EXTENDED");
                ConsoleWrite("Готово. Результат тут: " + Path.Combine(resultDirectory, AppName.MIKAS_Manager.ToString() + ".exe"), ConsoleColor.Green);
                Trace.TraceInformation("Готово. Результат тут: " + Path.Combine(resultDirectory, AppName.MIKAS_Manager.ToString() + ".exe"), ConsoleColor.Green);
                ConsoleWrite("Открыть папку? y/n", ConsoleColor.Green);
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Process.Start(resultDirectory);
                }
            }
            catch (Exception ex)
            {
                ConsoleWrite("Ошибка: " + ex.Message, ConsoleColor.Red);
                Console.ReadKey();
            }
        }

        static void ConsoleWrite(string text, ConsoleColor? color = null)
        {
            var prevColor = Console.ForegroundColor;
            if (color.HasValue)
                Console.ForegroundColor = color.Value;
            Console.WriteLine(text);
            Console.ForegroundColor = prevColor;
        }

        static void ZipAndCopyTo(string toFileFullPath, params string[] fullFileNamesToZip)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var file in fullFileNamesToZip)
                {
                    zip.AddFile(file, "");
                }
                zip.Save(toFileFullPath);
            }
        }

        static void ZipWithDataFolderAndCopyTo(string toFileFullPath, params string[] fullFileNamesToZip)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var file in fullFileNamesToZip)
                {
                    zip.AddFile(file, "data");
                }
                zip.Save(toFileFullPath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csprojPath"></param>
        /// <param name="configuration"></param>
        /// <exception cref="System.Exception">Thrown if msbuild exception</exception>
        /// <exception cref="System.ArgumentException">Thrown if arguments is empty</exception>
        static void BuildProject(string csprojPath, string configuration, bool isCleanProj = true)
        {
            if (string.IsNullOrWhiteSpace(csprojPath) || string.IsNullOrWhiteSpace(configuration))
                throw new ArgumentException();

            if (!File.Exists(msBuildExePath))
                throw new FileNotFoundException("MSBuild not found. Please set valid path.");

            var platform = "AnyCPU";
            var outDir = Path.Combine(Directory.GetParent(csprojPath).FullName, "bin", configuration);
            Console.WriteLine($"Build project: {csprojPath}");
            Console.WriteLine($"Configuration: {configuration}");
            Console.WriteLine($"Platform: {platform}");

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = msBuildExePath;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

            if (isCleanProj)
                startInfo.Arguments = $"{csprojPath} /t:Clean,Build /p:Configuration={configuration} /p:Platform=\"{platform}\" /p:OutDir=\"{outDir}\"";
            else
                startInfo.Arguments = $"{csprojPath} /t:Build /p:Configuration={configuration} /p:Platform=\"{platform}\" /p:OutDir=\"{outDir}\"";

            using (Process exeProcess = Process.Start(startInfo))
            {
                exeProcess.WaitForExit();
                if (exeProcess.ExitCode != 0)
                    throw new Exception("MSBuild error. Check parameters");
            }

            Console.Write(Environment.NewLine);
        }
    }
}
