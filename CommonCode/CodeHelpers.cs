﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FactoryDataConfig
{
    public static class CodeHelpers
    {
        public static T[] Concat<T>(this T[] x, T[] y)
        {
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");
            int oldLen = x.Length;
            Array.Resize<T>(ref x, x.Length + y.Length);
            Array.Copy(y, 0, x, oldLen, y.Length);
            return x;
        }

        public static void SubscribeToUndandledExceptions()
        {
            Debug.WriteLine(nameof(SubscribeToUndandledExceptions));

            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                Exception ee = (Exception)e.ExceptionObject;
                var exceptionMsg = ee.ToString();

                Exception realerror = ee;
                while (realerror.InnerException != null)
                {
                    realerror = realerror.InnerException;
                    exceptionMsg += "\n" + realerror.ToString();
                }

                Trace.Fail("Need developer for this problem." + "\n(" + ee.GetType().Name + ")\n" + exceptionMsg);
            };
        }

        public static string GetMd5Hash(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;
            byte[] data = null;
            using (MD5 md5Hash = MD5.Create())
            {
                var sl = new byte[] { 0x66, 0x33, 0x77, 0x65, 0x34, 0x33, 0x66, 0x77, 0x73, 0x65 };
                var inputBytes = Encoding.UTF8.GetBytes(input);
                data = md5Hash.ComputeHash(sl.Concat(inputBytes).ToArray());
            }
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();

        }

        public static bool IsScreenIsOld()
        {
            Debug.WriteLine(nameof(IsScreenIsOld));

            var screen = System.Windows.Forms.SystemInformation.VirtualScreen.Size;
            Trace.TraceInformation("ScreenSize: " + screen.ToString());
            return (screen.Width <= 800 || screen.Height <= 600);
        }

        public static string GetAppVersionString()
        {
            return "v" + GetAppVersion();
        }

        public static string GetAppVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public static bool TrySaveDataToXmlFile<T>(T Data, string filePath, bool encrypt = false)
        {
            Debug.WriteLine(nameof(TrySaveDataToXmlFile));

            Trace.TraceInformation("Try to save to: " + filePath);
            var result = false;
            if (string.IsNullOrEmpty(filePath))
            {
                Trace.TraceWarning("Not valid filepath to save");
            }
            else
            {
                try
                {
                    if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(filePath));



                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        if (Data == null)
                        {
                            Trace.TraceWarning(new ArgumentNullException(nameof(Data)).ToString());
                        }
                        else
                        {
                            Stream outputStream = fileStream;
                            if (encrypt)
                            {
                                RijndaelManaged RMCrypto = new RijndaelManaged();

                                byte[] key = new byte[] { 0x65, 0x33, 0x76, 0x65, 0x34, 0x32, 0x66, 0x77, 0x73, 0x65, 0x34, 0x32, 0x66, 0x77, 0x73, 0x65 };

                                outputStream = new CryptoStream(fileStream,
                                    RMCrypto.CreateEncryptor(key, key),
                                    CryptoStreamMode.Write);
                            }

                            serializer.Serialize(outputStream, Data);
                            outputStream.Flush();

                            if (encrypt)
                                outputStream.Close();
                        }
                    }
                    Trace.TraceInformation("Saved data to: " + filePath);
                    result = true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.ToString());
                }
            }
            return result;
        }

        public static IEnumerable<T> ToEnumerable<T>(this ComboBox.ObjectCollection collection)
        {
            foreach (var obj in collection)
            {
                yield return (T)obj;
            }
        }

        public static T TryLoadDataFromXmlFile<T>(string filePath, bool isDecrypt = false) where T : class
        {
            Debug.WriteLine(nameof(TryLoadDataFromXmlFile));

            Trace.TraceInformation("Try to load from: " + filePath);
            T result = null;
            if (string.IsNullOrEmpty(filePath))
            {
                Trace.TraceWarning("Not valid filepath to load");
            }
            else if (!File.Exists(filePath))
            {
                Trace.TraceWarning("File not exist");
            }
            else
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (var fileStream = new FileStream(filePath, FileMode.Open))
                    {
                        Stream inputStream = fileStream;

                        if (isDecrypt)
                        {
                            RijndaelManaged RMCrypto = new RijndaelManaged();
                            byte[] key = new byte[] { 0x65, 0x33, 0x76, 0x65, 0x34, 0x32, 0x66, 0x77, 0x73, 0x65, 0x34, 0x32, 0x66, 0x77, 0x73, 0x65 };
                            inputStream = new CryptoStream(fileStream,
                                RMCrypto.CreateDecryptor(key, key),
                                CryptoStreamMode.Read);
                        }

                        result = (T)serializer.Deserialize(inputStream);

                        if (isDecrypt)
                            inputStream.Close();
                    }

                    Trace.TraceInformation("Loaded data from: " + filePath);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.ToString());
                }
            }
            return result;
        }

        public static long GetUnixTimeNowSeconds()
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }

        public static bool ByteArrayCompare(byte[] a1, byte[] a2)
        {
            if (a1 == null || a2 == null)
            {
                Trace.TraceWarning(new ArgumentNullException().ToString());
                return false;
            }

            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
                if (a1[i] != a2[i])
                    return false;

            return true;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            if (ba == null)
            {
                Trace.TraceWarning(new ArgumentNullException(nameof(ba)).ToString());
                return string.Empty;
            }

            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static void CopyDirectory(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAllDirectory(diSource, diTarget);
        }

        public static void RemoveAllFilesFromDirector(string directoryFullPath)
        {
            DirectoryInfo directory = new DirectoryInfo(directoryFullPath);
            foreach (FileInfo fi in directory.GetFiles())
            {
                try
                {
                    fi.Delete();
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Can't remove " + fi.FullName);
                    Trace.TraceError(ex.ToString());
                }
            }
            foreach (DirectoryInfo diSourceSubDir in directory.GetDirectories())
            {
                RemoveAllFilesFromDirector(diSourceSubDir.FullName);
            }
        }

        /// <summary>
        /// Не долго ждет доступности адреса и спрашивает у пользователя, надо ли ждать или открыть рабочий стол?
        /// </summary>
        /// <returns>
        /// Возвр исходный путь если доступен или рабочий стол
        /// </returns>
        public static string FactCheckIsIsNetworkDirectoryExistAndWaitOrOpenDesktop(Form parent, string directory, int timeoutSec = 2)
        {
            var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (string.IsNullOrWhiteSpace(directory))
                return desktopPath;
            var task = new Task<bool>(() =>
            {
                var fi = new DirectoryInfo(directory); return fi.Exists;
            });
            task.Start();

            if (task.Wait(timeoutSec * 1000))
            {
                return directory;
            }
            else
            {
                var diagResult = MessageBox.Show(parent, "Хотите ожидать пока сетевой путь откроется?", "Подождать?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (diagResult == DialogResult.OK || diagResult == DialogResult.Yes)
                    return directory;
                else
                    return desktopPath;
            }
        }

        public static void CopyAllDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAllDirectory(diSourceSubDir, nextTargetSubDir);
            }
        }

        public static void DeleteSelfExeAndShortcut(string appGuid, string shortcutName)
        {
            Trace.TraceInformation(nameof(DeleteSelfExeAndShortcut));
            TryDeleteFileAsAdmin(Application.ExecutablePath);
            TryDeleteDesktopShortcut(shortcutName);
            TryDeleteAppFromReg(appGuid);
        }
        public static void DeleteSelfExeFolderAndShortcut(string appGuid, string shortcutName)
        {
            Trace.TraceInformation(nameof(DeleteSelfExeFolderAndShortcut));
            TryDeleteFolderAsAdmin(Path.GetDirectoryName(Application.ExecutablePath));
            TryDeleteDesktopShortcut(shortcutName);
            TryDeleteAppFromReg(appGuid);
        }

        private static bool TryDeleteDesktopShortcut(string shortcutName)
        {
            Trace.TraceInformation(nameof(TryDeleteDesktopShortcut));
            //Для удаления ярлыка, нужен админ
            var desktop = GetAllUsersDesktopFolderPath();
            var app = shortcutName + "." + SharedConstants.ShortcutExtension;
            var path = Path.Combine(desktop, app);
            return TryDeleteFileAsAdmin(path);
        }

        public static bool TryDeleteAppFromReg(string appGuid)
        {
            string regVersion = "32";
            if (Is64BitOS())
                regVersion = "64";
            return RunCmdAsAdmin("cmd.exe", string.Format("/C reg delete HKLM\\{0}\\{1} /reg:{2} /f", SharedConstants.UninstallRegKeyPath, new Guid(appGuid).ToString("B"), regVersion));
        }

        public static bool Is64BitOS()
        {
            bool is64BitProcess = (IntPtr.Size == 8);
            return is64BitProcess || InternalCheckIsWow64();
        }

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
    [In] IntPtr hProcess,
    [Out] out bool wow64Process
);

        private static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    if (!IsWow64Process(p.Handle, out bool retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool TryDeleteFileAsAdmin(string filePath)
        {
            Trace.TraceInformation(nameof(TryDeleteFileAsAdmin));
            return RunCmdAsAdmin("cmd.exe", "/C ping 1.1.1.1 -n 1 -w 3000 > Nul & Del \"" + filePath + "\"");
        }

        private static bool RunCmdAsAdmin(string app, string args)
        {
            Trace.TraceInformation(nameof(RunCmdAsAdmin));
            var result = false;
            try
            {
                var psi = new ProcessStartInfo(app, args)
                {
                    Verb = "runas"
                };
                Process.Start(psi);
                result = true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            return result;
        }

        public static bool TryDeleteFolderAsAdmin(string folderPath)
        {
            Trace.TraceInformation(nameof(TryDeleteFileAsAdmin));
            return RunCmdAsAdmin("cmd.exe", "/C ping 1.1.1.1 -n 1 -w 3000 > Nul & RD /s /q \"" + folderPath + "\"");
        }

        #region DesktopPath
        [DllImport("shfolder.dll", CharSet = CharSet.Auto)]
        private static extern int SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, int dwFlags, StringBuilder lpszPath);
        private const int MAX_PATH = 260;
        private const int CSIDL_COMMON_DESKTOPDIRECTORY = 0x0019;
        public static string GetAllUsersDesktopFolderPath()
        {
            StringBuilder sbPath = new StringBuilder(MAX_PATH);
            SHGetFolderPath(IntPtr.Zero, CSIDL_COMMON_DESKTOPDIRECTORY, IntPtr.Zero, 0, sbPath);
            return sbPath.ToString();
        }
        #endregion

        internal static string GetUserPublicFolderPath()
        {
            return new DirectoryInfo(GetAllUsersDesktopFolderPath()).Parent.FullName;
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void SwitchToAlreadyOpenedApp()
        {
            var currentProcess = Process.GetCurrentProcess();
            var processes = Process.GetProcessesByName(currentProcess.ProcessName);
            var process = processes.FirstOrDefault(p => p.Id != currentProcess.Id);
            if (process == null) return;

            SetForegroundWindow(process.MainWindowHandle);
        }

        public static UInt32 KmToDeviceMeters(double km)
        {
            return (uint)(km * 1000L / 5L);
        }

        public static double DeviceMetersToKm(UInt32 meters)
        {
            return meters * 5L / 1000d;
        }
    }

}
