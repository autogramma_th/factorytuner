public static class SharedConstants
{
    public const string FactoryTunerGuid = "8F3366D3-D17A-4815-B6F8-5302388BF211";
    public const string FactoryDataCfgGuid = "8F3366D3-D17A-4815-B6F8-5302388BF212";
    public const string FactoryUserCfgGuid = "8F3366D3-D17A-4815-B6F8-5302388BF213";
    public const string FactoryInstallerGuid = "8F3366D3-D17A-4815-B6F8-5302388BF214";
    public const string UninstallRegKeyPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
    public const string ShortcutExtension = "lnk";
    public const string DefaultUsersConfigFileName = "user.xml";
    public const string DefaultAutoDataFileName = "base.xml";
    public const string DefaultDataDirName = "data";
    public const string DefaultJournalFileName = "journal.csv";
    public const string DefaultConfigFileName = "config.xml";
    public const string FactoryAutoShortkutName = "����� ����";
    public const string FactoryUserShortkutName = "����� ������������";
    public const string FactoryTunerShortkutName = "����� ������";
    public const string DataFileFormat = "xml";
    public const string JournalFileFormat = "csv"; //������ ���������� �� DataFileFormat

    public const string Version = "1.12.0.0";

}

/// <summary>
/// ������ ��������� � exe �������
/// </summary>
public enum AppName
{
    MIKAS_Master,
    MIKAS_Manager,
    FactoryUserConfig,
    FactoryDataConfig
}

public class TextValueItem<T>
{
    public TextValueItem(string text, T value)
    {
        Text = text;
        Value = value;
    }

    public string Text { get; set; }
    public T Value { get; set; }
}